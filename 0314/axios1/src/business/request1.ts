import axios from "axios";

// 默认基础访问URL地址
axios.defaults.baseURL="http://localhost:3000"
axios.defaults.timeout=4000;//默认超时时间
// axios.get("/user/list")

// axios({
//     url:"/users/list",
//     method:"get",
//     params:{a:1,b:2},
//     headers:{}
// }).then(result=>{
//     console.log(result.data)
// })

// axios({
//     url:"/users/login",
//     method:"post",
//     data:{a:1,b:2},
//     headers:{}
// }).then((result)=>{
// console.log(result.data);
// })
// init()
// async function init() {
    // const result=await axios({url:"/users/list?a=1&b=2"}).catch(()=>{});
    // console.log(result.data)
    // const result=await axios({url:"/users/login",method:"post",data:{a:1,b:2}}).catch(()=>{});
    // console.log(result.data)

//    const result=await axios.get("/users/list?a=1&b=2");
//    console.log(result.data);

    // const result=await axios.post("/users/login",{a:1,b:2});
    // console.log(result.data);

    // axios并发请求
    // axios.all([
    //     axios.get("/users/list?a=1&b=2"),
    //     axios.post("/users/login",{a:1,b:2})
    // ]).then(axios.spread((result1,result2)=>{
    //     console.log(result1,result2)
    // }))
// }

