import axios from "axios";
import {useCounterStore} from "@/stores/counter";

const requestMap=new Map();


const ins=axios.create({
    baseURL:"http://localhost:3000",
    timeout:4000
});

// 请求拦截
ins.interceptors.request.use((config)=>{
    if(requestMap.has(config.url)){
        // 断开上次请求
        requestMap.get(config.url).abort();
    }
    const abort=new AbortController()
    // 重新本次请求，并且存储
    requestMap.set(config.url,abort);
    const store=useCounterStore();
    if(store.userInfo.token){
        config.headers.token=store.userInfo.token;
    }
    config.signal=abort.signal;
    return config;
},error=>{
    console.log(error,"----请求失败")
})

// 响应拦截
ins.interceptors.response.use(result=>{
    if(requestMap.has(result.config.url)){
        requestMap.delete(result.config.url)
    }
    if(result.data.token){
        const store=useCounterStore();
        store.userInfo.token=result.data.token;
    }
    // console.log(result,"----响应拦截");
    return result;
},error=>{
    console.log(error,"----响应失败");
})

export default ins;