export default {
    namespaced: true,
    // state() {
    //     return {
    //         language: "ch"
    //     }
    // },
    state:{
        language: "ch"
    },
    getters: {

    },
    mutations: {
        setLanguage(state,value){
            // console.log(state,value);
            state.language=value;
        }
    },
    actions: {

    },
}