import { createStore } from 'vuex'
import userStore from './userStore';
export default createStore({
  state: {
    count:1,
    data:null,
  },
  getters: {
    sums(state){
      // console.log(state,getters);
      if(!state.data) return 0;
      return state.data.a+state.data.b;
    }
  },
  mutations: {
    setCount(state,value){
      // this 执行当前store本身
      // state指state属性
      // value就是修改的值
      // console.log(state,value);
      state.count=value;
    },
    saveData(state,value){
      state.data=value;
    }
  },
  actions: {
    async getConfig(store,val){
      // console.log(store,val);
      var result=await fetch("http://localhost:8080/"+val).catch(()=>{});
      if(result){
        result=await result.json();
         store.commit("saveData",result);
      }
    }
  },
  modules: {
    userStore
  }
})
