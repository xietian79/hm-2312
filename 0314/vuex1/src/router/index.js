import MainView from '@/views/MainView.vue'
import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path:"/",
    name:"main",
    component:MainView
  },
  {
    path:"/user",
    name:"user",
    component:()=>import("@/views/UserView.vue")
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
