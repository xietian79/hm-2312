import MainView from '@/views/MainView.vue'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path:"/",
      name:"main",
      component:MainView
    },
    {
      path:"/user",
      name:"user",
      component:()=>import("@/views/UserView.vue")
    }
  ]
})

export default router
