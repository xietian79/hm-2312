import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

// export const useCounterStore = defineStore('counter', () => {
//   const count = ref(0)
//   const doubleCount = computed(() => count.value * 2)
//   function increment() {
//     count.value++
//   }

//   return { count, doubleCount, increment }
// })

export const useCounterStore=defineStore("counter",{
  persist:{
    key:"user",
    storage:sessionStorage
  },  
  state(){
    return {
        count:0,
        info:null,
    }
  },
  getters:{
    doubleCount(){
      return this.count*2;
    }
  },
  actions:{
    async increment(name){
        let result=await fetch("http://localhost:5173/"+name).catch(()=>{});
        if(result){
          result=await result.json();
          this.info=result;
        }
    }
  }
})