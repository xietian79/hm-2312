import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPresist from "pinia-plugin-persistedstate"
import "@/assets/base.styl";

import App from './App.vue'
import router from './router'

const app = createApp(App)
const pinia=createPinia();
pinia.use(piniaPresist)
app.use(pinia)
app.use(router)

app.mount('#app')
