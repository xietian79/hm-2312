var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/login', function(req, res, next) {
  res.send({err:null,data:{msg:"登录成功",data:[
    {proid:1001,proname:"张三"},
    {proid:1002,proname:"李四"},
    {proid:1003,proname:"王五"},
    {proid:1004,proname:"赵六"},
    {proid:1005,proname:"钱八"},
  ]}});
});
router.post('/register', function(req, res, next) {
  res.send({err:null,data:{msg:"注册成功",data:[
    {proid:1001,proname:"张三"},
    {proid:1002,proname:"李四"},
    {proid:1005,proname:"钱八"},
  ]}});
});


module.exports = router;
