
import IndexView from "@/views/IndexView.vue"
import { createRouter,createWebHistory } from "vue-router";
const routes=[
  {
    path:"/",
    component:IndexView
  },
    {
      path:"/user",
      name:"user",
      component:()=>import("@/views/UserView.vue"),
      children:[
          {
            path:"/user/register",
            name:"register",
            component:()=>import("@/views/RegisterView.vue")
          },
          {
            path:"/user/login",
            name:"login",
            component:()=>import("@/views/LoginView.vue")
          }
      ]
    },
    {
      path:"/list",
      name:"list",
      component:()=>import("@/views/ListView.vue")
    }
  
]

const router=createRouter({
  history:createWebHistory(),
  routes
})


export default router;