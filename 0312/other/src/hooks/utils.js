import { onMounted, onUnmounted, ref } from "vue";
export function useMousePoint() {

    const x = ref(0);
    const y = ref(0);

    onMounted(() => {
        window.addEventListener("mousemove", mouseHandler);
    })
    onUnmounted(() => {
        window.removeEventListener("mousemove", mouseHandler);
    })

    function mouseHandler(e) {
        x.value = e.clientX;
        y.value = e.clientY;
    }
    return { x, y };
}

export function usePostUser(results,router, body) {
    fetch("http://localhost:3000" + router, { method: "POST", body: JSON.stringify(body) })
        .then(result => {
            result.json().then(result => {
                results.data = result.data.data;
            }).catch(e => results.error = e);
        }).catch(e => results.error = e);
    return results;
}