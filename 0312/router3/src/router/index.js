import HomeView from "@/views/HomeView.vue";
import { createRouter,createWebHistory } from "vue-router";
const routes=[
  {
    path:"/",
    components:{
      default:HomeView,
      register:()=>import("@/views/AboutView.vue")
    }
  },
  {
    path:"/userlist",
    components:{
      default:()=>import("@/views/UserListView.vue"),
      register:()=>import("@/views/ShoppingView.vue")
    }
  }
  
]

const router=createRouter({
  history:createWebHistory(),
  routes
})


export default router;