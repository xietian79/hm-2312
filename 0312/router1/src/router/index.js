import HomeView from "@/views/HomeView.vue";
import { createRouter,createWebHistory } from "vue-router";
const routes=[
  {
    path:"/",
    name:"a",
    component:HomeView
  },
  {
    path:"/about",
    name:"b",
    component:()=>import("@/views/AboutView.vue")
  },
  {
    path:"/userlist",
    name:"c",
    meta:{bool:true},
    component:()=>import("@/views/UserListView.vue")
  }
]

const router=createRouter({
  history:createWebHistory(),
  routes
})


export default router;