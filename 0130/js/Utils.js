export default class Utils{
    static randomColor(){
        return Array.from({length:6}).reduce(v=>v+(~~(Math.random()*16)).toString(16),"#");
    }
}