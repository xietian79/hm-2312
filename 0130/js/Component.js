export default class Component{
    elem
    constructor(type,className){
        this.elem=document.createElement(type);
        if(className) this.elem.className=className;
    }
    appendTo(parent){
        if (typeof parent === "string") parent = document.querySelector(parent);
        if (parent instanceof HTMLElement) parent.appendChild(this.elem);
        return parent;
    }
}