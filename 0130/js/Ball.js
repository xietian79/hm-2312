// export default class Ball{
//     x=0;
//     y=0;
//     speedX=2;
//     speedY=2;
//     bool=false;
//     elem;
//     constructor(){
//         this.elem=document.createElement("div");
//         this.elem.className="ball"
//        var div= document.querySelector(".div1");
//        div.appendChild(this.elem);
//        setInterval(()=>this.animation(),16)
//        this.elem.addEventListener("click",e=>this.clickhandler(e));
//     }
//     animation(){
//         if(!this.bool) return;
//         this.x+=this.speedX;
//         this.y+=this.speedY;
//         if(this.x<=0 || this.x>=950) this.speedX=-this.speedX;
//         if(this.y<=0 || this.y>=450) this.speedY=-this.speedY;
//         this.elem.style.left=this.x+"px";
//         this.elem.style.top=this.y+"px";
//     }
//     clickhandler(e){
//         this.bool=!this.bool;
//     }
// }

// export default class Ball {
//   x = 0;
//   y = 0;
//   speedX = 2;
//   speedY = 2;
//   bool = false;
//   elem;
//   constructor() {
//     this.elem = document.createElement("div");
//     this.elem.className = "ball";
//     this.elem.addEventListener("click", (e) => this.clickHandler(e));
//   }
//   appendTo(parent) {
//     if (typeof parent === "string") parent = document.querySelector(parent);
//     if (parent instanceof HTMLElement) parent.appendChild(this.elem);
//     return parent;
//   }
//   clickHandler(e) {
//     this.bool = !this.bool;
//   }
//   update() {
//     if (!this.bool) return;
//     this.x += this.speedX;
//     this.y += this.speedY;
//     if (this.x <= 0 || this.x >= 950) this.speedX = -this.speedX;
//     if (this.y <= 0 || this.y >= 450) this.speedY = -this.speedY;
//     this.elem.style.left = this.x + "px";
//     this.elem.style.top = this.y + "px";
//   }
// }



// import Component from "./Component.js";
// export default class Ball extends Component{
//   x = 0;
//   y = 0;
//   speedX = 2;
//   speedY = 2;
//   bool = false;
//   static list=new Set();
//   constructor() {
//     super("div","ball");
//     Ball.list.add(this);
//     this.elem.addEventListener("click", (e) => this.clickHandler(e));
//   }
//   clickHandler(e) {
//     this.bool = !this.bool;
//   }
//   update() {
//     if (!this.bool) return;
//     this.x += this.speedX;
//     this.y += this.speedY;
//     if (this.x <= 0 || this.x >= 950) this.speedX = -this.speedX;
//     if (this.y <= 0 || this.y >= 450) this.speedY = -this.speedY;
//     this.elem.style.left = this.x + "px";
//     this.elem.style.top = this.y + "px";
//   }
//   static update(){
//     this.list.forEach(item=>item.update());
//   }
// }



import Component from "./Component.js";
import Utils from "./Utils.js";
export default class Ball extends Component{
  x = 0;
  y = 0;
  speedX = 2;
  speedY = 2;
  r=10;
  rect;
  static list=new Set();
  constructor() {
    super("div");
    this.r=~~(Math.random()*5)+10;
    this.speedX=this.randomSpeed();
    this.speedY=this.randomSpeed()
   
    Object.assign(this.elem.style,{
      width:this.r+"px",
      height:this.r+"px",
      borderRadius:this.r+"px",
      position:"absolute",
      left:this.x+"px",
      top:this.y+"px",
      backgroundColor:Utils.randomColor()
    })
    Ball.list.add(this);
  }
  randomSpeed(){
    var speed=Math.random()*6-3;
    if(speed<0.5 && speed>0) speed=1;
    else if(speed>-0.5 && speed<0) speed=-1; 
    return speed;
  }

  appendTo(parent){
    parent=super.appendTo(parent);
    this.rect=parent.getBoundingClientRect();
    this.x=(this.rect.width-this.r)*Math.random();
    this.y=(this.rect.height-this.r)*Math.random();
  }
  update() {
    this.x += this.speedX;
    this.y += this.speedY;
    if(this.rect){
      if (this.x < 0 || this.x > this.rect.width-this.r ) this.speedX = -this.speedX;
      if (this.y < 0 || this.y > this.rect.height-this.r) this.speedY = -this.speedY;
    }
    
    this.elem.style.left = this.x + "px";
    this.elem.style.top = this.y + "px";
  }
  static update(){
    this.list.forEach(item=>item.update());
  }
}
