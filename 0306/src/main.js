import { createApp } from 'vue'
import App from './App.vue'
import ComponentA from "@/components/ComponentA.vue";
// import ComponentC from "@/components/ComponentC.js";

var app=createApp(App);
// 注册一次多次使用
// app.component("注册标签名",组件)
app.component("ComponentA",ComponentA)
// .component("ComponentC",ComponentC);//js模版组件不可使用

app.mount('#app')


// var o={a:0,b:0,s:0};

// var p=new Proxy(o,{
//     set(target,key,value){
//         if(key==="a"){
//             Reflect.set(target,"s",value+Reflect.get(target,"b"));
//         }else if(key==="b"){
//             Reflect.set(target,"s",value+Reflect.get(target,"a"));
//         }
//         return Reflect.set(target,key,value);
//     },
//     get(target,key){
//         return Reflect.get(target,key);
//     }
// })

// p.a=10;
// p.b=20;
// console.log(p.s);



// camelCase 驼峰命名  getSum
// PascalCase 帕斯卡命名  ComponentCount
// kebab-case  短横线命名  button-component