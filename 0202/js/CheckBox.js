import Component from "./Component.js";
export default class CheckBox extends Component{
    _label="";
    icon;
    title;
    _checked=false;
    constructor(_label){
        super("div","checkbox clear");
        this.elem.innerHTML=`
            <div class='icon'></div>
            <h4 class='title'></h4>
        `
        this.icon=this.elem.querySelector(".icon");
        this.title=this.elem.querySelector(".title");
        if(_label) this.label=_label;
        this.checked=false;
        this.setStyle();
        this.elem.addEventListener("click",e=>this.clickhandler(e));
    }
    set label(value){
        this._label=value;
        this.title.textContent=value;
    }
    get label(){
        return this._label;
    }
    set checked(value){
        this._checked=value;
       Object.assign(this.icon.style,{
        backgroundPositionX:value ? "-128px" : "-238px",
        backgroundPositionY:value ? "-126px" : "-37px",

       })
    }
    get checked(){
        return this._checked;
    }
    clickhandler(e){
        this.checked=!this.checked;
    }
    setStyle(){
        CheckBox.setCss(`
            .checkbox{
                position:relative;
            }
            .checkbox>.icon{
                background-image:url('../img/new_icon.png');
                width:14px;
                height:14px;
                float:left;
                margin-top:4px;
            }
            .checkbox>.title{
                float:left;
                margin:0;
                padding:0;
                margin-left:10px;
                
            }
            .clear::after{
                content: "";
                overflow: hidden;
                clear: both;
                height: 0;
                display: block;
                visibility: hidden;
               }
        `)
    }
}