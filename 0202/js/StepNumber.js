import Component from "./Component.js";
export default class StepNumber extends Component{
    _step=1;
    ids;
    static STEP_CHANGE_EVENT="step_change_event";
    constructor(){
        super("div","stepnumber");
        this.elem.innerHTML=`
            <button disabled>-</button>
            <input type='text'>
            <button>+</button>
        `
        this.setStyle();
        // this.setStep(this._step);
        this.num=1;
        this.elem.addEventListener("click",e=>this.clickHandler(e));
        this.elem.addEventListener("input",e=>this.inputHandler(e));
    }
    clickHandler(e){
        if(e.target.nodeName!=="BUTTON") return;
        if(e.target.textContent==="-"){
            // this.setStep(this._step-1);
            this.num--;
        }else{
            this.num++;
            // this.setStep(this._step+1);
        }
        this.dispatch();
    }
    inputHandler(e){
        if(e.target.nodeName!=="INPUT") return;
        e.target.value=e.target.value.replace(/\D/g,"");
        // this.setStep(e.target.value);
        this.num=e.target.value;
        if(this.ids) return;
        this.ids=setTimeout(()=>{
            clearTimeout(this.ids);
            this.ids=undefined;
            this.dispatch();
        },500);
    }
    set num(value){
        this.elem.children[0].disabled=false;
        this.elem.children[2].disabled=false;
        value=Number(value);
        if(value<=1){
            value=1;
            this.elem.children[0].disabled=true;
        }else if(value>=99){
            value=99;
            this.elem.children[2].disabled=true;
        }
        this._step=value;
        this.elem.children[1].value=value;
    }
    get num(){
        return this._step
    }
    // setStep(value){
    //     this.elem.children[0].disabled=false;
    //     this.elem.children[2].disabled=false;
    //     value=Number(value);
    //     if(value<=1){
    //         value=1;
    //         this.elem.children[0].disabled=true;
    //     }else if(value>=99){
    //         value=99;
    //         this.elem.children[2].disabled=true;
    //     }
    //     this._step=value;
    //     this.elem.children[1].value=value;
    // }
    dispatch(){
        var evt=new Event(StepNumber.STEP_CHANGE_EVENT);
        evt.step=this._step;
        this.dispatchEvent(evt);
    }
    setStyle(){
        StepNumber.setCss(`
            .stepnumber{
                position:relative;
                width:80px;
                height:20px;
                font-size:0;
            }
            .stepnumber>button{
                width:15px;
                height:18px;
                border:1px solid #999;
                padding:0;
                margin:0;
                outline:none;
            }
            .stepnumber>input{
                width:46px;
                height:16px;
                border:none;
                border-top:1px solid #999;
                border-bottom:1px solid #999;
                margin:0;
                padding:0;
                outline:none;
                text-align:center;
            }
        `)
    }
}