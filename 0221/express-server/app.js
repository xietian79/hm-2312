// 引入http错误的包
var createError = require('http-errors');
// 引入express的包
var express = require('express');
// 引入了nodejs的API path包
var path = require('path');
// 引入cookie解析包
var cookieParser = require('cookie-parser');
// 引入了日志的包
var logger = require('morgan');
var cors=require("cors");
var multer=require("multer")();

// 引入路由
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var goodsRouter=require("./routes/goods");

// 创建一个服务
var app = express();

// view engine setup
// 配置如果访问views自动访问当前路径中views文件夹
app.set('views', path.join(__dirname, 'views'));
// 当遇到ejs扩展名时，通过调用views文件夹获取并且解析
app.set('view engine', 'ejs');

// 允许跨域
app.use(cors());
// 使用日志
app.use(logger('dev'));
app.use(multer.none());
// 当获取到前端发送给服务端的post请求消息时，解析json数据
app.use(express.json());
// 当获取到前端发送给服务端的post请求消息时，解析query字符串  a=1&b=2&c=3
app.use(express.urlencoded({ extended: false }));
// 解析发送过来的cookie
app.use(cookieParser());
// 设置当前public文件夹为静态资源文件夹
app.use(express.static(path.join(__dirname, 'public')));

// 使用引入的路由
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use("/goods",goodsRouter)

// catch 404 and forward to error handler
// 处理404错误
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// 当遇到错误时，解析错误返回给客户端一个错误页面
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
