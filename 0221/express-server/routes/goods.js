var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/list', function(req, res, next) {
   
    console.log(req.query);//获取前端发送get方式地址中的search的数据
  res.send("goodsList")
});
router.post("/add",function(req,res,next){
    console.log(req.body);//可以获取到前端通过post发送来的数据
    res.send("add success!")
})
router.post("/remove",function(req,res,next){
    console.log(req.body);//可以获取到前端通过post发送来的数据
    res.send("remove success!")
})

module.exports = router;