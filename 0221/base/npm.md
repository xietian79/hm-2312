1、npm i 包名 -g  全局安装
2、npm i 包名  --save   安装为项目依赖
  npm i 包名 -S
  npm i 包名

3、npm  i 包名  --save-dev   安装为开发依赖
 npm i 包名 -D


 npm i 包名1  包名2  包名3  -S


 {
  "name": "0221",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "jquery": "^3.7.1"   ^ 大版本锁定 小版本和微版本都会更新到最新的
    "jquery": "~3.7.1"   ~ 小版本锁定  大版本和小版本都锁定，更新微版本到最新的
    "jquery": "3.7.1"   全锁定 版本不会更新
    "jquery": "*"   * 下载最新版本
  },
  "devDependencies": {
    "webpack": "^5.90.3"
  }
}

1.2.1
2.4.5
3.7.1
3.7.3
3.8.5
4.2.1

4、 npm view 包名 versions

5、如果将包名和版本在package.json中描述了，直接使用npm i就可以更新所有的包  也可以使用npm update
"dependencies": {
      "jquery":"^3.7.1",
      "bootstrap":"~3.3.7"
  },
  "devDependencies": {
    "webpack": "^5.90.3"
  }

6、 npm ls 包名  查看已下载的包名的版本

7、npm view 包名 查看包名的信息

8、npm cache verfiy
npm cache clean --force
清除缓存，将已下载的无效数据清除

9、脚本    npm run 脚本名
在scripts中写入脚本名和对应的命令内容
可以使用  npm run 脚本名 执行对应的命令

 "config": {
      "name":"xietian",
      "age":20
  },
  process.env.npm_package_config_name
  process.env.npm_package_config_age
  必须通过脚本执行后才可获得，直接运行命令是不可以的

特殊脚本名  npm 脚本名
start   直接执行
install 先下载所有包，再执行代码

尽量不要全局安装插件，将全局包安装在开发依赖中
当执行脚本时会自动搜索到当前开发依赖包中的全局命令，就可以执行


10、卸载包
npm uninstall 包名 
npm uninstall 包名 -g
npm uninstall 包名 -D
npm uninstall 包名 -S


npx 全局包名
当临时文件夹中有这个包时，会自动执行这个全局命令
如果没有这个包，就会自动将这个包下载到临时文件夹中，然后执行这个全局命令


cnpm
npm i cnpm -g
它的使用方法与npm完全相同

yarn

yarn init -y
yarn add 包名   安装包
yarn remove 包名 删除包
yarn    等同于npm i