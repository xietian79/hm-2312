// node的API包，如果时node的API包或者在node_modules文件夹下的包不需要写路径./
const http=require("http");
// 创建服务
// http.createServer(function(请求对象,响应对象){
    // req请求对象  客户端发送给服务端的信息对象
    // res响应对象  服务端发送给客户端的信息对象
    // res.write("<div>1</div>") 给响应消息体写入内容
    // res.end("hello world!") 结束内容并且发送给客户端消息
// }).listen(端口号,本地地址,function(){
//     console.log("服务开启！")
// })

// 写入响应头 200表示成功
// res.writeHead(200,{
        
// })

http.createServer(function(req,res){
    res.writeHead(200,{
        "content-type":"text/html;charset=utf-8"
    })
    // res.write("<div>1</div>")
    // res.write("<div>谢天</div>")
    res.write("<ul>")
    res.write("<li>1</li>")
    res.write("<li>2</li>")
    res.write("<li>3</li>")
    res.write("</ul>")
    res.end()
}).listen(process.env.PORT||3000)