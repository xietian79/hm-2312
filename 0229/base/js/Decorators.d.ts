declare function state(): (param: any, key: string, desc: any) => void;
declare class Box {
    play(): void;
}
declare class Ball {
    run(): void;
}
declare var c: Ball;
//# sourceMappingURL=Decorators.d.ts.map