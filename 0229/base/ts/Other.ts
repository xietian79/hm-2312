// type A=Exclude<"a"|"b","b"|"c">
// var ab:A="a";
// 只能剔除字面量
// interface IA{
//     a:number;
//     b:number;
//     c:number;
// }
// interface IB{
//     a:number;
//     b:number;
// }

// type A=Exclude<IA,IB>;
// var ab:A={};
// 获取两个字面量的交集
// type A="a" | "b" | "c";
// type B="c" | "d" | "e"
// type C=Extract<A,B>;
// var ab:C="c"

// 返回函数接口的返回值类型
// interface IFn{
//     (a:number,b:number):number;
// }
// type A=ReturnType<IFn>;
// var a:A=1;
// a="a"

// 返回类类型的解构
// var o:{a:number,b:number}={a:1,b:2};
// var A=InstanceType<typeof o>;

// class C {
//     x = 0;
//     y = 0;
// }
// interface IC{
//     x:number;
//     y:number;
// }
// type A = InstanceType<typeof IC>;
// type A = InstanceType<typeof C>;
// var ab:A={x:10,y:20};


// var date=new Date();
// console.dir(date)
// console.log(date)

// var reg=/a/g;
// console.dir(reg)
// var divs=document.querySelectorAll("div");
// console.log(divs)


// namespace A{
//     export class Box{
//         constructor(){

//         }
//         public play():void{
//             console.log("playA")
//         }
//     }
// }
// namespace B{
//     export class Box{
//         constructor(){

//         }
//         public play():void{
//             console.log("playB")
//         }
//     }
// }

// var a:A.Box=new A.Box();
// a.play();
// var b:B.Box=new B.Box();
// b.play();