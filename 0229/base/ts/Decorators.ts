// interface IA{
//     play():void;
//     run():void;
// }
// function fn(param:any){
//     var plays=param.prototype.play;
//     param.prototype.play=function(){
//         plays.apply(this,arguments);
//         console.log("play2");
//     }
// }

// @fn
// class Box{
//     constructor(){

//     }
//     public play():void{
//         console.log("play1")
//     }
//     public run():void{
//         console.log("run1");
//     }
// }


// var b:IA=new Box() as IA;
// b.play();




// interface IA{
//     play():void
// }

// function fn(param:any){
//     param.prototype.play=function(){
//         if(param == Student){
//             console.log("play1");
//         }else if(param == Person){
//             console.log("play2")
//         }
//     }
// }

// @fn
// class Student{

// }

// @fn
// class Person{

// }
// @fn

// class Teacher{

// };

// var a:IA=new Student() as IA;
// a.play();

// var b:IA=new Person() as IA;
// b.play();



// 类属性装饰器
// function setFormat(value:number){
//     return function(param:any,key:string){
//         var inputs=document.querySelector("input");
//         inputs?.addEventListener("input",e=>inputHandler(e,param,key)) ;    
//     }
// }
// function inputHandler(e:Event,param:any,key:string):void{
//     param[key]=e.target.value;
// }


// class Box{
//     // 先执行了装饰器方法，设置了值，然后才会运行到后面设置10
//     @setFormat(30)
//     public num?:number;

//     public play():void{
//         console.log(this.num);
//     }
// }

// var b:Box=new Box();
// console.log(b.num);

// setTimeout(function(){
//     b.play();
// },5000)



function state(){
    return function(param:any,key:string,desc:any){
        // console.log(desc.value)
        var fn=desc.value;
        desc.value=function(){
            console.log("play1");
            fn.apply(this,arguments);
        }
    }
}

class Box{
    @state()
    public play():void
    {
        console.log("play")
    }
}

class Ball{
    @state()
    public run():void{
        console.log("run")
    }
}

// var b=new Box();
// b.play();

var c:Ball=new Ball();
c.run();