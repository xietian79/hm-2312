// var target:EventTarget=new EventTarget();
// target.addEventListener("abc",abcHandler1);
// target.addEventListener("abc",abcHandler2);
// target.addEventListener("abc",abcHandler3);


// var evt:Event=new Event("abc");
// target.dispatchEvent(evt);

// var evt:Event=new Event("abc");
// target.dispatchEvent(evt);


// function abcHandler1(e:Event){
//     console.log("aa")
// }
// function abcHandler2(e:Event){
//     console.log("bb")
//     target.removeEventListener("abc",abcHandler2);
// }
// function abcHandler3(e:Event){
//     console.log("cc")
// }
import Emitter from "./Emitter.js";
import EmitterTarget from "./EmitterTarget.js";

var target:EmitterTarget=new EmitterTarget();
target.addEventListener("abc",abcHandler1);
target.addEventListener("abc",abcHandler1);
target.addEventListener("abc",abcHandler2);
target.addEventListener("abc",abcHandler3);
//当执行这个侦听方法时，传入了type字符串和abcHandler1函数
// 在addEventListener就会把abcHandler1函数存储起来，存在对象中，以当前abc这个字符串作为key，存储了这个函数

var evt:Emitter=new Emitter("abc")
target.dispatchEvent(evt);
var evt:Emitter=new Emitter("abc")
// evt.a=1;
target.dispatchEvent(evt);
//创建的Emitter对象带入到dispatchEvent方法中
// 然后在EmitterTarget中通过存储的对象中找到对应key时abc这个字符串存储的函数，并且执行这个函数传参创建emitter

function abcHandler1(this:void,e:Emitter){
    // console.log(this)
    console.log("aa")
}
function abcHandler2(e:Emitter){
    console.log("bb")
    target.removeEventListener("abc",abcHandler2);
}
function abcHandler3(e:Emitter){
    console.log("cc")
}