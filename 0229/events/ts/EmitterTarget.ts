import Emitter from "./Emitter.js";

// interface IHandler{
//     (this:EmitterTarget,e:Emitter):void;
// }
// interface IDict{
//     [key:string]:Array<IHandler|null>
// }
// export default class EmitterTarget{
//     private dict:IDict={}
//     constructor(){

//     }
//     public addEventListener(type:string,handler:IHandler):void{
//         // this.dict[type]=handler;
//         // console.log(this.dict);
//         // 因为要根据type存储一个列表，因此先判断这个对象中有没有type对应的key存储，如果没有就创建一个新数组
//         if(!this.dict[type]) this.dict[type]=[];
//         // 因为考虑到数组里面存储的函数可能会重复，因此这里先判断数组中有没有这个函数，如果没有再把这个函数添加在数组中
//         if(!this.dict[type].find((f:IHandler|null)=>f==handler)) this.dict[type].push(handler);
//         // console.log(this.dict)
//     }

//     public removeEventListener(type:string,handler:IHandler):void{
//         // 如果存储对象中没有type这个属性，就跳出
//         if(!this.dict[type])return;
//         // 找到这个属性对应的数组中是否有handler这个函数，并且找到它的下标
//         var index:number=this.dict[type].findIndex((f:IHandler|null)=>f==handler);
//         if(index>-1){
//             // this.dict[type].splice(index,1);
//             // 因为直接删除会引起数组塌陷，所以在这里先设置为null，循环完成后再清除
//             this.dict[type][index]=null;
//         }
//     }

//     public dispatchEvent(evt:Emitter):void{
//         // console.log(evt);
//         // console.log(evt.type);//abc
//         // console.log(this.dict);//{"abc":函数}
//         // console.log(this.dict[evt.type])
//         // this.dict[evt.type](evt)
//         // 判断在存储的对象中有没有emitter对象的属性type带入的字符串作为key，如果没有就跳出
//         if(!this.dict[evt.type]) return;
//         // 如果有就是一个数组，遍历这个数组，数组中存储所有函数 this.dict[evt.type][i]每一个函数
//         for(var i=0;i<this.dict[evt.type].length;i++){
//             // 为了让这个事件函数中的this指向当前侦听的对象，也就是当前的EmitterTarget，所以使用call将当前this替代函数中this
//             // 并且传参evt
//             // 执行函数时，有可能这个数组中有被删除设置为null的部分，不执行
//            if(this.dict[evt.type][i])  this.dict[evt.type][i]?.call(this,evt);
//         }
//         // 当循环执行完所有的函数时，剔除掉元素不为真，null的元素
//         this.dict[evt.type]=this.dict[evt.type].filter((f:IHandler|null)=>f);
//     }
// }



interface IHandler{
    (this:EmitterTarget,e:Emitter):void;
}
interface IDict{
    [key:string]:Set<IHandler>
}
export default class EmitterTarget{
    private dict:IDict={}
    constructor(){

    }
    public addEventListener(type:string,handler:IHandler):void{
        // this.dict[type]=handler;
        // console.log(this.dict);
        // 因为要根据type存储一个列表，因此先判断这个对象中有没有type对应的key存储，如果没有就创建一个新数组
        if(!this.dict[type]) this.dict[type]=new Set()
        this.dict[type].add(handler);
    }

    public removeEventListener(type:string,handler:IHandler):void{
        // 如果存储对象中没有type这个属性，就跳出
        if(!this.dict[type])return;
        if(this.dict[type].has(handler)) this.dict[type].delete(handler);
    }

    public dispatchEvent(evt:Emitter):void{
        // console.log(evt);
        // console.log(evt.type);//abc
        // console.log(this.dict);//{"abc":函数}
        // console.log(this.dict[evt.type])
        // this.dict[evt.type](evt)
        // 判断在存储的对象中有没有emitter对象的属性type带入的字符串作为key，如果没有就跳出
        if(!this.dict[evt.type]) return;
        for(var handler of this.dict[evt.type]){
            handler.call(this,evt);
        }
    }
}