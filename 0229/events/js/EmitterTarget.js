export default class EmitterTarget {
    constructor() {
        this.dict = {};
    }
    addEventListener(type, handler) {
        // this.dict[type]=handler;
        // console.log(this.dict);
        // 因为要根据type存储一个列表，因此先判断这个对象中有没有type对应的key存储，如果没有就创建一个新数组
        if (!this.dict[type])
            this.dict[type] = new Set();
        this.dict[type].add(handler);
    }
    removeEventListener(type, handler) {
        // 如果存储对象中没有type这个属性，就跳出
        if (!this.dict[type])
            return;
        if (this.dict[type].has(handler))
            this.dict[type].delete(handler);
    }
    dispatchEvent(evt) {
        // console.log(evt);
        // console.log(evt.type);//abc
        // console.log(this.dict);//{"abc":函数}
        // console.log(this.dict[evt.type])
        // this.dict[evt.type](evt)
        // 判断在存储的对象中有没有emitter对象的属性type带入的字符串作为key，如果没有就跳出
        if (!this.dict[evt.type])
            return;
        for (var handler of this.dict[evt.type]) {
            handler.call(this, evt);
        }
    }
}
//# sourceMappingURL=EmitterTarget.js.map