import Emitter from "./Emitter.js";
interface IHandler {
    (this: EmitterTarget, e: Emitter): void;
}
export default class EmitterTarget {
    private dict;
    constructor();
    addEventListener(type: string, handler: IHandler): void;
    removeEventListener(type: string, handler: IHandler): void;
    dispatchEvent(evt: Emitter): void;
}
export {};
//# sourceMappingURL=EmitterTarget.d.ts.map