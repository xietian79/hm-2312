export default class Emitter {
    readonly type: string;
    [key: string]: any;
    constructor(type: string);
}
//# sourceMappingURL=Emitter.d.ts.map