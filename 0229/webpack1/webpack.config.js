import path from "path";
// 用来转换html并且给html中插入js
import HTMLWebpackPlugin from "html-webpack-plugin";
export default {
    entry:"./src/main.js",
    mode:process.env.mode,
    output:{
        path:path.join(path.resolve(),"./dist"),
        filename:"app.js"
    },
    plugins:[
        new HTMLWebpackPlugin({
            template:path.join(path.resolve(),"./public/html/index.html"),
            inject:true
        })
    ]
}