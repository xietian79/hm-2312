// import Ball from "./Ball.js";
// import { SIZE } from "./TimeVo.js";
import Bubbles from "./Bubbles.js";
// var div:HTMLDivElement=document.createElement("div");
// Object.assign(div.style,{
//     width:SIZE.WIDTH+"px",
//     height:SIZE.HEIGHT+"px",
//     position:"relative",
//     border:"1px solid #000"
// })
// document.body.appendChild(div);
// for(var i=0;i<1000;i++){
//     var b:Ball=new Ball();
//     b.appendTo(div);
// }
var form = document.querySelector("form");
form.addEventListener("submit", submitHandler);
function submitHandler(e) {
    e.preventDefault();
    var msg = form.firstElementChild.value;
    var b = new Bubbles(msg);
    b.appendTo(".msg-con");
    form.firstElementChild.value = "";
}
//# sourceMappingURL=Main.js.map