export default class TimeManager {
    constructor() {
        this.list = new Set();
    }
    static get instance() {
        return TimeManager._instance || (TimeManager._instance = new TimeManager());
    }
    add(item) {
        this.list.add(item);
        if (this.list.size > 0 && !this.ids) {
            this.ids = setInterval(() => this.animation(), 16);
        }
    }
    remove(item) {
        if (this.list.has(item))
            this.list.delete(item);
        if (this.list.size === 0 && this.ids) {
            clearInterval(this.ids);
            this.ids = undefined;
        }
    }
    animation() {
        for (var item of this.list) {
            item.update();
        }
    }
}
//# sourceMappingURL=TimeManager.js.map