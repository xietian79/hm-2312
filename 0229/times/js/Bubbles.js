import Component from "./Component.js";
import TimeManager from "./TimeManager.js";
export default class Bubbles extends Component {
    constructor(msg) {
        super("div", "bubble");
        this.msg = msg;
        this.x = 0;
        this.y = 0;
        this.speed = 2;
        this.w = 0;
        this.elem.innerHTML = msg;
    }
    appendTo(parent, firstBool, elem) {
        parent = super.appendTo(parent, firstBool, elem);
        this.rect = parent.getBoundingClientRect();
        this.x = this.rect.width;
        this.y = Math.random() * this.rect.height;
        this.renderBubble();
        this.w = this.elem.offsetWidth;
        TimeManager.instance.add(this);
        return parent;
    }
    setStyle() {
        Bubbles.setCss(`
           
        `);
    }
    dispatch() { }
    renderBubble() {
        this.elem.style.left = this.x + "px";
        this.elem.style.top = this.y + "px";
    }
    update() {
        this.x -= this.speed;
        if (this.x <= -this.w) {
            TimeManager.instance.remove(this);
            this.elem.remove();
        }
        this.renderBubble();
    }
}
//# sourceMappingURL=Bubbles.js.map