import Component from "./Component.js";
import IUpdate from "./IUpdate.js";
export default class Bubbles extends Component implements IUpdate {
    private readonly msg;
    private rect?;
    private x;
    private y;
    private speed;
    private w;
    constructor(msg: string);
    appendTo(parent: string | HTMLElement | DocumentFragment, firstBool?: boolean, elem?: HTMLElement | null): HTMLElement | DocumentFragment;
    protected setStyle(): void;
    protected dispatch(): void;
    private renderBubble;
    update(): void;
}
//# sourceMappingURL=Bubbles.d.ts.map