import IUpdate from "./IUpdate.js";
export default class TimeManager {
    private static _instance;
    private list;
    private ids?;
    private constructor();
    static get instance(): TimeManager;
    add(item: IUpdate): void;
    remove(item: IUpdate): void;
    private animation;
}
//# sourceMappingURL=TimeManager.d.ts.map