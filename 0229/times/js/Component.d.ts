type parentType = string | HTMLElement | DocumentFragment;
export default abstract class Component extends EventTarget {
    private static cssBool;
    protected elem: HTMLElement;
    constructor(type: string, className?: string);
    appendTo(parent: parentType, firstBool?: boolean, elem?: HTMLElement | null): HTMLElement | DocumentFragment;
    protected createDom(type: string, className?: string, parent?: string | HTMLElement, firstBool?: boolean): HTMLElement;
    protected static setCss(css: string): void;
    protected abstract setStyle(): void;
    protected abstract dispatch(): void;
}
export {};
//# sourceMappingURL=Component.d.ts.map