import TimeManager from "./TimeManager.js";
import { SIZE } from "./TimeVo.js";
class Ball {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.speedX = 2;
        this.speedY = 2;
        this.bool = false;
        this.elem = document.createElement("div");
        Object.assign(this.elem.style, {
            width: Ball.W + "px",
            height: Ball.H + "px",
            backgroundColor: "red",
            position: "absolute",
            borderRadius: Ball.W + "px"
        });
        this.elem.addEventListener("click", (e) => this.clickHandler(e));
    }
    clickHandler(e) {
        this.bool = !this.bool;
        this.bool ? TimeManager.instance.add(this) : TimeManager.instance.remove(this);
    }
    isString(parent) {
        return typeof parent === "string";
    }
    appendTo(parent) {
        if (this.isString(parent))
            parent = document.querySelector(parent);
        parent.appendChild(this.elem);
    }
    update() {
        this.x += this.speedX;
        this.y += this.speedY;
        if (this.x >= SIZE.WIDTH - Ball.W || this.x < 0)
            this.speedX = -this.speedX;
        if (this.y >= SIZE.HEIGHT - Ball.H || this.y < 0)
            this.speedY = -this.speedY;
        this.elem.style.left = this.x + "px";
        this.elem.style.top = this.y + "px";
    }
}
Ball.W = 50;
Ball.H = 50;
export default Ball;
//# sourceMappingURL=Ball.js.map