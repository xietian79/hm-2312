import IUpdate from "./IUpdate.js";
export default class Ball implements IUpdate {
    private static readonly W;
    private static readonly H;
    private elem;
    private x;
    private y;
    private speedX;
    private speedY;
    private bool;
    constructor();
    private clickHandler;
    private isString;
    appendTo(parent: string | HTMLElement): void;
    update(): void;
}
//# sourceMappingURL=Ball.d.ts.map