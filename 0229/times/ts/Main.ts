// import Ball from "./Ball.js";
// import { SIZE } from "./TimeVo.js";

import Bubbles from "./Bubbles.js";

// var div:HTMLDivElement=document.createElement("div");
// Object.assign(div.style,{
//     width:SIZE.WIDTH+"px",
//     height:SIZE.HEIGHT+"px",
//     position:"relative",
//     border:"1px solid #000"
// })

// document.body.appendChild(div);

// for(var i=0;i<1000;i++){
//     var b:Ball=new Ball();
//     b.appendTo(div);
// }


var form:HTMLFormElement=document.querySelector("form") as HTMLFormElement;
form.addEventListener("submit",submitHandler);

function submitHandler(e:Event){
    e.preventDefault();
    var msg:string=(form.firstElementChild as HTMLInputElement).value;
    var b:Bubbles=new Bubbles(msg);
    b.appendTo(".msg-con");
    (form.firstElementChild as HTMLInputElement).value="";
}