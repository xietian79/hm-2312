import IUpdate from "./IUpdate.js";
import TimeManager from "./TimeManager.js";
import { SIZE } from "./TimeVo.js";

export default class Ball implements IUpdate{
    private static readonly W:number=50;
    private static readonly H:number=50;
    private elem:HTMLDivElement;
    private x:number=0;
    private y:number=0;
    private speedX:number=2;
    private speedY:number=2;
    private bool:boolean=false;
    constructor(){
        this.elem=document.createElement("div");
        Object.assign(this.elem.style,{
            width:Ball.W+"px",
            height:Ball.H+"px",
            backgroundColor:"red",
            position:"absolute",
            borderRadius:Ball.W+"px"
        })
        this.elem.addEventListener("click",(e:MouseEvent)=>this.clickHandler(e));
    }
    private clickHandler(e:MouseEvent){
        this.bool=!this.bool;
        this.bool ? TimeManager.instance.add(this) : TimeManager.instance.remove(this);
    }
    private isString(parent:string|HTMLElement): parent is string
    {
        return typeof parent==="string";
    }
    public appendTo(parent:string|HTMLElement):void{
        if(this.isString(parent)) parent=document.querySelector(parent) as HTMLElement;
        parent.appendChild(this.elem);
    }
    public update(){
        this.x+=this.speedX;
        this.y+=this.speedY;
        if(this.x>=SIZE.WIDTH-Ball.W || this.x<0) this.speedX=-this.speedX;
        if(this.y>=SIZE.HEIGHT-Ball.H || this.y<0) this.speedY=-this.speedY;
        this.elem.style.left=this.x+"px";
        this.elem.style.top=this.y+"px";
    }
}