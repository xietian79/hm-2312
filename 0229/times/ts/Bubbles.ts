import Component from "./Component.js";
import IUpdate from "./IUpdate.js";
import TimeManager from "./TimeManager.js";

export default class Bubbles extends Component implements IUpdate{
    private rect?:DOMRect;
    private x:number=0;
    private y:number=0;
    private speed:number=2;
    private w:number=0;
    constructor(private readonly msg:string){
        super("div","bubble");
        this.elem.innerHTML=msg;
    }
    public  appendTo(parent: string | HTMLElement | DocumentFragment, firstBool?: boolean, elem?: HTMLElement | null): HTMLElement | DocumentFragment {
      parent=super.appendTo(parent,firstBool,elem);
        this.rect=(parent as HTMLElement).getBoundingClientRect();
        this.x=this.rect.width;
        this.y=Math.random()*this.rect.height;
        this.renderBubble();
        this.w=this.elem.offsetWidth;
        TimeManager.instance.add(this);
      return parent;
    }
    protected setStyle(): void {
        Bubbles.setCss(`
           
        `)
    }
    protected dispatch(): void {}

    private renderBubble():void{
        this.elem.style.left=this.x+"px";
        this.elem.style.top=this.y+"px";
    }
    public update(): void {
        this.x-=this.speed;
        if(this.x<=-this.w){
            TimeManager.instance.remove(this);
            this.elem.remove();
        }
        this.renderBubble();
    }
}