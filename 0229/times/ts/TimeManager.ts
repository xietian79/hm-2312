import IUpdate from "./IUpdate.js";

export default class TimeManager{
    private static _instance:TimeManager;
    private list:Set<IUpdate>=new Set()
    private ids?:number;
    private constructor(){

    }
    public static get instance():TimeManager{
        return TimeManager._instance||(TimeManager._instance=new TimeManager());
    }
    public add(item:IUpdate):void{
        this.list.add(item);
        if(this.list.size>0 && !this.ids){
            this.ids=setInterval(()=>this.animation(),16)
        }
    }
    public remove(item:IUpdate):void{
        if(this.list.has(item))this.list.delete(item);
        if(this.list.size===0 && this.ids){
            clearInterval(this.ids);
            this.ids=undefined;
        }
    }
    private animation(){
        for(var item of this.list){
            item.update();
        }
    }
    
}