type parentType=string|HTMLElement|DocumentFragment;
export default abstract class Component extends EventTarget{
    private static cssBool:boolean=false;
    protected elem:HTMLElement;
    constructor(type:string,className?:string){
        super();
        this.elem=this.createDom(type,className);
    }
    public appendTo(parent:parentType,firstBool:boolean=false,elem:HTMLElement|null=null):HTMLElement|DocumentFragment
    {
        if(!elem) elem=this.elem ;
        if(typeof parent==="string") parent=document.querySelector(parent) as HTMLElement;
        if((parent && parent instanceof HTMLElement )|| (parent && parent instanceof DocumentFragment)){
            if(firstBool) parent.insertBefore(elem,parent.firstElementChild);
            else parent.appendChild(elem);
        }
     
        return parent;
    }
    protected createDom(type:string,className?:string,parent?:string|HTMLElement,firstBool:boolean=false):HTMLElement
    {
        var elem:HTMLElement=document.createElement(type);
        if(className) elem.className=className;
        if(parent){
           this.appendTo(parent,firstBool,elem);
        }
        return elem;
    }
    protected static setCss(css:string):void
    {
        if(this.cssBool) return;
        this.cssBool=true;
        var style:HTMLStyleElement|undefined;
        if(document.styleSheets.length>0){
           style=Array.from(document.styleSheets).findLast((item:CSSStyleSheet)=>item.ownerNode?.nodeName==="STYLE")?.ownerNode as HTMLStyleElement;
        }
        if(!style){
            style=document.createElement("style");
            document.head.appendChild(style);
        }
        style.innerHTML+="\n"+css;
    }
    protected abstract setStyle():void;
    protected abstract dispatch():void;
}