import router from "@/router/index";
var data = {
  "login": () => import("@/views/LoginView.vue"),
  "register": () => import("@/views/RegisterView.vue"),
  "goods": () => import("@/views/GoodsView.vue"),
  "user": () => import("@/views/UserView.vue")
}

export default function useAddRouter(list) {
  list.sort(function (a, b) {
    if (!b.parent.charCodeAt(0) || a.parent.charCodeAt(0)) return -1;
    return a.parent.charCodeAt(0) - b.parent.charCodeAt(0);
  });
  list.reverse();
  for (var i = 0; i < list.length; i++) {
      if(!list[i].parent){
        router.addRoute({path:list[i].path,name:list[i].name,component:data[list[i].name]})
      }else{
        router.addRoute(list[i].parent,{path:list[i].path,name:list[i].name,component:data[list[i].name]})
      }
  }
  // for(var i=0;i<list.length;i++){
  // router.addRoute({
  //     path:list[i].path,
  //     name:list[i].name,
  //     component:data[list[i].name]
  // })
  // router.addRoute(路由对象)
  // router.addRoute(父级name,路由对象)
  // }
}