import MainView from '@/views/MainView.vue'
import Page404View from "@/views/Page404View.vue"
import { createRouter, createWebHistory } from 'vue-router'




const routes = [
  {
    path:"/",
    name:"main",
    component:MainView,
  },
  {
    path:"/:pathMatch(.*)",
    component:Page404View}
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  mode:"history",
  scrollBehavior:(to,from,pos)=>{
    console.log(pos)
    if(pos){
      return pos;
    }
    return {top:0}
  },
})


// router.beforeEach((to,from,next)=>{
//   if(!to.meta.login){
//       if(!localStorage.token){
//         next({name:"login"})
//       }
//   }else{
//     next();
//   }
// })

export default router

