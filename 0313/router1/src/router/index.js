import MainView from '@/views/MainView.vue'
import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path:"/",
    name:"main",
    component:MainView
  },
  {
    path:"/master",
    name:"master",
    redirect:(to)=>{
      console.log(to)
      return {name:"user",query:{...to.query}}
    }
  },
  {
    path:"/user",
    name:"user",
    component:()=>import("@/views/UserView.vue"),
    children:[
      {
        path:"register",
        name:"register",
        component:()=>import("@/views/RegisterView.vue")
      }
    ]
  },
  {
    path:"/goods",
    name:"goods",
    meta:{lv:1},
    component:()=>import("@/views/GoodsView.vue"),
    children:[
      {
        path:"list",
        name:"list",
        meta:{lv:2},
        component:()=>import("@/views/GoodsList.vue"),
        children:[
          {
            path:"edit",
            name:"edit",
            meta:{lv:3},
            component:()=>import("@/views/GoodsEdit.vue")
          }
        ]
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
