import MainView from '@/views/MainView.vue'
import { createRouter, createWebHistory } from 'vue-router'

var data={
  "login":()=>import("@/views/LoginView.vue"),
  "register":()=>import("@/views/RegisterView.vue")
}


const routes = [
  {
    path:"/",
    name:"main",
    component:MainView,
  },
  // {
  //   path:"/login",
  //   name:"login",
  //   // 针对该路由的变量
  //   meta:{login:true},
  //   component:()=>import("@/views/LoginView.vue")
  // },
  {
    path:"/user",
    name:"user",
    meta:{transition:"alpha"},
    component:()=>import("@/views/UserView.vue"),
  },
  {
    path:"/user/:name",
    component:()=>import("@/views/UserView.vue"),
    beforeEnter:(to,from,next)=>{
      // console.log(to.params);//login register
      router.addRoute({path:to.path,name:to.params.name,component:data[to.params.name]});
      next({name:to.params.name})
    }
  },
  {
    path:"/goods",
    name:"goods",
    meta:{transition:"top"},
    component:()=>import("@/views/GoodsView.vue"),
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


// router.beforeEach((to,from,next)=>{
//   if(!to.meta.login){
//       if(!localStorage.token){
//         next({name:"login"})
//       }
//   }else{
//     next();
//   }
// })

export default router

