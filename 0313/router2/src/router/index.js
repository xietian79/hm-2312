import MainView from '@/views/MainView.vue'
import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path:"/",
    name:"main",
    component:MainView
  },
  {
    path:"/user",
    name:"user",
    component:()=>import("@/views/UserView.vue"),
  },
  {
    path:"/goods",
    name:"goods",
    component:()=>import("@/views/GoodsView.vue"),
    // 这有这个路由，会进入这个守卫函数，可以在这个守卫函数中判断操作后续的内容，是否让其继续
    beforeEnter(to,from,next){
      console.log("beforeEnter");
      // console.log(next);
      next();
    }
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// 所有路由跳转页面执行开始的时候，还没有跳转的时候
router.beforeEach((to,from,next)=>{
  console.log("beforeEach",to.name)
    if(to.name=="user" && to.query.id!="1"){
        // router.replace({name:"goods"});
        next({name:"goods",query:{a:1}})
    }else{
       next();
    }
   
})

// 路由跳转后，可能在beforeEach中重定向，并且传参等内容后，进入beforeResolve，但是还没有页面跳转
router.beforeResolve((to,from,next)=>{
  console.log("beforeResolve")
  // console.log(to)
  next();
})

// 当路由跳转以后，页面完成跳转后，当前跳转的组件setup之前,触发,
router.afterEach((to,form)=>{
  console.log("afterEach")
  to.query.name="xietian"
  // console.log(to,form);
})

export default router

