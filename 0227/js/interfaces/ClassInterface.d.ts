interface IFn {
    (a: number): void;
}
declare var fn: IFn;
interface IA {
    a: number;
    play(): void;
}
declare var o: IA;
interface IUpdate {
    update(): void;
}
declare class Box implements IUpdate {
    update(): void;
}
interface IB {
    new (name: string): Ball;
    list: Array<number>;
}
declare class Ball {
    static list: Array<number>;
    constructor(name: string);
}
declare function getBall(className: IB): void;
//# sourceMappingURL=ClassInterface.d.ts.map