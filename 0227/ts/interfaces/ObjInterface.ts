// var obj:{a:number,b:number}={a:1,b:2}

// 1、普通的对象接口
// interface IObj{
//     a:number;//可以以分号或者逗号结束
//     b:number;
// }
// var obj:IObj={a:1,b:2};

// 2、可选属性对象接口
// interface IObj{
//     a:number;
//     b?:number;//可选属性
// }

// var  obj:IObj={a:1,b:2};


// function fn(a:number,b:number){
//     console.log(a,b);
// }
// fn(obj.a,obj.b as number);

// 3、只读属性
// interface IObj{
//     a:number;
//     readonly b:number;
//     // readonly b?:number; //不建议将只读属性设为可选属性
// }

// var o:IObj={a:1,b:2};
// o.a=10;
// o.b=20;

// 4、对象的方法
// interface IObj{
//     a:number;
//     // 对象的方法接口
//     play:(a:number)=>void;
// }
// interface IObj{
//     a:number;
//     // 对象方法只读不能修改
//     readonly play:(a:number)=>void;
// }
// var o:IObj={
//     a:1,
//     play(a){
//         console.log("a")
//     }   
// }


// 5、对象的键
// 1)、普通的键值
// interface IObj{
//     [key:string]:number|string;
// }

// var o:IObj={};
// o.a=10;
// o.b=20;
// o.c="a";
// 2)只读键名
// 可以在对象初始化的时候设置对象，之后这个对象，不能添加新属性，不能删除属性，不能修改属性值
// interface IObj{
//     readonly [key:string]:number;
// }
// var o:IObj={a:1,b:2};

// 3）可选属性
// interface IObj{
//     // 使用key键时不能用可选属性
//     [key:string]?:number;
// }

// 4)混合属性
// interface IObj{
//     a:number;//要求了对象必须至少有一个属性a,这个属性的值类型必须符合value对应的类型
//     [key:string]:number;
// }
// var o:IObj={a:1};


// interface IObj{
//     readonly a:number;//当只读属性和key属性混合时，考虑只读属性不能修改不能删除
//     [key:string]:number;
// }
// var o:IObj={a:1,b:2};
// delete o.b;
// delete o.a;
// o.a=10;

// interface IObj{
//     a?:number;//错误的 在key属性时，不能使用可选属性
//     [key:string]:number;
// }

// 6、对象的复合型接口

// interface IObj{
//     name:string;
//     age:number;
//     sex:string;
//     children?:Array<IObj>
// }

// var obj:IObj={
//     name:"xietian",
//     age:30,
//     sex:"男",
//     children:[
//         {name:"aa",age:10,sex:"男",children:[{name:"abc",age:2,sex:"男"}]},
//         {name:"bb",age:12,sex:"女"},
//         {name:"cc",age:15,sex:"男"},
//     ]
// }


// interface IO1{
//     g:number;
//     h:number;
// }
// interface IO2{
//     d:number;
//     e:number;
//     f:IO1
// }
// interface IO3{
//     a:number;
//     b:number;
//     c:IO2
// }
// var obj:IO3={
//     a:1,
//     b:2,
//     c:{
//         d:3,
//         e:4,
//         f:{
//             g:5,
//             h:6
//         }
//     }
// }

// interface IObj{
//     a:number;
//     b:number;
//     c:{
//         d:number;
//         e:number;
//         f:{
//             g:number;
//             h:number
//         }
//     }
// }
// var obj:IObj={
//     a:1,
//     b:2,
//     c:{
//         d:3,
//         e:4,
//         f:{
//             g:5,
//             h:6
//         }
//     }
// }


// interface IObj{
//    [key:string]:number|IObj;
// }
// var obj:IObj={
//     a:1,
//     b:2,
//     c:{
//         d:3,
//         e:4,
//         f:{
//             g:5,
//             h:6
//         }
//     }
// }

// function fn(a:number){

// }
// fn(obj.a)

// console.log((obj.c as IObj).d)

// 7、对象的索引属性和Symbol属性
// 当前对象的属性仅为数值索引
// interface IObj{
//     [index:number]:string|number;
//     // symbol 小写是类型
//     [key:symbol]:string|number;
// }
// var o:IObj={
//     1:10,
//     2:20,
//     // 大写Symbol创建Symbol
//     [Symbol("a")]:10
// }

// 8、或者类型 |
// interface IO1{
//     a:number;
//     b:number;
// }
// interface IO2{
//     c:string;
//     d:string;
// }
// var o:IO1|IO2={a:1,b:2}
// o.a=10;
// o={c:"a",d:"b"}
// 修改单独属性是不可以的
// var o1:IO2=<IO2>o;
// o1.c="a";
// o1.d="d"

// 9、联合类型  &
// interface IO1{
//     a:number;
//     b:number;
// }
// interface IO2{
//     c:number;
//     d:number;
// }

// var o:IO1={a:1,b:2};
// var o1:IO2={c:3,d:4};

// var o2:IO1&IO2={a:1,b:2,c:3,d:4}

// function fn(o1:IO1,o2:IO2):IO1&IO2
// {
//     var o:IO1&IO2=<IO1&IO2>{};
//     Object.assign(o,o1,o2);
//     return o;
// }

// 10、keyof 键的归属
// interface IObj{
//     a:number;
//     b:number;
// }

// var o:IObj={a:1,b:2};
// var key:keyof IObj="a";//keyof 对象接口 这个变量是这个对象接口中的键名
// key="b";
// key="c";
// console.log(o[key])
// var key:keyof IObj;
// for(key in o){
//     console.log(o[key]);
// }

// for(var key in o){
//     // console.log(o[key as keyof IObj])
//     console.log(o[<keyof IObj>key])
// }

// 枚举不能使用keyof
// const enum CLOLR{RED="red",BLUE="blue",GREEN="GREEN"};
// var key:keyof CLOLR="red";
// for(key in CLOLR){
//     console.log(key)
// }


// ReadOnly类型
// 只读数组，数组方法中提出了修改原数组的方法，这个数组的元素不能修改，不能删除，不能添加
// var arr:ReadonlyArray<number>=[1,2,3,4,5];
// console.log(arr[0]);
// arr.push(1)
// arr.splice(2,1)

// 只读Set，不能添加删除元素
// var s:ReadonlySet<number>=new Set([2,3,5,6,7]);

// 只读的Map
// var m:ReadonlyMap<string,number>=new Map([["a",1],["b",2]]);

// for(var [key,value] of m){
//     console.log(key,value);
// }
