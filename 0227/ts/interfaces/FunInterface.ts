// 1、普通函数的接口
// var fn:(a:number,b:number)=>number=(a,b)=>a+b;
// var fn:(a:number,b:number)=>number=function(a,b){
//     return a+b;
// }

// interface IFn{
//     (a:number,b:number):number;
// }

// var fn:IFn=(a,b)=>a+b;

// 2、参数也是一个函数的接口
// a是一个数值,f是一个回调函数 有一个参数，并且参数是数值，返回一个数值结果

// interface IFn{
//     (a:number,f:(a:number)=>number):number
// }

// interface IFn1{
//     (a:number):number;
// }
// interface IFn2{
//     (a:number,f:IFn1):number;
// }
// var fn:IFn2=(a,f)=>{
//     if(a>5){
//       return  f(a);
//     }else{
//         a-=5;
//      return   f(a)
//     }
// }

// 3、函数返回Promose对象
// interface IFn{
//     (time:number):Promise<number>
// }

// Promise<>中的类型是Promise中resolve返回结果的类型
// var delay:IFn=function(time){
//     return new Promise(function(resolve:(n:number)=>void,reject:()=>void){
//         setTimeout(function(){
//             resolve(10);
//         },time)
//     })
// }

// interface ILoadImage{
//     (src:string):Promise<HTMLImageElement>
// }

// var loadImage:ILoadImage=src=>new Promise((resolve,reject)=>{
//     var img=new Image();
//     img.src=src;
//     img.onload=e=>{
//         resolve(img);
//     }
//     img.onerror=e=>{
//         reject(src);
//     }
// })

// interface IF{
//     (time:number):Promise<void>
// }
// 4、async函数的接口
// var fn:IF=async (time)=>{
//     var a:number=await delay(time).catch(e=>{}) as number;
//     console.log(a);
// }

// 5、多样性的函数接口 
// 1)、全必填参数
// interface IFn1{
//     (a:number,b:number,c:number):void;
// }
// 2）、选填参数
// interface IFn2{
//     (a:number,b:number,c?:number):void;
// }
// 3）、包含属性的函数
// interface IFn5{
//     (a:number,b:number,c?:number):void;
//     num:number;
// }

// var fn:IFn5=((a,b,c)=>{

// })
// 4）、剩余参数的接口
// interface IFn3{
//     (a:number,b:number,...arg:Array<number>):void;
// }

// 5）、使用元组的参数接口
// 如果arg使用元组类型，意味着这个函数参数是一个固定的参数项
// interface IFn4{
//     (a:number,b:number,...arg:[string,number,string]):void;
// }

// var fn:IFn4=(a,b,...arg)=>{
//     console.log(a,b,arg);
// }


// // 这种复合型的函数，需要开始使用断言
// var fn:IFn5=<IFn5>((a,b,c)=>{

// })
// fn.num=10;

// fn(1,2,3);//IFn1
// fn(1,2);//IFn2
// fn(1,2);//IFn5

// 6）、使用元组统一设定参数类型，可以避免对于函数中参数名的不同
// interface IFn{
//     (...arg:[number,string,number,number,string]):void
// }
// var fn:IFn=(a,b,c,d,e)=>{

// }
// var fn1:IFn=(num1,str1,num2,num3,str2)=>{

// }


// 6、函数中this参数
// this参数 当使用回调函数或者事件触发时，可能会用到this，这是this报错，可以将this放在函数参数第一个，并且设置this类型为void
// var obj={
//     init(){
//         document.addEventListener("click",this.clickHandler);
//     },
//     clickHandler(this:void,e:MouseEvent){
//         console.log(this);
//     }
// }
// obj.init();



// 7、重载
// function fn(a:number,b:number):number;
// function fn(a:string,b:string):string;
// function fn(a:Array<number>,b:Array<number>):Array<number>;
// function fn(a:number|string|Array<number>,b:number|string|Array<number>):number|string|Array<number>
// {
//     if(Array.isArray(a) && Array.isArray(b)){
//         return a.concat(b)
//     }else if(typeof a==="string" && typeof b==="string"){
//         return a+","+b;
//     }else if(typeof a==="number" && typeof b==="number"){
//         return a+b;
//     }
//     throw new Error("错误参数类型")
   
// }

// var arr=[1,2,3,4];
// arr.reduce()

// class Arrays{
//     splice(start:number,deleteCount?:number):Array<number>;
//     splice(start:number,deleteCount:number,...arg:Array<number>):Array<number>;
//     splice(start:number,deleteCount:number|undefined,...arg:Array<number>):Array<number>{
//         return [1,2,3]
//     }
// }

// var arr1=new Arrays();
// arr1.splice()

// a参数可以是number或者boolean或者数组或者对象，函数执行完成会转换为字符串
// function toStrings(a:number):string;
// function toStrings(a:boolean):string;
// function toStrings(a:Array<any>):string;
// function toStrings(a:object):string;
// function toStrings(a:number|boolean|Array<any>|object):string
// {
//     return ""
// }


