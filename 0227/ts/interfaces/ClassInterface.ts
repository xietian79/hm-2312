// interface IUpdate{
//     num:number;
//     update():void;
// }

// // implements 实现接口
// class Box implements IUpdate{
//     num: number=1;
//     constructor(){

//     }
//     update(): void {
        
//     }
// }
// class Ball{
//     play(){

//     }
// }
// class Rect{
//     num:number=1;
//     constructor(){

//     }
//     update(){
//         console.log("aa")
//     }
// }

// var b:Box=new Box();
// var c:Ball=new Ball();
// var r:Rect=new Rect();

// var arr:Array<IUpdate>=[];
// arr.push(b);
// // arr.push(c);
// arr.push(r);


// class A{
//     num:number=10;
// }
// class B extends A{
//     str:string="aa";
// }

// var arr:Array<A>=[];
// var a:A=new A();
// var b:B=new B();
// console.log(b.num)
// arr.push(a);
// arr.push(b);

// 类的关系
// class A{
//     num:number=10;
// }
// class B{
//     num:number=1;
// }

// var a:A=new A();
// var b:B=new B();
// var arr:Array<{num:number}>=[];
// arr.push(a);
// arr.push(b);


// interface IA{
//     num:number;
// }
// class A implements IA{
//     num:number=10;
// }
// class B implements IA{
//     num:number=1;
// }

// var a:A=new A();
// var b:B=new B();
// var arr:Array<IA>=[];
// arr.push(a);
// arr.push(b);


// 2、类中接口属性问题
// interface IA{
//     num?:number;//可选属性
// }
// interface IA{
//     readonly num:number;//只读属性
// }

// class A implements IA{
//     readonly num: number=1;
// }

// var a:A=new A();
// console.log(a.num)
// a.num=10;

// interface IA{
//     // 键值属性
//     [key:string]:any;
// }

// class Emiter extends Event implements IA{
//     [key:string]:any;
// }
// var evt:Emiter=new Emiter("click");
// evt.a=10;


// 3、多接口的实现
// interface IA{
//     a:number;
// }
// interface IB{
//     b:number;
// }
// interface IC{
//     c:number;
// }

// // 使用,分隔接口，可以实现一个类实现多个接口
// class A implements IA,IB
// {
//     a:number=1;
//     b:number=2;
// }

// class B implements IB,IC
// {
//     b:number=2;
//     c:number=3;
// }
// class C implements IC,IA{
//     c:number=1;
//     a:number=0;
// }

// var a:A=new A();
// var b:B=new B();
// var c:C=new C();

// var arr1:Array<IA>=[];
// arr1.push(a);
// // arr1.push(b);//不能放入
// arr1.push(c);

// var arr2:Array<IB>=[];
// arr2.push(b);
// arr2.push(a);
// arr2.push(c);//不能放入

// var arr3:Array<IC>=[];
// arr3.push(a);//不能放入
// arr3.push(b);
// arr3.push(c);


// 4、接口也是可以继承
// interface IA{
//     a:number;
// }
// interface IB extends IA{
//     b:number;
// }

// class Box implements IB{
//     a:number=1;
//     b:number=2;
// }

// 接口继承类，只是继承了类的属性和方法名及参数类型，并不会继承类中这个属性的值或者方法中内容
// class Box{
//     a:number=1;
//     play(){
//         console.log("aa")
//     }
// }

// interface IA extends Box{
//     b:number;
// }
// class Ball implements IA{
//     b:number=1;
//     a:number=2;
//     play(): void {
        
//     }
// }


// 5、静态属性和方法的接口
// 构造函数接口
// interface IA{
//     new(name:string):Box;
//     list:Array<number>;
//     play():void;
// }
// // 类不直接实现构造函数接口
// class Box{
//     static list:Array<number>=[];
//     constructor(name:string){

//     }
//     static play():void{

//     }
// }

// function getBox(className:IA){
//     var a:Box=new className("a");
//     console.log(a);
// }
// // 类名作为参数给入，className这个变量就是类名
// getBox(Box);


// function Box(){

// }
// Box.a=1;
// Box.play=function(){

// }
// Box.prototype.b=2;
// Box.prototype.run=function(){

// }



// 1、函数接口
interface IFn{
    (a:number):void;
}
// 函数接口的实现
var fn:IFn=function(a){

}
// 2、对象接口
interface IA{
    a:number;
    play():void;
}
var o:IA={
    a:1,
    play(){
        console.log("aa")
    }
}
// 3、类接口
interface IUpdate{
    update():void;
}

class Box implements IUpdate{
    update(): void {
        
    }
}

// 4、构造函数接口
interface IB{
    new (name:string):Ball;
    list:Array<number>;
}


class Ball{
    static list:Array<number>=[1,2,3];
    constructor(name:string){

    }
}

function getBall(className:IB){

}
getBall(Ball)