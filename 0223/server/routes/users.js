var express = require('express');
var router = express.Router();
var  {selectAll,insert,remove,login,update,selectInfo}=require("../business/sql");
var token=require("jsonwebtoken");


router.post('/list',async function(req, res, next) {
  if(! await auth(req,res)) return;
    var result=await selectAll().catch(e=>{});
    if(result){
        res.send({err:null,data:result});
    } else{
      res.send({err:1,message:"获取列表错误"})
    }
});

router.post("/register",async function(req,res,next){
  // console.log(req.body)
  var result=await insert(req.body).catch(e=>{});
  if(result){
    res.send({err:null,message:"register success!"});
  }else{
    res.send({err:1,message:"register reject!"});
  }
  
})

router.post("/login",async function(req,res,next){
  var {user,password}=req.body;
    var result=await login(user,password).catch(e=>{})
    if(!result){
        res.send({err:1,message:"登录失败"})
    }else{
       var keys=token.sign(req.body,"123456");
      //  设定响应头允许设置自定义的响应内容X-Session，X-User
        res.set("Access-Control-Expose-Headers",["X-Session","X-User"])
        // 设置响应头添加X-Session的值
        res.set("X-Session",keys);
        // 设置响应头添加X-Session的值
        res.set("X-User",user)
        res.send({err:null,message:"登录成功"})
    }

})

router.get("/logout",function(req,res,next){
    res.send({err:null,message:"退出登录"})
})

router.get("/editinfo",async function(req,res,next){
      if(!req.query.pid){
        res.send({err:1,message:"错误的数据"});
      }
      if(! await auth(req,res)) return;
      var result =await selectInfo(Number(req.query.pid)).catch(e=>{});
      if(!result || result.length===0){
        res.send({err:1,message:"错误的数据"});
      }else{
          res.send({err:null,data:result[0]})
      }
})

router.post("/remove",async function(req,res,next){
    // if(!req.headers["x-session"]){
    //   res.send({err:1,message:"登录过期"})
    //   return;
    // }
    // var {user,password}=token.verify(req.headers["x-session"],"123456");
    // var result=await login(user,password).catch(e=>{})
    // if(!result){
    //     res.send({err:1,message:"未登录"})
    //     return;
    // }
    if(! await auth(req,res)) return;
    await remove("`pid`="+req.body.pid).catch(e=>{});
    var result=await selectAll().catch(e=>{});
    if(!result){
      res.send({err:1,message:"获取列表错误"});
    }else{
      res.send({err:null,data:result});
    }
})
router.post("/update",async function(req,res,next){
  var pid=Number(req.body.pid);
  req.body.age=Number(req.body.age);
  delete req.body.pid;
  if(! await auth(req,res)) return;
  var result=await update(req.body,"`pid`="+pid).catch(e=>{});
  if(result){
      res.send({err:null,message:"修改成功"});
  }else{
    res.send({err:1,message:"修改失败"})
  }
})


async function auth(req,res){
  if(!req.headers["x-session"]){
    res.send({err:1,message:"登录过期"})
    return false;
  }
  var {user,password}=token.verify(req.headers["x-session"],"123456");
  var result=await login(user,password).catch(e=>{})
  if(!result){
      res.send({err:1,message:"未登录"})
      return false;
  }
  return true;
}
module.exports = router;
