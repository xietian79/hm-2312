export default async function getPage(app){
    var result=await fetch("http://localhost:4000/users/list",{
        method:"POST",
        headers:{
            "X-Session":localStorage.token || ""
        }
    });
    result=await result.json();
    if(result.err){
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/login"
    }else{
        renderTable(result.data,app)
    }
   
}
function renderTable(data,app){
    app.innerHTML=`
    <table class="table table-hover">
        ${data.reduce((v,t)=>{
            return v+`<tr>${Object.keys(t).reduce((value,key)=>{
                if(key==="pid") return value;
                return value+`<td>${t[key]}</td>`
            },"")}<td><button class='remove' data='${t.pid}'>&times;</button></td><td><button class='edit' data='${t.pid}'>编辑</button></td></tr>`
        },"")}
    </table>
    `
    document.querySelector("#username").innerHTML=localStorage.user;
    app.addEventListener("click",clickHandler);
}

function clickHandler(e){
    if(!/remove|edit/.test(e.target.className))return;
    var pid=Number(e.target.getAttribute("data"));
    if(e.target.className==="remove"){
        removeUser(pid,e.currentTarget)
    }else if(e.target.className==="edit"){
        location.href=location.href.replace(/\?.*|\#.*/,"")+"?pid="+pid+"#/edit"
    }
}

async function removeUser(pid,app){
    var result=await fetch("http://localhost:4000/users/remove",{
        method:"POST",
        body:JSON.stringify({pid:pid}),
        headers:{
            "Content-Type":"application/json",
            "X-Session":localStorage.token || ""
        }
    });
    result=await result.json();
    if(result.err){
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/login"
    }else{
        renderTable(result.data,app)
    }
}
