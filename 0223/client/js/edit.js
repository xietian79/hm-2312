export default function getPage(app){
    var pid= Number(location.search.slice(1).split("=")[1]);
    getEditInfo(pid)
}

async function getEditInfo(pid){
    var result=await fetch("http://localhost:4000/users/editinfo?pid="+pid,{
        headers:{
            "X-Session":localStorage.token || ""
        }
    }).catch(e=>{});
    result=await result.json();
    if(result.err){
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/login"
    }else{
        setPage(result.data);
    }

}
function setPage(data){
    app.innerHTML= ` <h2 class="text-center title">修改用户信息</h2>
    <form class="form-horizontal col-md-5 col-md-offset-3" data='${data.pid}'>
        <div class="form-group">
          <label for="user" class="col-sm-2 control-label">User</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="user" name="user" placeholder="User" value='${data.user}'>
          </div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value='${data.password}' autocomplete>
          </div>
        </div>
       
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="name" name="name" value='${data.name}' placeholder="Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Sex</label>
            <div class="radio-inline">
                <label>
                  <input type="radio" name="sex" id="optionsRadios1" value="男" ${data.sex==="男"? "checked" : ""}>
                  男
                </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="sex" id="optionsRadios2" value="女"  ${data.sex==="女"? "checked" : ""}>
                  女
                </label>
              </div>
          </div>
          <div class="form-group">
            <label for="age" class="col-sm-2 control-label">Age</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="age" name="age" value='${data.age}' placeholder="Age">
            </div>
          </div>
          <div class="form-group">
            <label for="tel" class="col-sm-2 control-label">Tel</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tel" name="tel" value='${data.tel}' placeholder="Tel">
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="email" name="email" value='${data.email}' placeholder="Email">
            </div>
          </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>`
      document.querySelector("#username").innerHTML=localStorage.user;
      var form=app.querySelector("form");
      form.addEventListener("submit",submitHandler);
}

function submitHandler(e){
    e.preventDefault();
    var fd=new FormData(e.currentTarget);
    fd.set("pid",Number(e.currentTarget.getAttribute("data")))
    fd.set("age",Number(fd.get("age")));
    submitServer(fd);
}

async function submitServer(data){
    var result=await fetch("http://localhost:4000/users/update",{
        method:"POST",
        body:data,
        headers:{
            "X-Session":localStorage.token || ""
        }
    });
    result=await result.json();
    if(result.err){
        alert("修改失败!")
    }else{
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/list"
    }
}