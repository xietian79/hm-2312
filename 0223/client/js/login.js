export default function getPage(app){
    app.innerHTML=  `<h2 class="text-center title">用户注册</h2>
    <form class="form-horizontal col-md-5 col-md-offset-3">
        <div class="form-group">
          <label for="user" class="col-sm-2 control-label">User</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="user" name="user" placeholder="User">
          </div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Sigin In</button>
            <a  class="btn btn-default" href='#/register'>Register</a>
          </div>
        </div>
      </form>`
      var form=app.querySelector("form");
      form.addEventListener("submit",submitHandler);
}
function submitHandler(e){
    e.preventDefault();
   var fd=new FormData(e.currentTarget);
   submitServer(fd);
}

async function submitServer(data){
    var result=await fetch("http://localhost:4000/users/login",{
        method:"POST",
        body:data
    });
    // 获取服务端返回的响应头，并且从响应头中获取x-session这个就是加密后token，再拿到用户名
    for(var [key,value] of result.headers){
       if(key==="x-session"){
          localStorage.token=value;
       }else if(key==="x-user"){
          document.querySelector("#username").innerHTML=value;
          localStorage.user=value;
       }
    }
    result=await result.json();
    if(result.err){
        alert("登录失败")
    }else{
   
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/list"
    }
}