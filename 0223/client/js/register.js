export default function getPage(app){
    app.innerHTML= ` <h2 class="text-center title">用户注册</h2>
    <form class="form-horizontal col-md-5 col-md-offset-3">
        <div class="form-group">
          <label for="user" class="col-sm-2 control-label">User</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="user" name="user" placeholder="User">
          </div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete>
          </div>
        </div>
       
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Sex</label>
            <div class="radio-inline">
                <label>
                  <input type="radio" name="sex" id="optionsRadios1" value="男" checked>
                  男
                </label>
              </div>
              <div class="radio-inline">
                <label>
                  <input type="radio" name="sex" id="optionsRadios2" value="女">
                  女
                </label>
              </div>
          </div>
          <div class="form-group">
            <label for="age" class="col-sm-2 control-label">Age</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="age" name="age" placeholder="Age">
            </div>
          </div>
          <div class="form-group">
            <label for="tel" class="col-sm-2 control-label">Tel</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tel" name="tel" placeholder="Tel">
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="email" name="email" placeholder="Email">
            </div>
          </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Register</button>
            <a  class="btn btn-default" href='#/login'>Sigin In</a>
          </div>
        </div>
      </form>`
      var form=app.querySelector("form");
      form.addEventListener("submit",submitHandler);
}

function submitHandler(e){
    e.preventDefault();
    var fd=new FormData(e.currentTarget);
    // for(var [key,value] of fd){
    //     if(!authForm(key,value)){
    //         // 聚焦错误的部分
    //         document.getElementsByName(key)[0].focus();
    //         return;
    //     }
    // }
    submitServer(fd)
}

function authForm(name,value){
    switch(name){
        case "user":
           return  /^\w{8,16}$/.test(value)
        case "password":
            return /^(?=\D]+\d)(?=.*[a-z])(?=.*[A-Z])\w{8,16}$/.test(value);
    }
}

async function submitServer(data){
    var result=await fetch("http://localhost:4000/users/register",{
        method:"POST",
        body:data
    });
    result=await result.json();
    if(result.err){
        alert("注册失败!")
    }else{
        location.href=location.href.replace(/\?.*|\#.*/,"")+"#/login"
    }
}