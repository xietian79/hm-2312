var express = require('express');
var router = express.Router();
var {selectAll,insert,login,update,remove}=require("../business/sql")

/* GET users listing. */
router.get('/list',async function(req, res, next) {
   var result=await selectAll().catch(e=>{});
   res.send(result);
});
router.post("/register",async function(req,res,next){
 
  var result=await insert(req.body).catch(e=>{})
  res.send(result)
})
router.post("/login",async function(req,res,next){
    var {user,password}=req.body;
    var result=await login(user,password).catch(e=>{});
    if(result){
      res.send("登录成功")
    }else{
      res.send("登录失败")
    }
})

router.get("/update",async function(req,res,next){
    var result=await update({age:18,tel:"16618096780"},"`user`='xietian'").catch(e=>{});
    console.log(result);
    res.send(result)
})

router.get("/remove",async function(req,res,next){
  var result=await remove("`user`='xietian'").catch(e=>{});
  res.send(result);
})

module.exports = router;
