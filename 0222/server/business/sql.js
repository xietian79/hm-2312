const mysql = require("mysql");
const Connection = require("mysql/lib/Connection");

var pool = mysql.createPool({
  port: 3306,
  host: "localhost",
  database: "game",
  user: "root",
  password: "123456",
});

function getConnection() {
  return new Promise(function (resolve, reject) {
    pool.getConnection(function (err, connect) {
      if (err) {
        reject();
      } else {
        resolve(connect);
      }
    });
  });
}

function selectAll(tableName="user") {
  return new Promise(function (resolve, reject) {
    getConnection()
      .then(function (connect) {
        connect.query("SELECT * FROM `"+tableName+"` WHERE 1", function (err, result) {
            if(err){
                reject();
            }else{
                resolve(result);
            }
            connect.destroy();
        });
      })
      .catch(function (err) {
            reject();
      });
  });
}


function insert(data,tableName="user") {
    return new Promise(function (resolve, reject) {
      getConnection()
        .then(function (connect) {
           var keys=Object.keys(data).reduce((v,t)=>v+"`"+t+"`"+", ","").slice(0,-2)
          connect.query("INSERT INTO `"+tableName+"`("+keys+") VALUES ("+Object.keys(data).fill("?").join()+")",Object.values(data), function (err, result) {
              if(err){
                  reject();
              }else{
                  resolve(result);
              }
              connect.destroy();
          });
        })
        .catch(function (err) {
              reject();
        });
    });
  }


function login(user,password,tableName="user") {
    return new Promise(function (resolve, reject) {
      getConnection()
        .then(function (connect) {
          connect.query("SELECT * FROM `"+tableName+"` WHERE `user`='"+user+"' AND `password`='"+password+"'", function (err, result) {
              if(err){
                  reject();
              }else{
                if(result.length>0){
                    resolve(result);
                }else{
                    reject();
                }
              }
              connect.destroy();
          });
        })
        .catch(function (err) {
              reject();
        });
    });
}


function update(data,condition,tableName="user") {
    return new Promise(function (resolve, reject) {
      getConnection()
        .then(function (connect) {
            var str=Object.keys(data).reduce((v,key)=>{
                return v+"`"+key+"`="+(typeof data[key]==="string" ? "'"+data[key]+"'" : data[key])+", "
            },"").slice(0,-2);
          connect.query("UPDATE `"+tableName+"` SET "+str+" WHERE "+condition, function (err, result) {
              if(err){
                  reject();
              }else{
                resolve(result)
              }
              connect.destroy();
          });
        })
        .catch(function (err) {
              reject();
        });
    });
}

function remove(condition,tableName="user") {
    return new Promise(function (resolve, reject) {
      getConnection()
        .then(function (connect) {
         
          connect.query("DELETE FROM `"+tableName+"` WHERE "+condition, function (err, result) {
              if(err){
                  reject();
              }else{
                resolve(result)
              }
              connect.destroy();
          });
        })
        .catch(function (err) {
              reject();
        });
    });
}

exports.selectAll=selectAll;
exports.insert=insert;
exports.login=login;
exports.update=update;
exports.remove=remove;