import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import path from "path";
export default {
  mode: process.env.mode,
  devtool: "source-map",
  entry: {
    "./js/app": "./src/main.js",
  },
  performance: {
    hints: false
  },
  output: {
    path: path.join(path.resolve(), "./dist"),
    // name就是entry中设置属性名 "./js/app"
    filename:
      "[name]-[fullhash]" +
      (process.env.mode === "production" ? ".min" : "") +
      ".js",
  },
  module:{
    rules:[
      {test:/\.svg$/i,type: 'asset'},
      {test:/\.scss$/i,use:["style-loader","css-loader","sass-loader"]}
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(path.resolve(), "./public/html/index.html"),
      inject: true,
    }),
    new CleanWebpackPlugin(),
  ],
  devServer: {
    static: {
      directory: path.join(path.resolve(), "./public"),
      publicPath: path.join(path.resolve(), "./dist"),
    },
    port: 5500,
    proxy:[
        {context: ['/a'],target: "http://10.9.45.81:3000",pathRewrite: { "/a": "" }},
        {context: ['/b'],target: "http://121.89.205.189:3000",pathRewrite:{"/b":""}},
        
    ]
  },
};
