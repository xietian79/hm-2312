import ins from "@/business/request";
import { useMain } from "@/stores/Main";
export async function getUserMenuList(){
    const store=useMain();
    const result=await ins.get("/api/role/"+store.userInfo.roleid+"/menuauths");
    if(result){
        return result.data.data;
    }
    return null;
}

export async function getUserList(query?:{page?:string|number,size?:string|number,user:string,name:string}) {
    const querystring=Object.keys(query).reduce((v,k)=>v+k+"="+query[k]+"&","?").slice(0,-1);
    const result=await ins.get("/api/user/list"+querystring);
    if(result){
        return result.data.data;
    }
    return null
}

export async function getRoleList(){
    const result=await ins.get("/api/role/list");
    if(result){
        return result.data.data;
    }
    return null;
}
export async function addUser(fd:FormData)
 {
     const result=await ins.post('/api/role/create',fd);
     if(result){
        return result.data.data;
    }
    return null;
}
export async function  removeUserList(data:{ids:Array<number>}) {
    const result=await ins.post('/api/user/delbatch',data);
    if(result){
        return result.data.data;
    }
    return null;
}
export async function removeSingleUser(id:number|string) {
    const result=await ins.delete("/api/user/"+id);
    if(result){
        return result.data.data;
    }
    return null;
}

export async function getRoleTouser(id:string|number){
    const result =await ins.get("/api/roleTouser/"+id);
    if(result){
        return result.data.data;
    }
    return null;
}
export async function changeRoleName(id:string|number,fd:FormData){
    const result=await ins.put("/api/role/edit/"+id,fd);
    if(result){
        return result.data.data;
    }
    return null;
}

export async function getRuleList(){
    const result=await ins.get("/api/auth/list");
    if(result){
        return result.data.data;
    }
    return null;
}
export async function getRoleRuleList(id:string|number) {
    const result=await ins.get('/api/role/'+id+'/auths');
    if(result){
        return result.data.data;
    }
    return null;
}
export async function saveRuleRule(id:string|number,list:number[]) {
    const result=await ins.post('/api/role/setAuthNode/'+id,list);
    if(result){
        return result.data.data;
    }
    return null;
}

export async function getAuthList() {
    const result=await ins.get("/api/auth/list");
    if(result){
        return result.data.data;
    }
    return null;
}
export async function getTopAuthList() {
    const result=await ins.get('/api/auth/topnode');
    if(result){
        return result.data.data;
    }
    return null;
}