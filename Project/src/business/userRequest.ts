import ins from "@/business/request";

export async function login(formData:{userName:string,password:string}):void{
    await ins.post("/api/user/login",formData)
}

export async function getAuth(tel:string):Promise<{code:number,data:number,msg:string}>
{
    const result=await ins.post("/api/user/phoneNumberCode",{phone:tel}).catch(()=>{});
    return result.data;
}
export async function telLogin(formData:{phone:string,code:string}):void{
    const result= await ins.post("/api/user/loginPhone",formData);
    console.log(result);
}