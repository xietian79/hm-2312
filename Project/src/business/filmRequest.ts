import ins from "@/business/request";

export async function loadFile(fd){
    const result=await ins.post("/api/film/uploadChunkVideo",fd);
    if(result){
        return result.data.data;
    }
    return null;
}
export async function loadComplate(data:{name:string,ext:string}){
    const result=await ins.post("/api/film/mergeChunkVideo",data);
    if(result){
        return result.data.data;
    }
    return null;
}