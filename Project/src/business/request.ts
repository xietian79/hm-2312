import axios from "axios";
import { useMain } from "@/stores/Main";
import {messageSuccess} from "@/utils/utils";
// 在vite环境下获取.env的环境变量
// console.log(import.meta.env.MODE)
// const ins=axios.create({
//     baseURL:import.meta.env.MODE =='development' ? '/' : "http://localhost:5001/a",
//     timeout:4000
// });
const ins=axios.create({});

ins.interceptors.request.use((value)=>{
    console.log(value);
    value.headers["token"]="aaaa"
    return value
},error=>{

})
ins.interceptors.response.use((value)=>{
    // 收到所有axios发送数据后得到返回的消息
    if(value.data.data.token){
        const store=useMain();
        store.userInfo=value.data.data;
        messageSuccess(value.data.msg);
    }
    // 继续向后运行
    return value;
},error=>{
    messageSuccess(error.data.msg);
    return error;
})

export default ins;