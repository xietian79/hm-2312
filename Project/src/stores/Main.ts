import { defineStore } from 'pinia'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import en from 'element-plus/dist/locale/en.mjs'

export const useMain = defineStore('main', {
    persist:{
        key:"user",
        storage:sessionStorage
    },
    state(){
        return {
            locale:zhCn,
            userInfo:null
        }
    },
    actions:{
        setLanguage(type:"zhCn"|'en'):void{
            this.locale= type=="zhCn" ? zhCn : en;
        }
    }
})
