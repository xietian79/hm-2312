import { ElMessage } from 'element-plus'

export function messageSuccess(msg: string): void {
    ElMessage({
        message: msg,
        type: 'success',
    })
}
export function messageError(msg: string): void {
    ElMessage.error(msg)
}
