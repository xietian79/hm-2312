import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from "element-plus";
import App from './App.vue'
import router from './router'
import "element-plus/dist/index.css";
import IconElement from './impotElement/IconElement';
import piniaPresist from "pinia-plugin-persistedstate";
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import "@/assets/base.styl";
import "default-passive-events";


const app = createApp(App)
app.use(ElementPlus,{locale:zhCn});
const pinia=createPinia();
pinia.use(piniaPresist);
app.use(pinia)
app.use(router)
for(const [key,value] of Object.entries(IconElement)){
    app.component(key,value);
}
app.mount('#app')


// import Mock,{Random} from "mockjs";
// console.log(Mock.mock({
//     errno:null,
//     msg:"ok",
//     data:{
//         user:Mock.mock("@name"),
//         name:Mock.mock("@cname"),
//         token:Mock.mock(/\w{36,58}/),
//         "list|1-10":[
//            {
//             "user|+1":"@name",
//             "name|+1":"@cname",
//             "age|1-100":100,
//             "sex|1":["男","女"],
//             "tel":/1[2-9]\d{9}/,
//             "email":"@email"
//             }
//         ]
//     }
// }))