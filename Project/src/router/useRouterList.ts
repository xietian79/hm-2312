import film from "@/router/film";
import sys from "@/router/sys";
import info from "@/router/info";
import router from "@/router/index";
export function useRouterList(list){
//    console.log( [...film,...sys,...info],list)
    [...film,...sys,...info].filter(item=>list.includes(item.path)).forEach(item=>{
        router.addRoute("main",item);
    });
}
