export default [
    {
        path:"/film/list",
        name:'list',
        component:()=>import("@/views/FilmListView.vue")
    },
    {
        path:"/film/create",
        name:'create',
        component:()=>import("@/views/FilmCreateView.vue")
    }
]