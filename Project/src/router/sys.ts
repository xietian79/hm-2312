export default [
    {
        path:"/sys/user",
        name:'user',
        component:()=>import("@/views/UserView.vue")
    },
    {
        path:"/sys/role",
        name:'role',
        component:()=>import("@/views/RoleView.vue")
    },
    {
        path:"/sys/auths",
        name:'auths',
        component:()=>import("@/views/AuthsView.vue")
    }
]