import LayoutView from '@/views/LayoutView.vue'
import { createRouter, createWebHistory } from 'vue-router'
import { useMain } from '@/stores/Main'
import {useRouterList} from "@/router/useRouterList";
import Page404View from "@/views/Page404View.vue"
let bool=false;

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path:"/",
      name:"main",
      component:LayoutView
    },
    {
      path:"/login",
      name:"login",
      meta:{noLogin:true},
      component:()=>import("@/views/LoginView.vue")
    },
    {path:"/:pathMatch(.*)",component:Page404View}
  ]
})

router.beforeEach((to,from,next)=>{
  // 不是login这个路由的，其他跳转
  if(!to.meta.noLogin){
    const store=useMain();
    // 先判断store中存储userInfo是否有token，如果没有token表示没有登录，跳转到登录页面
    if(!store.userInfo?.token){
      return next("/login")
    }
    if(!bool){
      bool=true;
      useRouterList(store.userInfo.routes);
      return next({path:to.href});
    }
  }
  next();
})
export default router
