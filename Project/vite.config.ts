import { fileURLToPath, URL } from 'node:url'
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import {ElementPlusResolver} from "unplugin-vue-components/resolvers";
import {viteMockServe} from "vite-plugin-mock";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers:[ElementPlusResolver()],
      imports:["vue","vue-router"],
      dts: 'auto-imports.d.ts'
    }),
    Components({
      dirs: ['src/components',"src/views"],
      resolvers:[ElementPlusResolver()],
      extensions: ['vue'],
      dts: 'components.d.ts',
    }),
    viteMockServe()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server:{
    port:5001,
    proxy:{
      "/a":{
        target:"http://10.9.45.81:4000",
        changeOrigin:true,
        rewrite:path=>path.replace(/^\/a/,"")
      }
    },
  }
})
