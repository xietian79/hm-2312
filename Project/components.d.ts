/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    AddAutch: typeof import('./src/components/auth/AddAutch.vue')['default']
    AddUser: typeof import('./src/components/userlist/AddUser.vue')['default']
    App: typeof import('./src/views/App.vue')['default']
    AuthList: typeof import('./src/components/auth/AuthList.vue')['default']
    AuthsView: typeof import('./src/views/AuthsView.vue')['default']
    ButtonGroup: typeof import('./src/components/userlist/ButtonGroup.vue')['default']
    ChangeRule: typeof import('./src/components/role/ChangeRule.vue')['default']
    ChartView: typeof import('./src/components/dashoard/ChartView.vue')['default']
    DashoardView: typeof import('./src/views/DashoardView.vue')['default']
    ElAside: typeof import('element-plus/es')['ElAside']
    ElAvatar: typeof import('element-plus/es')['ElAvatar']
    ElButton: typeof import('element-plus/es')['ElButton']
    ElCard: typeof import('element-plus/es')['ElCard']
    ElCascader: typeof import('element-plus/es')['ElCascader']
    ElCol: typeof import('element-plus/es')['ElCol']
    ElCollapse: typeof import('element-plus/es')['ElCollapse']
    ElCollapseItem: typeof import('element-plus/es')['ElCollapseItem']
    ElConfigProvider: typeof import('element-plus/es')['ElConfigProvider']
    ElContainer: typeof import('element-plus/es')['ElContainer']
    ElDatePicker: typeof import('element-plus/es')['ElDatePicker']
    ElDialog: typeof import('element-plus/es')['ElDialog']
    ElDropdown: typeof import('element-plus/es')['ElDropdown']
    ElDropdownItem: typeof import('element-plus/es')['ElDropdownItem']
    ElDropdownMenu: typeof import('element-plus/es')['ElDropdownMenu']
    ElForm: typeof import('element-plus/es')['ElForm']
    ElFormItem: typeof import('element-plus/es')['ElFormItem']
    ElHeader: typeof import('element-plus/es')['ElHeader']
    ElIcon: typeof import('element-plus/es')['ElIcon']
    ElImage: typeof import('element-plus/es')['ElImage']
    ElInput: typeof import('element-plus/es')['ElInput']
    ElInputNumber: typeof import('element-plus/es')['ElInputNumber']
    ElMain: typeof import('element-plus/es')['ElMain']
    ElMenu: typeof import('element-plus/es')['ElMenu']
    ElMenuItem: typeof import('element-plus/es')['ElMenuItem']
    ElMenuItemGroup: typeof import('element-plus/es')['ElMenuItemGroup']
    ElOption: typeof import('element-plus/es')['ElOption']
    ElPagination: typeof import('element-plus/es')['ElPagination']
    ElRadio: typeof import('element-plus/es')['ElRadio']
    ElRadioGroup: typeof import('element-plus/es')['ElRadioGroup']
    ElRow: typeof import('element-plus/es')['ElRow']
    ElScrollbar: typeof import('element-plus/es')['ElScrollbar']
    ElSelect: typeof import('element-plus/es')['ElSelect']
    ElSubMenu: typeof import('element-plus/es')['ElSubMenu']
    ElTable: typeof import('element-plus/es')['ElTable']
    ElTableColumn: typeof import('element-plus/es')['ElTableColumn']
    ElTabPane: typeof import('element-plus/es')['ElTabPane']
    ElTabs: typeof import('element-plus/es')['ElTabs']
    ElTag: typeof import('element-plus/es')['ElTag']
    ElTree: typeof import('element-plus/es')['ElTree']
    ElUpload: typeof import('element-plus/es')['ElUpload']
    FilmCreateView: typeof import('./src/views/FilmCreateView.vue')['default']
    FilmListView: typeof import('./src/views/FilmListView.vue')['default']
    ImportUser: typeof import('./src/components/userlist/ImportUser.vue')['default']
    LayoutView: typeof import('./src/views/LayoutView.vue')['default']
    LoginView: typeof import('./src/views/LoginView.vue')['default']
    MainView: typeof import('./src/views/MainView.vue')['default']
    MenuList: typeof import('./src/components/layout/MenuList.vue')['default']
    Page404: typeof import('./src/views/Page404.vue')['default']
    Page404View: typeof import('./src/views/Page404View.vue')['default']
    RoleList: typeof import('./src/components/role/RoleList.vue')['default']
    RoleTop: typeof import('./src/components/role/RoleTop.vue')['default']
    RoleView: typeof import('./src/views/RoleView.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    SearchBar: typeof import('./src/components/userlist/SearchBar.vue')['default']
    TelLogin: typeof import('./src/components/login/TelLogin.vue')['default']
    TopBar: typeof import('./src/components/auth/TopBar.vue')['default']
    UploadFilm: typeof import('./src/components/film/UploadFilm.vue')['default']
    UserLogin: typeof import('./src/components/login/UserLogin.vue')['default']
    UserTable: typeof import('./src/components/userlist/UserTable.vue')['default']
    UserView: typeof import('./src/views/UserView.vue')['default']
  }
}
