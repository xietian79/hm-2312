import Mock from 'mockjs'
import type { MockMethod } from 'vite-plugin-mock'

export default [
  // 文件切片上传
  {
    url: '/api/film/uploadChunkVideo',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 切片上传成功后,通知服务器合并请求
  {
    url: '/api/film/mergeChunkVideo',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          url: 'http://localhost:5173/video.mp4',
          path: './uploads/video.mp4'
        }
      }
    }
  }
] as MockMethod[]