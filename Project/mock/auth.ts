import Mock from 'mockjs'
import type { MockMethod } from 'vite-plugin-mock'

export default [
  // 获取顶层节点元素
  {
    url: '/api/auth/topnode',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: [
          {
            id: 1,
            name: '信息汇总'
          },
          {
            id: 2,
            name: '后台设置'
          },
          {
            id: 3,
            name: '电影管理'
          }
        ]
      }
    }
  },
  // 权限节点列表 -- 不分页
  {
    url: '/api/auth/list',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: [
          {
            id: 1,
            name: '信息汇总',
            ename: 'info',
            pid: 0,
            menu: 1,
            route: '',
            icon: 'Plus',
            children: [
              {
                id: 10,
                name: '图形汇总',
                ename: 'dashoard',
                pid: 1,
                menu: 1,
                route: '/info/dashoard',
                icon: 'Plus'
              }
            ]
          },
          {
            id: 2,
            name: '后台设置',
            ename: 'sys',
            pid: 0,
            menu: 1,
            route: '',
            icon: 'Plus',
            children: [
              {
                id: 20,
                name: '用户列表',
                ename: 'user',
                pid: 2,
                menu: 1,
                route: '/sys/user',
                icon: 'Plus'
              },
              {
                id: 21,
                name: '添加用户',
                ename: 'adduser',
                pid: 2,
                menu: 0,
                route: '',
                icon: 'Plus'
              },
              {
                id: 22,
                name: '角色列表',
                ename: 'role',
                pid: 2,
                menu: 1,
                route: '/sys/role',
                icon: 'Plus'
              }
            ]
          }
        ]
      }
    }
  }
] as MockMethod[]