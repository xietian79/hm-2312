// export default [
//     {
//         url: "/users/list",
//         methods: "GET",
//         response: () => {
//             return Mock.mock({
//                 errno: null,
//                 msg: "ok",
//                 data: {
//                     user: Mock.mock("@name"),
//                     name: Mock.mock("@cname"),
//                     token: Mock.mock(/\w{36,58}/),
//                     "list|1-10": [
//                         {
//                             "user|+1": "@name",
//                             "name|+1": "@cname",
//                             "age|1-100": 100,
//                             "sex|1": ["男", "女"],
//                             "tel": /1[2-9]\d{9}/,
//                             "email": "@email"
//                         }
//                     ]
//                 }
//             })
//         }
//     }
// ]
import Mock from 'mockjs'
import type { MockMethod } from 'vite-plugin-mock'
// 接口规范 restful
// get 用于获取资源  请求体
// post 用于新增资源
// put/patch 用于修改资源 put全量修改 patch增量修改
// delete 删除资源
// ---- 公司中
// get/post
// get 获取和删除单个
// post 新增、修改、批量删除

export default [
  // 账号登录
  {
    url: '/api/user/login',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          userid: 1000,
          roleid: 2,
          uname: '张三',
          token: 'fewklfjewkfjeklfjewklfjlewfjewklfjelfjelfjew',
          // 后台登录成功后，当前用户它对应的角色所可以访问到的路由地址
          // 拿到此路由地址，就可以去前端路由表中去匹配，然后通过动态路由去创建对应的路由表
          routes: [
            '/info/dashoard',
            '/sys/user',
            '/sys/role',
            '/sys/auths',
            '/film/list',
            '/film/create'
          ]
        }
      }
    }
  },
  // 发送登录短信验证码
  {
    url: '/api/user/phoneNumberCode',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 短信验证码登录
  {
    url: '/api/user/loginPhone',
    method: 'post',
    response: () => {
      // 手机号和验证码
      return {
        code: 0,
        msg: 'ok',
        data: {
          userid: 1000,
          roleid: 2,
          uname: '张三',
          token: 'fewklfjewkfjeklfjewklfjlewfjewklfjelfjelfjew',
          // 后台登录成功后，当前用户它对应的角色所可以访问到的路由地址
          // 拿到此路由地址，就可以去前端路由表中去匹配，然后通过动态路由去创建对应的路由表
          routes: [
            '/info/dashoard',
            '/sys/user',
            '/sys/role',
            '/sys/auths',
            '/film/list',
            '/film/create'
          ]
        }
      }
    }
  },
  // 账号唯一性检查
  {
    url: '/api/user/exist',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        // 1存在，0不存在
        data: 0
      }
    }
  },
  // 上传头像
  {
    url: '/api/user/avatar',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          // 用于回显的地址
          imgUrl: 'http://video.1314000.cn/face.png',
          // 用于存储于数据库中的地址
          filepath: './uploads/face.png'
        }
      }
    }
  },
  // 接受新增用户
  {
    url: '/api/user/create',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        // 新增返回的数据结果情况
        // 1.返回新增的用户在数据表中的id值
        // 2.返回新增用户的的记录数据，数据它是包含id值的
        data: 10
      }
    }
  },
  // 用户列表
  {
    url: '/api/user/list',
    method: 'get',
    response: ({ query }) => {
      const page = query.page ? Number(query.page) : 1
      const size = query.size ? Number(query.size) : 10
      // mockjs来生成假数据
      const users = Mock.mock({
        [`users|${size}`]: [
          {
            'id|+1': page * size,
            username: '@name',
            realname: '@cname',
            'age|18-100': 18,
            'sex|1': [1, 2],
            birthday: '@date',
            avatar: 'http://video.1314000.cn/face.png',
            city: ['北京', '昌平'],
            address: '北京昌平地区沙河大桥'
          }
        ]
      })
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...users,
          total: 100
        }
      }
    }
  },
  // 删除单个用户记录
  {
    url: '/api/user/:id',
    method: 'delete',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        // 1表示成功，非1为失败
        data: 1
      }
    }
  },
  // 批量单个用户记录
  {
    url: '/api/user/delbatch',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        // 1表示成功，非1为失败
        data: 1
      }
    }
  },
  // 根据用户id列表返回它对应的用户数据列表
  {
    url: '/api/user/ids',
    method: 'post',
    response: () => {
      const data = Mock.mock({
        'data|10': [
          {
            'id|+1': 1,
            username: '@name',
            realname: '@cname',
            'age|18-100': 18,
            'sex|1': [1, 2],
            birthday: '@date',
            avatar: './uploads/face.png',
            imgUrl: 'http://video.1314000.cn/face.png',
            city: ['北京', '昌平'],
            address: '北京昌平地区沙河大桥'
          }
        ]
      })
      return {
        code: 0,
        msg: 'ok',
        ...data
      }
    }
  },
  // 根据用户id返回对应的用户数据,此数据没有密码
  {
    url: '/api/user/:id',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: 20,
          username: 'Angela Wilson',
          realname: '谭涛',
          age: 60,
          sex: 2,
          birthday: '2021-12-22',
          avatar: './uploads/face.png',
          imgUrl: 'http://video.1314000.cn/face.png',
          city: ['北京', '昌平'],
          address: '北京昌平地区沙河大桥'
        }
      }
    }
  },
  // 根据用户id来修改用户信息
  {
    url: '/api/user/:id',
    method: 'put',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  }
] as MockMethod[]