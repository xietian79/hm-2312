import Mock from 'mockjs'
import url from 'url'
import querystring from 'querystring'
import type { MockMethod } from 'vite-plugin-mock'

const delay = (ms = 1000) => {
  return new Promise((resolve) => {
    setTimeout(() => {
        resolve()
    }, ms)
  })
}

export default [
  // 给角色分配权限
  {
    url: '/api/role/setAuthNode/:id',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  },
  // 根据角色id返回它已有的权限 -- 包含有菜单和没有菜单
  {
    url: '/api/role/:id/auths',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          half: [2],
          checked: [1, 10, 20, 21]
        }
      }
    }
  },
  // 根据角色id返回它已有的权限 -- 有菜单
  {
    url: '/api/role/:id/menuauths',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          menus: [
            {
              id: 1,
              name: '信息汇总',
              ename: 'info',
              pid: 0,
              menu: 1,
              route: '/info',
              icon: 'Plus',
              children: [
                {
                  id: 10,
                  name: '图形汇总',
                  ename: 'dashoard',
                  pid: 1,
                  menu: 1,
                  route: '/info/dashoard',
                  icon: 'Plus'
                }
              ]
            },
            {
              id: 2,
              name: '后台设置',
              ename: 'sys',
              pid: 0,
              menu: 1,
              route: '/sys',
              icon: 'Plus',
              children: [
                {
                  id: 20,
                  name: '用户列表',
                  ename: 'user',
                  pid: 2,
                  menu: 1,
                  route: '/sys/user',
                  icon: 'Plus'
                },
                {
                  id: 22,
                  name: '角色列表',
                  ename: 'role',
                  pid: 2,
                  menu: 1,
                  route: '/sys/role',
                  icon: 'Plus'
                },
                {
                  id: 23,
                  name: '节点列表',
                  ename: 'auths',
                  pid: 2,
                  menu: 1,
                  route: '/sys/auths',
                  icon: 'Plus'
                }
              ]
            },
            {
              id: 3,
              name: '电影管理',
              ename: 'film',
              pid: 0,
              menu: 1,
              route: '/film',
              icon: 'Plus',
              children: [
                {
                  id: 31,
                  name: '电影列表',
                  ename: 'list',
                  pid: 0,
                  menu: 3,
                  route: '/film/list',
                  icon: 'Plus'
                },
                {
                  id: 32,
                  name: '新增电影',
                  ename: 'create',
                  pid: 0,
                  menu: 3,
                  route: '/film/create',
                  icon: 'Plus'
                }
              ]
            }
          ],
          btns: ['adduser']
        }
      }
    }
  },
  // 添加角色
  {
    url: '/api/role/create',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        // 添加成功一般要求后端返回一个插入数据到数据表中的id值
        data: Date.now()
      }
    }
  },
  // 角色列表
  {
    url: '/api/role/list',
    method: 'get',
    response: () => {
      const roles = Mock.mock({
        'roles|10': [
          {
            'id|+1': 1,
            role_name: '@cname'
          }
        ]
      })

      return {
        code: 0,
        msg: 'ok',
        data: {
          ...roles,
          total: 20
        }
      }
    }
  },
  // 根据角色获取对应用户列表数据
  {
    url: '/api/roleTouser/:id',
    method: 'get',
    rawResponse: async (req, res) => {
      // console.log(querystring.parse(url.parse(req.url).search?.slice(1)))
      const page = 1 //query.page ? Number(query.page) : 1
      const size = 10 // query.size ? Number(query.size) : 10
      // mockjs来生成假数据
      const users = Mock.mock({
        [`users|${size}`]: [
          {
            'id|+1': page * size,
            username: '@name',
            realname: '@cname',
            'age|18-100': 18,
            'sex|1': [1, 2],
            birthday: '@date',
            avatar: 'http://video.1314000.cn/face.png',
            city: ['北京', '昌平'],
            address: '北京昌平地区沙河大桥'
          }
        ]
      })

      await delay(3000)

      res.setHeader('Content-Type', 'application/json')
      res.statusCode = 200
      res.end(
        JSON.stringify({
          code: 0,
          msg: 'ok',
          data: {
            ...users,
            total: 100
          }
        })
      )
    }
  },
  // 修改角色
  {
    url: '/api/role/edit/:id',
    method: 'put',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  }
] as MockMethod[]