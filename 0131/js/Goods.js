import Component from "./Component.js";
export default class Goods extends Component{
    data;
    iconImg;
    price;
    prev;
    ids;
    static GOODS_CHANGE="goods_change";
    constructor(){
        super("div","goods")
        this.setStyle();
        this.elem.addEventListener("click",e=>this.clickhandler(e));
    }
    setData(data){
        this.data=data;
        this.elem.innerHTML=`
            <div class='iconImg'>
                <img class='icon-img' src=''>
                ${!data.schedule ? "" :`
                    <div class='iconPromote'>
                        ${!data.schedule.img ? "" : `<img src='${data.schedule.img}'>`}
                        ${!data.schedule.info ? "" : `<span class='iconPromoteTxt'>${data.schedule.info}</span>`}
                    </div>
                `}
            </div>
            <div class='iconList clear'>
                ${data.list.reduce((v,t)=>v+`<img id='${t.id}' src='${t.img}'>`,"")}
            </div>
            <div class='priceCon'>
                ￥<i></i>
            </div>
            <div class='titleCon'>
                <span class='titleIcon'>京品手机</span><a href='javascript:void(0)'>${data.info}</a>
            </div>
            <div class='judgeCon'>
                <span class='judge'>${String(data.judge)[0]+["","0","00","000","万","0万","00万","000万","亿","0亿","00亿","000亿","万亿"][String(data.judge).length-1]}</span>条评论
            </div>
            <div class='shoppingCon'>
                <a class='shopping' href='javascript:void(0)'>${data.shop}</a>
                <img src='../img/chart.png'>
            </div>
            <div>
                ${Object.keys(data.icons).reduce((v,key)=>{
                    if(!data.icons[key]) return v;
                    return v+data.icons[key].reduce((value,item)=>{
                        return value+`<span class='${key}'>${item}</span>`
                    },"")
                },"")}
            </div>
        `
        this.iconImg=this.elem.querySelector(".icon-img");
        this.price=this.elem.querySelector(".priceCon>i");
        var iconList=this.elem.querySelector(".iconList");
        iconList.addEventListener("mouseover",e=>this.mouseHandler(e));
        var evt=new MouseEvent("mouseover",{bubbles:true});
        iconList.firstElementChild.dispatchEvent(evt);
    }
    mouseHandler(e){
        if(e.target.nodeName!=="IMG") return;
        this.iconImg.src=e.target.src;
        var id=e.target.id;
        this.ids=Number(id);
        var item=this.data.list.find(t=>t.id==Number(id));
        this.price.textContent=Number(item.price).toFixed(2);
        this.changePrev(e.target);
    }
    changePrev(img){
        if(this.prev){
            this.prev.style.borderColor="#ddd";
        }
        this.prev=img;
        this.prev.style.borderColor="#e4393c"
    }
    clickhandler(e){
        var evt=new Event(Goods.GOODS_CHANGE);
        evt.data=this.data;
        evt.ids=this.ids;
        this.dispatchEvent(evt);
    }
    setStyle(){
        Goods.setCss(`.goods {
            width: 240px;
            height: 444px;
            padding: 12px 9px;
            font: 12px/150% tahoma, arial, Microsoft YaHei, Hiragino Sans GB, "\u5b8b\u4f53", sans-serif;
            color: #666;
            position: relative;
            float: left;
        }

        .goods:hover {
            box-shadow: 0px 0px 4px #999;
        }

        .goods>.iconImg {
            text-decoration: none;
            width: 100%;
            height: 220px;
            text-align: center;
            display: block;
            position: relative;
        }

        .goods>.iconImg>img {
            width: 220px;
            height: 220px;
        }

        .goods>.iconImg>.iconPromote {
            width: 220px;
            height: 25px;
            text-align: left;
            position: absolute;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.6);
        }

        .goods>.iconImg>.iconPromote>img {
            margin-left: 10px;
            vertical-align: middle;
        }

        .goods>.iconImg .iconPromoteTxt {
            line-height: 25px;
            color: white;
            margin-left: 10px;
        }

        .goods>.iconList {
            margin-left: 2px;
            margin-top:5px
        }

        .goods>.iconList>img {
            float: left;
            /* #e4393c */
            border: 1px solid #ddd;
            padding: 1px;
            width: 25px;
            height: 25px;
            margin: 2px;
        }

        .clear::after {
            content: "";
            display: block;
            height: 0;
            overflow: hidden;
            clear: both;
            visibility: hidden;
        }

        .goods>.priceCon {
            font-size: 16px;
            color: #e4393c;
            margin: 0;
           
            margin-top: 8px;
           
        }

        .goods>.priceCon>i {
            font-size: 20px;
            font-style: normal;
            font-weight: 400;
            font-family: Verdana;
        }

        .goods>.titleCon {
            width: 220px;
            overflow: hidden;
            white-space: wrap;
            margin: 0;
            margin-top: 8px;
            padding: 0;
            height:40px;
            overflow:hidden;
        }

        .goods>.titleCon>.titleIcon {
            float: left;
            height: 16px;
            padding: 0 3px;
            margin-top: 2px;
            margin-right: 3px;
            overflow: hidden;
            color: #fff;
            font: 12px/16px "Helvetica Neue", "Hiragino Sans GB", SimSun, serif;
            background: #c81623;
            border-radius: 2px;

        }

        .goods>.titleCon>a {
            text-decoration: none;
            color: #666;
        }

        .goods>.titleCon>a:hover {
            color: #c81623;
        }

        .goods>.info {
            margin: 0;
            margin-top: 8px;
        }

        .goods>.info>.infoitem {
            float: left;
            height: 19px;
            line-height: 19px;
            padding: 0 6px;
            margin-right: 7px;
            color: #999;
            background: #f4f4f4;
            text-decoration: none;
        }

        .goods>.info>.infoitem:hover {
            color: #c81623;
        }

        .goods>.judgeCon {
            margin-top: 8px;
        }

        .goods>.judgeCon>.judge {
            color: #646fb0;
            font-family: verdana;
            font-weight: 700;
        }

        .goods>.shoppingCon {
            margin-top: 8px;
            margin-bottom: 10px;
        }

        .goods>.shoppingCon>.shopping {
            color: #999;
            text-decoration: none;
            display: inline-block;
            max-width: 122px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            margin-right:5px;
        }

        .goods>.shoppingCon>.shopping:hover {
            color: #c81623;
        }

        .icon1,
        .icon3,
        .icon2 {
            float: left;
            height: 16px;
            line-height: 16px;
            padding: 0 3px;
            margin-right: 3px;
            overflow: hidden;
            text-align: center;
            font-style: normal;
            font-size: 12px;
            font-family: "Helvetica Neue", "Hiragino Sans GB", SimSun, serif;
            background: #e23a3a;
            color: #FFF;
            cursor: default;
            border-radius: 2px;
        }

        .icon4 {
            float: left;
            height: 14px;
            line-height: 14px;
            padding: 0 3px;
            border: 1px solid #e23a3a;
            margin-right: 3px;
            overflow: hidden;
            text-align: center;
            font-style: normal;
            font-size: 12px;
            font-family: "Helvetica Neue", "Hiragino Sans GB", SimSun, serif;
            border-radius: 2px;
            color: #e23a3a;
        }

        .icon3 {
            background: #31c19e;
        }

        .icon2 {
            float: left;
            height: 14px;
            line-height: 14px;
            line-height: 16px;
            padding: 0 3px;
            margin-right: 3px;
            overflow: hidden;
            text-align: center;
            font-style: normal;
            font-size: 12px;
            font-family: "Helvetica Neue", "Hiragino Sans GB", SimSun, serif;
            border-radius: 2px;
            border: 1px solid #4d88ff;
            color: #4d88ff;
            background-color: white;
        }

        .double11 {
            position: absolute;
            right: 10px;
            top: 20px;
        }`)
    }
}