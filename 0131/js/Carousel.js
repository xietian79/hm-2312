import Component from "./Component.js";

export default class Carousel extends Component {
  static LEFT = Symbol("left");
  static RIGHT = Symbol("right");
  imgCon;
  dot;
  bool = false;
  direction = Carousel.LEFT;
  pos = 0;
  data = [];
  prev;
  x = 0;
  autoBool = false;
  ids;
  speed = 50;
  static list = [];
  constructor() {
    super("div", "swiper");
    this.setStyle();
    this.imgCon = this.createDom("div", "img-con", this.elem);
    this.dot = this.createDom("ul", "dot");
    this.elem.appendChild(this.dot);
    ["left", "right"].forEach((item) => {
      var bn = this.createDom("img", item, this.elem);
      bn.src = `./img/${item}.png`;
    });
    Carousel.list.push(this);
    this.elem.addEventListener("click", (e) => this.clickHandler(e));
    this.elem.addEventListener("mouseenter", (e) => this.mouseHandler(e));
    this.elem.addEventListener("mouseleave", (e) => this.mouseHandler(e));
  }
  setData(data) {
    this.data = data;
    this.dot.innerHTML = data.reduce((v) => v + "<li></li>", "");
    this.createImgBlock(data[0]);
    this.changePrev();
  }
  createImgBlock(item, firstBool = false) {
    var elem = this.createDom("div", "img-block", this.imgCon, firstBool);
    elem.innerHTML = `
            <img src='${item.src}'>
            <div>
                <div class='date'>
                    <span class='day'>${item.date.split("/")[0]}</span>
                    ${"/" + item.date.split("/")[1]}
                </div>
                <h3>${item.info}</h3>
            </div> 
            
        `;
  }
  mouseHandler(e) {
    this.autoBool = e.type === "mouseleave";
  }
  clickHandler(e) {
    if (this.bool || this.data.length == 0) return;
    if (e.target.nodeName != "LI" && !/left|right/.test(e.target.className))
      return;
    if (e.target.nodeName === "LI") {
      var index = Array.from(this.dot.children).indexOf(e.target);
      this.direction = index > this.pos ? Carousel.LEFT : Carousel.RIGHT;
      this.pos = index;
    } else if (e.target.className === "left") {
      this.direction = Carousel.RIGHT;
      this.pos--;
      if (this.pos < 0) this.pos = this.data.length - 1;
    } else {
      this.direction = Carousel.LEFT;
      this.pos++;
      if (this.pos > this.data.length - 1) this.pos = 0;
    }
    this.createNextBlock();
    this.changePrev();
  }
  createNextBlock() {
    if (this.direction === Carousel.LEFT) {
      this.createImgBlock(this.data[this.pos]);
      this.x = 0;
    } else if (this.direction === Carousel.RIGHT) {
      this.createImgBlock(this.data[this.pos], true);
      this.x = -this.elem.offsetWidth;
    }
    this.imgCon.style.left = this.x + "px";
    this.bool = true;
  }
  changePrev() {
    if (this.prev) {
      this.prev.style.backgroundColor = "rgba(255,0,0,0)";
    }
    // 第几张图对应li的下标
    this.prev = this.dot.children[this.pos];
    this.prev.style.backgroundColor = "red";
  }
  update() {
    this.imgConMove();
    this.autoPlay();
  }
  imgConMove() {
    if (!this.bool) return;
    if (this.direction === Carousel.LEFT) {
      this.x -= this.speed;
      if (this.x <= -this.elem.offsetWidth) {
        this.bool = false;
        this.imgCon.firstElementChild.remove();
        this.x = 0;
      }
    } else if (this.direction === Carousel.RIGHT) {
      this.x += this.speed;
      if (this.x >= 0) {
        this.bool = false;
        this.imgCon.lastElementChild.remove();
        this.x = 0;
      }
    }
    this.imgCon.style.left = this.x + "px";
  }
  async autoPlay() {
    if (this.ids) return;
    await this.delaty();
    if (!this.autoBool) return;
    var evt = new MouseEvent("click", { bubbles: true });
    this.elem.lastElementChild.dispatchEvent(evt);
  }
  delaty() {
    return new Promise((resolve, reject) => {
      this.ids = setTimeout(() => {
        clearTimeout(this.ids);
        this.ids = undefined;
        resolve();
      }, 4000);
    });
  }
  static update() {
    Carousel.list.forEach((item) => item.update());
  }
  setStyle() {
    Carousel.setCss(` body{
            margin: 0;
        }
        .swiper{
            width: 100%;
            height: 33.3vw;
            position: relative;
            overflow: hidden;
        }
        .img-con{
            width: 200%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }
        .swiper>.dot{
            list-style: none;
            margin: 0;
            padding: 0;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            bottom: 20px;
        }
        .swiper>.dot>li{
            width: 18px;
            height: 18px;
            border: 2px solid #f00;
            border-radius: 18px;
            float: left;
            margin-left: 10px;
        }
        .swiper>.dot>li:first-child{
            margin-left: 0;
        }
        .swiper>.left,.swiper>.right{
            position: absolute;
            top: 50%;
            transform: translate(0,-50%);
        }
        .swiper>.left{
            left: 50px;
        }
        .swiper>.right{
            right: 50px;
        }
        .swiper>.img-con>.img-block{
            width: 50%;
            height: 100%;
            float: left;
            position: relative;
        }
        .swiper>.img-con>.img-block>div{
            position: relative;
            left: 0;
            top: 0;
        }
        .swiper>.img-con>.img-block>div>.date{
            font-size: 20px;
            position: relative;
            width: 180px;
            overflow: hidden;
            text-shadow: 0 1px 3px rgba(0,0,0,.9);
            color: white;
            top: 25px;
            left: 50%;
            margin-left: -500px 
        }
        .swiper>.img-con>.img-block>div>.date>.day{
            font-size: 30px;
        }
        .swiper>.img-con>.img-block>div>h3{
            position: relative;
            font-weight:normal;
            font-size: 20px;
            text-shadow: 0 1px 3px rgba(0,0,0,.9);
            color: white;
            top: 10px;
            left: 50%;
            margin-left: -500px;
            text-align: left;
            cursor: pointer;
            width:70%
        }
        .swiper>.img-con>.img-block>img{
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
        }`);
  }
}
