export default class Component extends EventTarget{
    elem;
    static cssBool=false;
    constructor(type,className){
        super();
        this.elem=this.createDom(type,className);
    }
    appendTo(parent,firstBool=false,elem=null){
        if(!elem) elem=this.elem;
        if(typeof parent==="string") parent=document.querySelector(parent);
        if(parent instanceof HTMLElement || parent instanceof DocumentFragment){
            if(firstBool) parent.insertBefore(elem,parent.firstElementChild);
            else parent.appendChild(elem);
        }
     
        return parent;
    }
    createDom(type,className,parent,firstBool=false){
        var elem=document.createElement(type);
        if(className) elem.className=className;
        if(parent){
           this.appendTo(parent,firstBool,elem);
        }
        return elem;
    }
    static setCss(css){
        if(this.cssBool) return;
        this.cssBool=true;
        var style;
        if(document.styleSheets.length>0){
           style=Array.from(document.styleSheets).findLast(item=>item.ownerNode.nodeName==="STYLE")?.ownerNode;
        }
        if(!style){
            style=document.createElement("style");
            document.head.appendChild(style);
        }
        style.innerHTML+="\n"+css;
    }
}