"use strict";
// 1、交叉类型
// interface IA{
//     a:number;
//     b:number;
// }
// interface IB{
//     c:number;
//     d:number;
// }
// var o:IA&IB={a:1,b:2,c:3,d:4};
// interface IA{
//     a:number;
// }
// interface IB{
//     b:number;
// }
// var key:keyof (IA&IB)="a";
// 2、联合类型
// var a:number|string="a";
// function getType(a:any):a is number
// {
//     return typeof a==="number";
// }
// function add(a:number|string):void{
//     if(typeof a==="number"){
//         console.log(a);
//     }else if(typeof a==="string"){
//         console.log("aaa")
//     }
// }
// function add(a:number|string):void{
//     if(getType(a)){
//         console.log(a);
//     }else if(typeof a==="string"){
//         console.log("aaa")
//     }
// }
// add(1);
// class A{
//    public  play(){
//     console.log("play")
//     }
// }
// class B{
//     public run(){
//         console.log("run")
//     }
// }
// function runAction(o:A|B){
//     if((o as A).play){
//         (o as A).play();
//     }else if((o as B).run){
//         (o as B).run();
//     }
// }
//对象 is 类型  返回值中使用
// function isA(o:A|B):o is A
// {
//     return o instanceof A;
// }
// function runAction(o:A|B){
//    if(isA(o)){
//         o.play();
//    }else{
//         o.run();
//    }
// }
// var a:A=new A();
// var b:B=new B();
// console.log(isA(b))
// 类型别名
// type a=number | string | boolean;
// var o:a;
// o=1;
// o="a";
// o=true;
// o={a:1}
// type IA={
//     a:number;
//     b:number;
// }
// interface IA{
//     a:number;
//     b:number;
// }
// type IB=IA;
// var o:IB={a:1,b:2};
// 范围别名
// type A="red" | "blue" | "green";
// var str:A;
// str="red";
// str="blue";
// str="green";
// str="yellow"
// 泛型别名
// type IA<T>={a:T};
// var o:IA<number>={a:1};
// type Alias = { num: number }
// interface Interface {
//     num: number;
// }
// declare function aliased(arg: Alias): Alias;
// declare function interfaced(arg: Interface): Interface;
/*
 类型别名和接口的区别
 1、类型别名鼠标悬停时显示字面量，接口鼠标悬停时显示接口名
 2、接口是可以被extends和implements的继承和实现的，别名不能被继承和实现
 3、当无法通过接口来描述一个类型，需要使用到联合类型，交叉类型或者元组类型等时，可以使用别名更快捷

*/
//# sourceMappingURL=AdvancedType.js.map