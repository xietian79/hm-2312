"use strict";
// 普通函数
// function fn(a:number|string|boolean):void
// {
class Box {
    constructor() {
    }
    update() {
        console.log("box update");
    }
}
class Ball {
    constructor() {
    }
    update() {
        console.log("ball update");
    }
}
class Rect {
    constructor() {
    }
    render() {
        console.log("Rect render");
    }
}
class Circle {
    constructor() {
    }
    render() {
        console.log("Circle render");
    }
}
// function getInstance<T>(abc:new ()=>T):T
// {
//     var a=new abc();
//     return a;
// }
// getInstance<IUpdate>(Box);
// getInstance<IUpdate>(Ball);
// getInstance<IRender>(Rect);
// getInstance<IRender>(Circle);
// 5、泛型约束
// function setProperty<T extends IUpdate>(a:T){
// }
// var b:Ball=new Ball();
// setProperty(b);
// var c:Circle=new Circle();
// setProperty(c);
// function setProperty<T extends number|string>(a:T){
// }
// setProperty<string>("a");
// setProperty<number>(1);
// setProperty<boolean>(true);
// 6、keyof的使用
// function getProperty<T>(obj:T,key:keyof T):void
// {
// }
// interface IObj{
//     a:number;
//     b:number;
// }
// var obj:IObj={a:1,b:2};
// getProperty<IObj>(obj,"a");
// getProperty<IObj>(obj,"b");
// getProperty<IObj>(obj,"c");
//# sourceMappingURL=Genericity.js.map