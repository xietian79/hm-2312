import Component from "./Component.js";
export default class StepNumber extends Component {
    private _step;
    private list;
    private ids?;
    static readonly STEP_CHANGE_EVENT: string;
    constructor();
    private clickHandler;
    private inputHandler;
    set step(value: number);
    get step(): number;
    protected setStyle(): void;
    protected dispatch(): void;
}
//# sourceMappingURL=StepNumber.d.ts.map