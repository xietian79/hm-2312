export default abstract class Component extends EventTarget {
    private static cssBool;
    protected elem: HTMLElement;
    constructor(type: string, className?: string);
    appendTo(parent: string | HTMLElement | DocumentFragment, firstBool?: boolean, elem?: HTMLElement | null): HTMLElement | DocumentFragment;
    protected createDom(type: string, className?: string, parent?: string | HTMLElement, firstBool?: boolean): HTMLElement;
    protected static setCss(css: string): void;
    protected abstract setStyle(): void;
    protected abstract dispatch(): void;
}
//# sourceMappingURL=Component.d.ts.map