import Component from "./Component.js";
class StepNumber extends Component {
    constructor() {
        super("div", "stepnumber");
        this._step = 1;
        this.elem.innerHTML = `
            <button disabled>-</button>
            <input type='text'>
            <button>+</button>
        `;
        this.list = Array.from(this.elem.children);
        this.setStyle();
        this.step = 1;
        this.elem.addEventListener("click", (e) => this.clickHandler(e));
        this.elem.addEventListener("input", (e) => this.inputHandler(e));
    }
    clickHandler(e) {
        var elem = e.target;
        if (elem.nodeName !== "BUTTON")
            return;
        if (elem.innerHTML === "-") {
            this.step--;
        }
        else {
            this.step++;
        }
    }
    inputHandler(e) {
        this.list[1].value = this.list[1].value.replace(/\D/g, "");
        this.step = Number(this.list[1].value);
        if (this.ids)
            return;
        this.ids = setTimeout(() => {
            clearTimeout(this.ids);
            this.ids = undefined;
            this.dispatch();
        }, 500);
    }
    set step(value) {
        this.list[0].disabled = false;
        this.list[2].disabled = false;
        if (value <= 1) {
            value = 1;
            this.list[0].disabled = true;
        }
        else if (value >= 99) {
            value = 99;
            this.list[2].disabled = true;
        }
        this._step = value;
        this.list[1].value = String(value);
    }
    get step() {
        return this._step;
    }
    setStyle() {
        StepNumber.setCss(`
            .stepnumber{
                position:relative;
                width:80px;
                height:20px;
                font-size:0;
            }
            .stepnumber>button{
                width:15px;
                height:18px;
                border:1px solid #999;
                padding:0;
                margin:0;
                outline:none;
            }
            .stepnumber>input{
                width:46px;
                height:16px;
                border:none;
                border-top:1px solid #999;
                border-bottom:1px solid #999;
                margin:0;
                padding:0;
                outline:none;
                text-align:center;
            }
        `);
    }
    dispatch() {
        var evt = new Event(StepNumber.STEP_CHANGE_EVENT);
        evt.step = this._step;
        this.dispatchEvent(evt);
    }
}
StepNumber.STEP_CHANGE_EVENT = "step_change_event";
export default StepNumber;
//# sourceMappingURL=StepNumber.js.map