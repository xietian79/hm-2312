class Component extends EventTarget {
    constructor(type, className) {
        super();
        this.elem = this.createDom(type, className);
    }
    appendTo(parent, firstBool = false, elem = null) {
        if (!elem)
            elem = this.elem;
        if (typeof parent === "string")
            parent = document.querySelector(parent);
        if ((parent && parent instanceof HTMLElement) || (parent && parent instanceof DocumentFragment)) {
            if (firstBool)
                parent.insertBefore(elem, parent.firstElementChild);
            else
                parent.appendChild(elem);
        }
        return parent;
    }
    createDom(type, className, parent, firstBool = false) {
        var elem = document.createElement(type);
        if (className)
            elem.className = className;
        if (parent) {
            this.appendTo(parent, firstBool, elem);
        }
        return elem;
    }
    static setCss(css) {
        var _a;
        if (this.cssBool)
            return;
        this.cssBool = true;
        var style;
        if (document.styleSheets.length > 0) {
            style = (_a = Array.from(document.styleSheets).findLast((item) => { var _a; return ((_a = item.ownerNode) === null || _a === void 0 ? void 0 : _a.nodeName) === "STYLE"; })) === null || _a === void 0 ? void 0 : _a.ownerNode;
        }
        if (!style) {
            style = document.createElement("style");
            document.head.appendChild(style);
        }
        style.innerHTML += "\n" + css;
    }
}
Component.cssBool = false;
export default Component;
//# sourceMappingURL=Component.js.map