"use strict";
// 1<<2   将2进制向左移动几位   10<<2   1000    1011<<2  101100   1<<1 10  1<<2   100  1<<3 1000  1<<4 16
// 1<<n    2**n
// 2|4    = 6
// 二进制运算
// 010
// 100
// 110  =6
// enum E {
//     X, Y, Z
// }
// function f(obj: { X: number }) {
//     return obj.X;
// }
// f(E);
// function fn(o:{a:number,b:number}){
// }
// fn({a:1,b:2});
// var b:HTMLDivElement | null=document.querySelector("div");
// var p: Promise<number>=new Promise(function(resolve: (value: number) => void,reject:(reason?: any) => void){
//     setTimeout(function(){
//         resolve(10);
//     },1000)
// })
// interface IA{
//     a:number;
// }
// var b:{a:number,b:number}={a:1,b:2};
// var c:IA=b;
// interface IA{
//     a:number;
//     b:number;
// }
// interface IB{
//     c:number;
//     d:number;
//     a:number;
// }
// var o:IB={c:1,d:3,a:4};
// var o1:IA=o;
//# sourceMappingURL=EnumBase.js.map