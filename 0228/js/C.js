class C {
    constructor() {
        this.num = 1;
        this.age = 0;
    }
    static getInstance() {
        if (!C._instance) {
            C._instance = new C();
        }
        return C._instance;
    }
}
C.a = 1;
export default C;
//# sourceMappingURL=C.js.map