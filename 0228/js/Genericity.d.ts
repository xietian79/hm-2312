interface IUpdate {
    update(): void;
}
interface IRender {
    render(): void;
}
declare class Box implements IUpdate {
    constructor();
    update(): void;
}
declare class Ball implements IUpdate {
    constructor();
    update(): void;
}
declare class Rect implements IRender {
    constructor();
    render(): void;
}
declare class Circle implements IRender {
    constructor();
    render(): void;
}
//# sourceMappingURL=Genericity.d.ts.map