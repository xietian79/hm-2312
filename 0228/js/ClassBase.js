"use strict";
// 1、基础
// 在实例化的方法中this指向当前实例化对象，实例化属性的值如果使用this，也是实例化对象
// 在静态方法中的this指向当前类名也就是当前构造函数本身，静态属性的值如果使用this,也是当前的类
// class Box{
//     // 静态属性可以不用赋初值
//     static list:Array<number>;
//     // 实例化属性定义必须赋初值，或者在constructor构造函数中赋值
//     num:number=1;
//     name:string;
//     // age:number;//没有赋初值就会报错
//     age?:number;//可选属性不用赋初值
//     // age:number=this.num;
//    constructor构造函数不要返回值的类型
//     constructor(name:string){
//         this.name=name;
//     }
//     play():void
//     {
//         console.log("play")
//     }
//     static run():void{
//         console.log(this)
//     }
// }
// // 继承后，Ball类就有了Box类中所有实例化属性方法和静态属性方法
// class Ball extends Box{
//     constructor(name:string){
//         super(name);//继承后构造函数中必须写super() 这个是执行超类的构造函数
//     }
//     // 重新方法 override
//     // play(): void {
//     //     // 这样可以直接重新，当前类中的play方法覆盖超类的play方法
//     // }
//     // 在超类原有方法基础上新增内容
//     play(): void {
//         super.play();//执行超类的play方法
//         // 新增内容
//     }
// }
// 2、修饰符
/*
 写类的时候，需要先将所有的属性和方法全部写成private的
 当继承后需要重写或者调用的属性方法，改为protected
 类写完如果这个方法或者属性需要再实例化中调用，改为public

*/
// 用来修饰所有的属性和方法（包括实例化属性方法和静态属性方法）
//                    实例化对象可以调用的   静态属性和方法可以直接通过类调用   当前类中的实例化方法中调用实例属性和方法    当前类中的静态方法中调用静态属性和方法     在继承的子类的实例化方法中是否可以调用父类的实例化属性和方法   在继承的子类的静态方法中是否可以调用父类的静态属性和方法   子类中重写父类的方法时（override）
// public   公有            可以                 可以                           可以                                    可以                                        可以                                                可以                                        可以
// private  私有            不可以               不可以                         可以                                      可以                                       不可以                                             不可以                                      不可以
// protected  受保护的       不可以               不可以                        可以                                        可以                                      可以                                              可以                                           可以
// class Box{
//     public name:string="a";
//     private age:number=30;
//     protected sex:string="男";
//     public static a:number=1;
//     private static b:number=2;
//     protected static c:number=3;
//     constructor(){
//         console.log(this.name);
//         console.log(this.age);
//         console.log(this.sex);
//         this.play();
//         this.run();
//         this.jump();
//     }
//     public play():void
//     {
//         console.log("play")
//     }
//     private run():void
//     {
//         console.log("run")
//     }
//     protected jump():void{
//         console.log("jump")
//     }
//     public static method1(){
//         console.log(Box.a)
//         console.log(Box.b)
//         console.log(Box.c)
//         Box.method1();
//         Box.method2();
//         Box.method3();
//     }
//     private static method2(){
//     }
//     protected static method3(){
//     }
// }
// class Ball extends Box{
//     constructor(){
//         super();
//         console.log(this.name);//public
//         console.log(this.sex);//protected
//         // console.log(this.age);//private 不能调用
//     }
//     static abc(){
//         // console.log(Ball.a);//public
//         // console.log(Ball.b);//private  不能调用
//         // console.log(Ball.c);//protected
//         // Ball.method1();
//         // Ball.method3();
//         // Ball.method2();//private
//     }
//     public play(): void {
//     }
//     // 重写私有方法时会报错
//     // private run():void{
//     // }
//     protected  jump(): void {
//     }
// }
// var b:Box=new Box();
// 只有公有的属性才可以通过实例化对象调用
// console.log(b.name);//可以打印
// console.log(b.age);//错误
// console.log(b.sex);//错误
// 只有公有的方法才可以通过实例化对象执行
// b.play();
// b.run();
// b.jump();
// console.log(Box.a);
// console.log(Box.b);//错误
// console.log(Box.c);//错误
// Box.method1();
// Box.method2();
// Box.method3();
// class Box{
//     constructor(){
//         document.addEventListener("click",e=>this.clickHandler(e));
//     }
//     private clickHandler(e:MouseEvent):void
//     {
//         console.log(e.currentTarget);
//     }
// }
// var b:Box=new Box();
// 2）构造函数修饰符
// class Box{
//     public constructor(){
//         console.log("aa")
//     }
// }
// var b:Box=new Box();
// protected
// class Box{
//     // 受保护的构造函数不能实例化
//     protected constructor(){
//         console.log("aa")
//     }
// }
// class Ball extends Box{
//    public constructor(){
//         super()
//     }
// }
// 受保护的构造函数不能实例化
// 继承Box后的Ball是可以实例化
// var b:Box=new Box()
// var b:Ball=new Ball();
// console.log(b)
// private
// class Box{
//     private constructor(){
//         console.log("aa")
//     }
// }
// class Ball extends Box{
//     constructor(){
//         super();
//     }
// }
// 私有构造函数是不能实例化的
// 私有的构造函数类也是不能继承的
// var b:Box=new Box();
// class Box{
//     private static _instance:Box;
//     private constructor(){
//     }
//     public static getInstance():Box
//     {
//         if(!Box._instance){
//             Box._instance=new Box();
//         }
//         return Box._instance;
//     }
// }
// console.log(Box.getInstance());
// console.log(Box.getInstance());
// console.log(Box.getInstance()===Box.getInstance());
// 在外面没法通过new Box()实例化，但是可以通过Box.getInstance()获取到唯一一个实例化对象  单例
// 3、readonly
// 在类中不能使用const定义常量 ，使用readonly可以设置该属性只读
// 在类中不可以定义枚举属性
// 在类中可以使用readonly定义静态常量，但是static必须写在readonly的前面
// 如果使用时只是为了获取到一个初值使用readonly，如果需要有一个过程才能拿到结果可以使用get
// class Box{
//     // public readonly NUM:number=10;
//     // public readonly enum COLOR{}
//     // public static readonly COL:number=10;
//     constructor(){
//     }
//     // 使用get也可以完成只读属性
//     public get num():number{
//         return 1;
//     }
// }
// class Box{
//     private static _instance:Box;
//     public age:number=10;
//     private constructor(){
//     }
//     public static get instance():Box{
//         return Box._instance || (Box._instance=new Box());
//     }
// }
// 2) 构造函数中使用readonly
// class Box{
//     constructor(public readonly name:string){
//             // name="c";//参数name可以修改，但是产生实例化属性name是不能修改
//     }
// }
// 上面与下面的是相同的
// class Box{
//     public readonly name:string;
//     constructor(name:string){
//         this.name=name;
//     }
// }
// class Box{
//     constructor(public name:string){
//     }
// }
// class Box{
//     public name:string;
//     constructor(name:string){
//         this.name=name;
//     }
// }
// class Box{
//     constructor(protected readonly name:string){
//     }
// }
// class Ball extends Box{
//     constructor(name:string){
//         super(name)
//     }
//     public play(){
//         console.log(this.name)
//         // this.name="a"
//     }
// }
// var b=new Box("a");
// console.log(b.name)
// b.name="b";
// class Box{
//     constructor(private readonly name:string){
//     }
// }
// class Ball extends Box{
//     constructor(name:string){
//         super(name);
//     }
//     public play(){
//         console.log(this.name)
//     }
// }
// var b=new Ball("ab");
// b.play();
// 4、存取器
// class Box{
//     private _sum:number=0;
//     // 必须保证 set方法中参数的类型与get方法返回值的类型一致
//     constructor(){
//     }
//     // set方法后面不能写返回类型
//     public set sum(value:number){
//         this._sum=value;
//     }
//     // get方法后面必须写返回类型，但是不能在get方法写入参数
//     public get sum():number{
//         return this._sum;
//     }
// }
//5、 接口实现的问题
// 在接口中是不能使用修饰符的
// interface IBox{
//     num:number;
//     age:number;
//     name:string;
// }
// 类中实现接口中的属性和方法，必须是public，不能设置为私有或者受保护的
// class Box implements IBox{
//    public  num: number=1;
//    public age: number=2;
//    public name: string="a";
// }
// class Box implements IBox{
//     public num:number=1
//     public age:number=2;
//     constructor(public name:string){
//     }
// }
// 当接口继承类时，必须保证这个类中的所有实例化属性方法必须是public公有才可以继承
// 当类作为接口使用时，也不能使用私有和受保护的属性方法
// class Box{
//     private age:number=1;
//     constructor(public name:string){
//     }
//     public play():void{
//         console.log("play")
//     }
// }
// interface IBox extends Box{
//     num:number;
// }
// class Ball implements IBox{
//     public age: number=10;
//     public name:string="aa";
//     public num:number=1;
//     constructor(){
//     }
//     public  play(): void {
//     }
// }
// 6、抽象类
// 在抽象类中可以使用public protected定义抽象属性和方法，但是不能使用private定义私有抽象属性和方法
// abstract class Box{
//     public age:number=1;
//     // 抽象属性不用赋初值
//     public abstract name:string;
//     constructor(){
//     }
//     public play():void{
//     }
//     // 抽象方法不写方法内容
//     public abstract run():void;
//     protected abstract jump():void;
// }
// // 子类继承抽象类后，必须实现抽象类中所有抽象属性和方法
// class Ball extends Box{
//     public name: string="a";
//     public run(): void {
//     }
//     protected jump(): void {
//     }
// }
// // 抽象类不能实例化
// // var b:Box=new Box();
// // 继承抽象类的子类可以实例化
// var b:Ball=new Ball();
//# sourceMappingURL=ClassBase.js.map