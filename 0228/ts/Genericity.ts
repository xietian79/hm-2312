// 普通函数
// function fn(a:number|string|boolean):void
// {

// }
// fn()

// 重载
// function fn(a:number):void;
// function fn(a:string):void;
// function fn(a:boolean):void;
// function fn(a:number|string|boolean):void{

// }
// fn()

// 禁止使用any
// function fn(a:any):void{

// }
// 泛型
// 泛型函数  泛型接口  泛型类
// 1、泛型函数
// T相当于一个变量 这个变量表示类类型
// 约定俗成 泛型变量一般用 T U V W X Y Z
// function fn<T>(a:T):void
// {

// }
// fn<number>(1);
// fn<string>("a");
// fn<Date>(new Date())


// function fn<T,U>(a:T,b:U):void{

// }
// fn<string,number>("a",10);


// Array<number>
// Set<number>
// Map<string,number>
// Promise<number>

// 泛型接口
// interface IObj<T>{
//     num:T;
// }

// var obj:IObj<string>={num:"a"};
// var o:IObj<number>={num:1};

// interface IFn<T,U>{
//     (a:T,b:U):void;
// }

// var fn:IFn<string,number>=(a,b)=>{
//     console.log(a,b);
// }
// fn("a",3);

// interface IA<T>{
//     play(a:T):void;
// }

// class Box implements IA<number>{
//     public play(a:number):void
//     {

//     }
// }
// class Ball implements IA<string>{
//     public play(a: string): void {
        
//     }
// }

// 3、泛型类
// class Box<T,U>{
//    public age:T;
//    constructor(age:T){
//         this.age=age;
//    }
//    public play(num:U):U
//    {
//       return num;
//    }
// }

// var b:Box<number,string>=new Box(1);
// b.play("a");

// 4、泛型类类型

// function getInstance<T>(className:new ()=>T):T{
//     return new className();
// }
// getInstance<Array<number>>(Array)

// class Box{
//     constructor(){

//     }
//     public play():void
//     {

//     }
// }
// getInstance<Box>(Box)


interface IUpdate{
    update():void;
}
interface IRender{
    render():void;
}
class Box implements IUpdate{
    constructor(){

    }
    public update(): void {
        console.log("box update")
    }
}
class Ball implements IUpdate{
    constructor(){

    }
    public update(): void {
        console.log("ball update")
    }
}
class Rect implements IRender{
    constructor(){

    }
    public  render(): void {
        console.log("Rect render")
    }
}
class Circle implements IRender{
    constructor(){

    }
    public  render(): void {
        console.log("Circle render")
    }
}

// function getInstance<T>(abc:new ()=>T):T
// {
//     var a=new abc();
//     return a;
// }

// getInstance<IUpdate>(Box);
// getInstance<IUpdate>(Ball);
// getInstance<IRender>(Rect);
// getInstance<IRender>(Circle);

// 5、泛型约束
// function setProperty<T extends IUpdate>(a:T){

// }

// var b:Ball=new Ball();
// setProperty(b);
// var c:Circle=new Circle();
// setProperty(c);
// function setProperty<T extends number|string>(a:T){

// }

// setProperty<string>("a");
// setProperty<number>(1);
// setProperty<boolean>(true);


// 6、keyof的使用

// function getProperty<T>(obj:T,key:keyof T):void
// {

// }
// interface IObj{
//     a:number;
//     b:number;
// }
// var obj:IObj={a:1,b:2};

// getProperty<IObj>(obj,"a");
// getProperty<IObj>(obj,"b");
// getProperty<IObj>(obj,"c");
