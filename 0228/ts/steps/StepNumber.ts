import Component from "./Component.js";
import { IEmitter } from "./Emiter.js";

export default class StepNumber extends Component{
    private _step:number=1;
    private list:[HTMLButtonElement,HTMLInputElement,HTMLButtonElement];
    private ids?:number;
    public static readonly STEP_CHANGE_EVENT:string="step_change_event";
    constructor(){
        super("div","stepnumber");
        this.elem.innerHTML=`
            <button disabled>-</button>
            <input type='text'>
            <button>+</button>
        `
        this.list=Array.from(this.elem.children) as [HTMLButtonElement,HTMLInputElement,HTMLButtonElement];
        this.setStyle();
        this.step=1;
        this.elem.addEventListener("click",(e:MouseEvent)=>this.clickHandler(e));
        this.elem.addEventListener("input",(e:Event)=>this.inputHandler(e));
    }
    private clickHandler(e:MouseEvent):void
    {
        var elem:HTMLElement=e.target as HTMLElement;
        if(elem.nodeName!=="BUTTON") return;
        if(elem.innerHTML==="-"){
            this.step--;
        }else{
            this.step++;
        }

    }
    private inputHandler(e:Event):void{
     
        this.list[1].value=this.list[1].value.replace(/\D/g,"");
        this.step=Number(this.list[1].value)
        if(this.ids) return;
        this.ids=setTimeout(()=>{
            clearTimeout(this.ids);
            this.ids=undefined;
            this.dispatch();
        },500);
    }
    public set step(value:number){
        
        this.list[0].disabled=false;
        this.list[2].disabled=false;
        if(value<=1){
            value=1;
            this.list[0].disabled=true;
        }else if(value>=99){
            value=99;
            this.list[2].disabled=true;
        }
        this._step=value;
        this.list[1].value=String(value);
    }
    public get step():number{
        return this._step;
    }
    protected setStyle(): void {
        StepNumber.setCss(`
            .stepnumber{
                position:relative;
                width:80px;
                height:20px;
                font-size:0;
            }
            .stepnumber>button{
                width:15px;
                height:18px;
                border:1px solid #999;
                padding:0;
                margin:0;
                outline:none;
            }
            .stepnumber>input{
                width:46px;
                height:16px;
                border:none;
                border-top:1px solid #999;
                border-bottom:1px solid #999;
                margin:0;
                padding:0;
                outline:none;
                text-align:center;
            }
        `)
    }
    protected dispatch(): void {
        var evt:IEmitter=new Event(StepNumber.STEP_CHANGE_EVENT);
        evt.step=this._step;
        this.dispatchEvent(evt as Event);
    }
}