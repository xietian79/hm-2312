export default class C{
    public static a:number=1;
    public num:number=1;
    public age:number=0;
    private static _instance:C;
    constructor(){

    }
    public static getInstance():C{
        if(!C._instance){
            C._instance=new C();
        }
        return C._instance;
    }
   
}