export default class MyTarget extends EventTarget{
    static _instance;
    constructor(){
        super();
    }
    static get instance(){
        return MyTarget._instance || (MyTarget._instance=new MyTarget());
    }

}