import {h, mergeProps,cloneVNode} from "vue";

const one={
    class:"div1",
    onClick:()=>{
        console.log("1")
    },
}

const two={
    class:{div2:true},
    onClick:()=>{
        console.log("2")
    }
}

const three=mergeProps(one,two);
// const three=Object.assign(one,two);


const a1= h("div",three,"div的内容");
// 类似于cloneNode
const a2=cloneVNode(a1,{id:"a2"});

export default {
    render(){
        return [
            a1,
            a2
        ]
    },
}