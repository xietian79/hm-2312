import { h } from "vue";

export default {
  name: "ChildA",
  render() {
    return [
      // h("div",{class:"div1"},"你好")
      h(
        "ul",
        { class: "ul1" },
        Array.from({ length: 6 }).map((item, index) =>
          h("li", null, [h("a", { href: "javascript:void(0)","onclick":()=>this.clickHandler(index) }, "超链接" + index)])
        )
      ),
    ];
  },
  methods: {
    clickHandler(index){
        console.log(index)
    }
  },
};
