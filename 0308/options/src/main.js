import { createApp } from 'vue'
import App from './App.vue'

var app=createApp(App)

// 国际化
const i18nPlugin = {
    install (app, options) {
      app.config.globalProperties.$t = function (item)  { // item welcome
        // console.log(item)
        // console.log(options)
        // console.log(this);//谁调用当前$t方法，this指向对应组件中的this
        return options[this.local][item]
      }
    }
  }



// 全局自定义指令
app.directive("color",{
    mounted(el,binding) {
        // console.log(el,binding)
        el.style.color=binding.value;
    },
    updated(el,binding){
        el.style.color=binding.value;
    }
})
app.use(i18nPlugin, {
    'en': { welcome: 'welcome', bye: 'goodbye' },
    'zh': { welcome: '欢迎', bye: '拜拜' }
  })
app.mount('#app')
