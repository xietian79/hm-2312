1、npm init -y 创建初始化配置文件,产生一个package.json的文件
{
  "name": "0220",      项目名称，不能和npmjs官网中插件名称重复，不能使用大写字母 不能使用-和_,可以使用/ @
  "version": "1.0.0",   项目版本  A.B.C  A大版本  B小版本  C微版本  
  "description": "",   描述内容
  "main": "a.js",    主文件名
  "type": "commonjs",   当前项目模块化类型
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"  脚本
  },
  "dependencies": {
    项目依赖 项目上线必须用到的模块包名
  },
  "devDependencies": {
    开发依赖 项目上线时不需要，但是开发过程需要使用的
  },
  "keywords": [],   关键词
  "author": "",  作者
  "license": "ISC" 专利
}

2、npm install 包名@版本   下载对应的包
npm i 包名@版本  不写版本就是最新的

npm i 包名 -g  全局安装 一般用来安装命令插件
http-server  创建一个web服务   npm i http-server -g
nodemon 修改node代码后会自动重启 npm i nodemon -g       使用   nodemon 文件名
nrm 修改npm下载镜像地址的命令  npm i nrm -g

    nrm ls  显示所有镜像地址
    nrm test 镜像名  测试镜像地址的速度
    nrm use 镜像名 使用镜像地址