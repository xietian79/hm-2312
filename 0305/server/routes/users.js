var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/list', function(req, res) {
  res.send([
    {id:1001,name:"商品1",price:1000,num:1},
    {id:1002,name:"商品2",price:2000,num:1},
    {id:1003,name:"商品3",price:3000,num:1},
    {id:1004,name:"商品4",price:4000,num:1},
    {id:1005,name:"商品5",price:5000,num:1},
]);
});
router.get("/update",function(req,res){
  res.send([
    {id:1001,name:"商品1",price:3000,num:1},
    {id:1002,name:"商品2",price:1000,num:2},
    {id:1003,name:"商品3",price:4000,num:4},
    {id:1004,name:"商品4",price:2000,num:6},
    {id:1005,name:"商品5",price:6000,num:1},
])
})

module.exports = router;
