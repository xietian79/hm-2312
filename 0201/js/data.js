export default  [
    {
        id: 1101,
        list: [
            { id: 110101, img: './img/110101.jpg', price: 1099 },
            { id: 110102, img: './img/110102.jpg', price: 1099 },
            { id: 110103, img: './img/110103.jpg', price: 1349 },
        ],
        info: '华为畅享 5000万高清AI三摄 5000mAh超能续航 128GB 宝石蓝 大内存',
        arguments: ['50z', '128GB', '白灰色'],
        judge: 61345,
        shop: ' 华为京东自营官方旗舰店',
        icons: {
            icon1: ['自营'],
        },
        titleIcon:"京品手机",
        double11: false,
        schedule: null,
    },
    {
        id: 1102,
        list: [
            { id: 110201, img: './img/110201.jpg', price: 1919 },
            { id: 110202, img: './img/110202.jpg', price: 1949 },
            { id: 110203, img: './img/110203.jpg', price: 1949 },
        ],
        info: 'HUAWEI 一亿像素质感人像 4500mAh长续航 轻薄机身128GB 10号色',
        arguments: ['nova 10 SE', '128GB', '柔性直屏', '青绿色'],
        judge: 50000,
        shop: ' 华为京东自营官方旗舰店',
        icons: {
            icon1: ['自营'],
            icon2: ['秒杀'],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 1103,
        list: [
            { id: 110301, img: './img/110301.jpg', price: 1499 },
            { id: 110302, img: './img/110302.jpg', price: 1495 },
            { id: 110303, img: './img/110303.jpg', price: 1329 },
        ],
        info: '华为/HUAWEI 一亿像素超清摄影 创新Vlog体验 支持66W快充',
        arguments: ['nova 9 SE', '256GB', '贝白色'],
        judge: 10000,
        shop: ' 京东手机优选自营旗舰店',
        icons: {
            icon1: ['自营'],
            icon2: ['放心购'],
            // icon3: ['新品'],
            // icon4: ['赠', '券'],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 1104,
        list: [
            { id: 110401, img: './img/110401.jpg', price: 1589 },
            { id: 110402, img: './img/110402.jpg', price: 1599 },
            { id: 110403, img: './img/110403.jpg', price: 1649 },
            { id: 110404, img: './img/110404.jpg', price: 1649 },
        ],
        info: '华为智选Hi 5G全网通 120Hz高刷 hinova9后置5000万超感光主摄 8+128GB',
        arguments: ['nova 9', '128GB', '紫色'],
        judge: 2000000,
        shop: ' 智能手机京喜专区',
        icons: {
            icon1: ['自营'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1105,
        list: [
            { id: 110501, img: './img/110501.jpg', price: 4269 },
            { id: 110502, img: './img/110502.jpg', price: 3669 },
            { id: 110503, img: './img/110503.jpg', price: 4099 },
            { id: 110504, img: './img/110504.jpg', price: 4099 },
        ],
        info: '荣耀70 IMX800三主摄设计 高通骁龙778G Plus 66W快充 5G手机 8GB+256GB ',
        arguments: ['70', '256GB', ' 双曲屏', '墨玉青'],
        judge: 2000,
        shop: ' 荣耀京东自营旗舰店',
        icons: {
            icon1: ['自营'],
            icon2: ['放心购'],
            icon3: ['满2150-100'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1106,
        list: [
            { id: 110601, img: './img/110601.jpg', price: 949 },
            { id: 110602, img: './img/110602.jpg', price: 1099 },
        ],
        info: '华为手机 华为畅享 5000mAh大电池6.麒麟芯片 搭载',
        arguments: ['20e', '256GB', '3英寸高清大屏', '墨绿色'],
        judge: 2000,
        shop: ' 京东手机优选自营旗舰店',
        icons: {
            icon1: ['自营'],
            icon2: ['放心购'],
            icon3: ['秒杀'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1107,
        list: [
            { id: 110701, img: './img/110701.jpg', price: 1549 },
            { id: 110702, img: './img/110702.jpg', price: 1549 },
            { id: 110703, img: './img/110703.jpg', price: 1649 },
        ],
        info: '华为智选 Hi 5G双模全网通 一亿像素超清摄影 66W疾速快充',
        arguments: ['nova9 SE', '128GB', '蓝白色'],
        judge: 200,
        shop: ' 智能手机京喜专区',
        icons: {
            icon1: ['自营'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1108,
        list: [
            { id: 110801, img: './img/110801.jpg', price: 2499 },
            { id: 110802, img: './img/110802.jpg', price: 2499 },
            { id: 110803, img: './img/110803.jpg', price: 2499 },
            { id: 110804, img: './img/110804.jpg', price: 2499 },
        ],
        info: 'HUAWEI  前置6000万超广角镜头 6.88mm轻薄机身 256GB 绮境森林 华为',
        arguments: ['nova 10', '256GB', '青绿色'],
        judge: 200,
        shop: ' 运营商手机京东自营专区',
        icons: {
            icon1: ['自营'],
            icon2: ['厂商配送'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1109,
        list: [
            { id: 110901, img: './img/110901.jpg', price: 1899 },
            { id: 110902, img: './img/110902.jpg', price: 1899 },
            { id: 110903, img: './img/110903.jpg', price: 1899 },
            { id: 110904, img: './img/110904.jpg', price: 1899 },
            { id: 110905, img: './img/110905.jpg', price: 1899 },
        ],
        info: 'Redmi  骁龙870 三星E4 AMOLED 120Hz直屏 OIS光学防抖 67W快充 幽芒 12GB+256GB 5G智能手机 小米红米',
        arguments: ['K40S', '256GB', '120Hz直屏', '亮黑'],
        judge: 500000,
        shop: '小米京东自营旗舰店',
        icons: {
            icon1: ['自营'],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1110,
        list: [
            { id: 111001, img: './img/111001.jpg', price: 1799 },
            { id: 111002, img: './img/111002.jpg', price: 1799 },
            { id: 111003, img: './img/111003.jpg', price: 1699 },
        ],
        info: 'OPPO 暗夜黑 8GB+256GB 天玑 8000-MAX 金刚石VC液冷散热 120Hz高帧变速屏 5G全网通 【移动用户特惠】',
        arguments: ['K10', '256GB', 'OLED直屏', '暗夜黑'],
        judge: 1000,
        shop: ' OPPO京东自营官方旗舰店',
        icons: {
            icon1: ['自营'],
            icon2: ['放心购'],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 1201,
        list: [
            { id: 120101, img: "./img/120101.jpg", price: 1699 },
            { id: 120102, img: "./img/120102.jpg", price: 1699 },
            { id: 120103, img: "./img/120103.jpg", price: 1699 },
            { id: 120104, img: "./img/120104.jpg", price: 1699 },

        ],
        info: "荣耀X40 120Hz 5100mAh 快充大电池 7.9mm轻薄设计 5G手机",
        arguments: ["X40", "256GB", " OLED硬核曲屏", "彩云追月", "幻夜黑"],

        judge: 200000,
        shop: "荣耀京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1202,
        list: [
            { id: 120201, img: "./img/120201.jpg", price: 2399 },
            { id: 120202, img: "./img/120202.jpg", price: 2099 },
            { id: 120203, img: "./img//120203.jpg", price: 2399 },


        ],
        info: "荣耀X40 GT 骁龙888旗舰芯 1 66W超级快充 5G手机 ",
        arguments: ["X40 GT", "12GB+256GB", " 44Hz高刷电竞屏", "太空银"],

        judge: 100000,
        shop: "荣耀京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["券"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1203,
        list: [
            { id: 120301, img: "./img/120301.jpg", price: 6399 },
            { id: 120302, img: "./img/120302.jpg", price: 6999 },
            { id: 120303, img: "./img/120303.jpg", price: 9098 },
        ],
        info: "OPPO Find N2 Flip  5000万超清自拍  ",
        arguments: ["12GB+256GB", " 120Hz镜面屏", "太流金 任意窗"],

        judge: 10000,
        shop: " OPPO京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["券"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1204,
        list: [
            { id: 120401, img: "./img/120401.jpg", price: 1099 },
            { id: 120402, img: "./img/120402.jpg", price: 1099 },

        ],
        info: "vivo iQOO U5x 骁龙680 5000mAh大电池 6.51英寸大屏幕 智慧三摄 全网通智能手机 ",
        arguments: ["12GB+256GB", " 120Hz镜面屏", "星光黑", "极昼蓝"],

        judge: 10000,
        shop: "vivo京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1205,
        list: [
            { id: 120501, img: "./img/120501.jpg", price: 9899 },


        ],
        info: "Apple iPhone 14 Pro Max (A2896) 支持移动联通电信5G 双卡双待手机 ",
        arguments: ["256GB ", "暗紫色 "],

        judge: 10000,
        shop: " Apple产品京东自营旗旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: null
    },



    {
        id: 1206,
        list: [
            { id: 120601, img: "./img/120601.jpg", price: 4399 },
            { id: 120602, img: "./img/120602.jpg", price: 4399 },

        ],
        info: "OPPO 一加 11 第二代骁龙 8   哈苏影像拍照  游戏电竞5G旗舰手机 ",
        arguments: ["12GB+256GB", " 2K + 120Hz 高刷屏", "无尽黑 "],

        judge: 50000,
        shop: " 一加京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1207,
        list: [
            { id: 120701, img: "./img/120701.jpg", price: 4599 },


        ],
        info: "小米13 徕卡光学镜头 第二代骁龙8处理器  67W快充  5G",
        arguments: ["12GB+256GB", "120Hz高刷", "超窄边屏幕", "黑色"],

        judge: 10000,
        shop: " 小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: null
    },

    {
        id: 1208,
        list: [
            { id: 120801, img: "./img/120801.jpg", price: 1919 },
            { id: 120802, img: "./img/120802.jpg", price: 1949 },
            { id: 120803, img: "./img/120803.jpg", price: 2249 },
        ],
        info: "HUAWEI  一亿像素质感人像 4500mAh长续航 轻薄机身 ",
        arguments: ["nova 10 SE", "128GB", " 120Hz镜面屏", "10号色"],

        judge: 50000,
        shop: " 华为京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["秒杀"],
            icon2: [""],
            icon3: [""],
            icon4: [""]
        },
        double11: false,
        schedule: null
    },

    {
        id: 1209,
        list: [
            { id: 120901, img: "./img/120901.jpg", price: 2499 },
            { id: 120901, img: "./img/120902.jpg", price: 2499 },
            { id: 120901, img: "./img/120903.jpg", price: 2499 },
            { id: 120901, img: "./img/120904.jpg", price: 2499 },
        ],
        info: "OPPO    6400万水光人像镜头 120Hz  4500mAh大电池 7.19mm轻薄 5G手机 ",
        arguments: ["Reno9", "12GB+256GB", " OLED超清曲面屏", "微醺"],

        judge: 50000,
        shop: " OPPO京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""]
        },
        double11: false,
        schedule: null
    },




    {
        id: 1210,
        list: [
            { id: 121001, img: "./img/121001.jpg", price: 4928 },
            { id: 121002, img: "./img/121002.jpg", price: 4799 },
            { id: 121003, img: "./img/121003.jpg", price: 4928 },
            { id: 121004, img: "./img/121004.jpg", price: 4799 },
            { id: 121005, img: "./img/121005.jpg", price: 4799 },
        ],
        info: "Apple iPhone 13  蓝色 支持移动联通电信5G 双卡双待手机 ",
        arguments: ["A2634", "128GB", " 120Hz镜面屏", "粉色"],

        judge: 1000000,
        shop: " Apple京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 1301,
        list: [
            { id: 130101, img: "./img/130101.jpg", price: 2699 },
        ],
        info: "魅族 18s 8GB+128GB 渡海 5G 骁龙888+",
        arguments: [
            "36W超充",
            "6.2英寸2K曲面屏",
            "6400W高清三摄光学防抖"
        ],
        judge: 30000,
        shop: "魅族京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1302,
        list: [
            { id: 130201, img: "./img/130201.jpg", price: 1999 },
            { id: 130202, img: "./img/130202.jpg", price: 1599 },
        ],
        info: "魅族 18X 12GB+256GB 岚 5G 骁龙870",
        arguments: ["120Hz刷新率", "4300mAh大电池", "6400万AI超清三摄"],
        judge: 100000,
        shop: "魅族京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1303,
        list: [
            { id: 130301, img: "./img/130301.jpg", price: 4999 },
            { id: 130302, img: "./img/130302.jpg", price: 4999 },
        ],
        info: "魅族 18s Pro 12GB+256GB 飞雪流光 5G 骁龙888+",
        arguments: ["140W无线超充", "2K曲面屏", " AR全场景大师影像系统"],
        judge: 20000,
        shop: "魅族京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1304,
        list: [
            { id: 130401, img: "./img/130401.jpg", price: 1799 },
            { id: 130402, img: "./img/130402.jpg", price: 1799 },
        ],
        info: "魅族16th/16th Plus 全面屏游戏手机 骁龙845",
        arguments: ["二手手机", "静夜黑", "9成新"],
        judge: 2000,
        shop: "魅族京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1305,
        list: [
            { id: 130501, img: "./img/130501.jpg", price: 799 },
            { id: 130502, img: "./img/130502.jpg", price: 799 },
            { id: 130503, img: "./img/130503.jpg", price: 799 }
        ],
        info: "魅蓝10 全网通4G(MEIZU)魅族手机",
        arguments: [
            " 5000mAh大电池",
            " 6.52英寸大屏",
            "学生机老人机备用机",
            "星光白",
        ],
        judge: 5000,
        shop: "魅族甄选专卖店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1306,
        list: [
            { id: 130601, img: "./img/130601.jpg", price: 1829 },
            { id: 130602, img: "./img/130602.jpg", price: 1829 },
            { id: 130603, img: "./img/130603.jpg", price: 1949 },
            { id: 130604, img: "./img/130604.jpg", price: 1949 },
        ],
        info: "魅族17 十七PRO 骁龙865 旗舰5G",
        arguments: ["5000万四摄", "90Hz屏幕", " 支持NFC", "十七度灰"],
        judge: 200000,
        shop: "魅族京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1307,
        list: [
            { id: 130701, img: "./img/130701.jpg", price: 899 },
            { id: 130702, img: "./img/130702.jpg", price: 899 },
            { id: 130703, img: "./img/130703.jpg", price: 899 }
        ],
        info: "魅蓝10S 全网通4G手机",
        arguments: ["5000mAh大电池", "6.52英寸刘海屏", "学生机老人机", "幻夜黑"],
        judge: 20000,
        shop: "魅族甄选专卖店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1308,
        list: [
            { id: 130801, img: "./img/130801.jpg", price: 699 },
            { id: 130802, img: "./img/130802.jpg", price: 699 }
        ],
        info: "MEIZU魅族 魅蓝note2 移动联通4G手机",
        arguments: ["2GB+16GB", "灰色"],
        judge: 10000,
        shop: "魅族甄选专卖店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1309,
        list: [
            { id: 130901, img: "./img/130901.jpg", price: 1059 },
            { id: 130902, img: "./img/130902.jpg", price: 1059 },
            { id: 130903, img: "./img/130903.jpg", price: 1059 },
        ],
        info: "魅族16t全面屏三摄拍照娱乐游戏手机-4500mAh大电池",

        arguments: ["95新成色", "鲸跃蓝", "6G 128G全网通"],
        judge: 100,
        shop: "魅族甄选专卖店",
        icons: {
            icon1: [""],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1310,
        list: [
            { id: 131001, img: "./img/131001.jpg", price: 599 },
            { id: 131002, img: "./img/131002.jpg", price: 599 },
            { id: 131003, img: "./img/131003.jpg", price: 599 },
        ],
        info: "魅族 魅族X8 骁龙710",
        arguments: ["游戏拍照安卓二手手机", "亮黑", "6GB+64GB"],
        judge: 500,
        shop: "魅族甄选专卖店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 1401,
        list: [
            { id: 140101, img: "./img/140101.jpg", price: 4699 },
            { id: 140102, img: "./img/140102.jpg", price: 3999 }
        ],
        info: "努比亚（nubia）红魔7S 16GB+512GB 氘锋透明 新骁龙8+ 稳帧引擎 ICE魔冷散热 165Hz高刷 5G电竞游戏手机",
        arguments: ["16GB+512GB", "新骁龙8+", "ICE魔冷散热", "165Hz高刷"],
        judge: 5050,
        shop: "努比亚京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: ["满2000-300"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1402,
        list: [
            { id: 140201, img: "./img/140201.jpg", price: 5399 },
            { id: 140202, img: "./img/140202.jpg", price: 5999 },
            { id: 140203, img: "./img/140203.jpg", price: 4399 },
            { id: 140204, img: "./img/140204.jpg", price: 4299 },
            { id: 140205, img: "./img/140205.jpg", price: 5999 },
        ],
        info: "ROG游戏手机6 16GB+512GB 暗影黑 骁龙8+Gen1 矩阵式液冷散热6.0 165Hz三星电竞屏 2x3Plus肩键 5G ROG6 ",
        arguments: ["16+512暗影黑/幻影白", "12+256暗影黑/天玑版太空灰", "12+128暗影黑/幻影白"],
        judge: 15555,
        shop: "玩家国度ROG京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: []
        },
        double11: false,
        schedule: null
    }, {
        id: 1403,
        list: [
            { id: 140301, img: "./img/140301.jpg", price: 2199 },
            { id: 140302, img: "./img/140302.jpg", price: 2199 },
            { id: 140303, img: "./img/140303.jpg", price: 1999 },
        ],
        info: "Redmi K60E 天玑8200处理器 2K旗舰直屏 OIS光学防抖相机 5500mAh长续航67W充电 8GB+256GB 墨羽 小米红米5G",
        arguments: ["K60E", "天玑8200处理器", "2K旗舰直屏", "OIS光学防抖相机"],
        judge: 78,
        shop: "京东手机优选自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [],
            icon4: ["赠"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1404,
        list: [
            { id: 140401, img: "./img/140401.jpg", price: 2499 },
            { id: 140402, img: "./img/140402.jpg", price: 2299 },
            { id: 140403, img: "./img/140403.jpg", price: 1499 },
        ],
        info: "vivo iQOO Neo6 SE 12GB+256GB 星际 高通骁龙870 双电芯80W闪充 OIS光学防抖  双模5G全网通",
        arguments: [],
        judge: 204651,
        shop: "iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [],
            icon4: ["满2000-100"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1405,
        list: [
            { id: 140501, img: "./img/140501.jpg", price: 1459 },
            { id: 140502, img: "./img/140502.jpg", price: 1399 },
            { id: 140503, img: "./img/140503.jpg", price: 1399 },
            { id: 140504, img: "./img/140504.jpg", price: 1699 },
        ],
        info: "Redmi Note11T Pro  5G 天玑8100 144HzLCD旗舰直屏 67W快充 6GB+128GB子夜黑 5G智能手机 小米红米",
        arguments: ["5G", "天玑8100", "144HzLCD旗舰直屏", "67W快充"],
        judge: 504651,
        shop: "小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: []
        },
        double11: false,
        schedule: null
    }, {
        id: 1406,
        list: [
            { id: 140601, img: "./img/140601.jpg", price: 1599 },
            { id: 140602, img: "./img/140602.jpg", price: 1599 },
            { id: 140603, img: "./img/140603.jpg", price: 1599 },
            { id: 140604, img: "./img/140604.jpg", price: 1599 },
            { id: 140605, img: "./img/140605.jpg", price: 1499 },
        ],
        info: "小米11青春版 骁龙780G处理器  AMOLED柔性直屏 8GB+256GB 清凉薄荷 5G时尚",
        arguments: ["骁龙780G处理器", "AMOLED柔性直屏"],
        judge: 202156,
        shop: "小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: ["赠"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1407,
        list: [
            { id: 140701, img: "./img/140701.jpg", price: 2799 },
            { id: 140702, img: "./img/140702.jpg", price: 2799 },
            { id: 140703, img: "./img/140703.jpg", price: 2299 },
        ],
        info: "vivo iQOO Neo6 SE 12GB+512GB 星际 高通骁龙870 双电芯80W闪充 OIS光学防抖 双模5G全网通",
        arguments: [],
        judge: 5312,
        shop: "京东手机优选自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: ["满2000-300"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1408,
        list: [
            { id: 140801, img: "./img/140801.jpg", price: 3179 },
            { id: 140802, img: "./img/140802.jpg", price: 3179 },
            { id: 140803, img: "./img/140803.jpg", price: 3179 },
        ],
        info: "荣耀 Magic3 Pro 骁龙888Plus 6.76英寸超曲屏 多主摄计算摄影 66W有线50W无线双超级快充 8GB+256GB 亮黑色",
        arguments: ["骁龙888Plus", "6.76英寸超曲屏", "多主摄计算摄影", "66W有线50W无线双超级快充"],
        judge: 5312,
        shop: "京东手机优选自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [],
            icon3: [],
            icon4: ["满3169-100"]
        },
        double11: false,
        schedule: null
    }, {
        id: 1409,
        list: [
            { id: 140901, img: "./img/140901.jpg", price: 3099 },
            { id: 140902, img: "./img/140902.jpg", price: 3099 },
            { id: 140903, img: "./img/140903.jpg", price: 3099 },
            { id: 140904, img: "./img/140904.jpg", price: 3099 },
        ],
        info: "荣耀80 1.6亿像素超清主摄 AI Vlog视频大师 全新Magic OS 7.0系统 5G",
        arguments: [],
        judge: 532,
        shop: "京东手机优选自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [],
            icon4: ["满3169-100", "赠"]
        },
        double11: false,
        schedule: null
    }
    ,
    {
        id: 1501,
        list: [
            { id: 150101, img: "./img/150101.jpg", price: 1499 },
            { id: 150102, img: "./img/150102.jpg", price: 1499 },
            { id: 150103, img: "./img/150103.jpg", price: 1598 },

        ],
        info: "vivo iQOO Z6x 8GB+256GB 黑镜 6000mAh巨量电池 44W闪充 6nm强劲芯 5G智能",
        arguments: ["Z6x", "256GB", "6000mAh", "5G"],
        judge: "100000",
        shop: "iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1502,
        list: [
            { id: 150201, img: "./img/150201.jpg", price: 2499 },
            { id: 150202, img: "./img/150202.jpg", price: 2499 },
            { id: 150203, img: "./img/150203.jpg", price: 2499 },

        ],
        info: "vivo iQOO Neo7 SE 12GB+256GB 银河  天玑8200 120W超快闪充 120Hz柔性直屏 5G电竞",
        arguments: ["SE", "256GB", "天玑8200", "5G"],
        judge: "50000",
        shop: "iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满2000-200"]

        },
        double11: false,
        schedule: null
    },
    {
        id: 1503,
        list: [
            { id: 150301, img: "./img/150301.jpg", price: 2999 },
            { id: 150302, img: "./img/150302.jpg", price: 2999 },
            { id: 150303, img: "./img/150303.jpg", price: 2799 },

        ],
        info: "vivo iQOO Neo7竞速版 12GB+256GB 几何黑 骁龙8+旗舰芯片 独显芯片Pro+ 120W超快闪充 5G电竞",
        arguments: ["Neo7", "256GB", "骁龙8+旗舰芯片", "5G"],
        judge: "20000",
        shop: "iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满2000-200"],
            icon4: [""],

        },
        double11: false,
        schedule: null
    },
    {
        id: 1504,
        list: [
            { id: 150401, img: "./img/150401.jpg", price: 1699 },
            { id: 150402, img: "./img/150402.jpg", price: 1699 },
            { id: 150403, img: "./img/150403.jpg", price: 1699 },

        ],
        info: "vivo Y73t 12GB+256GB 镜黑 6000mAh大电池 44W疾速闪充 后置5000万像素 150%超大音量 5G 拍照 ",
        arguments: ["Y73t", "256GB", "6000mAh", "5G"],
        judge: "20000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],

        },
        double11: false,
        schedule: null
    },
    {
        id: 1505,
        list: [
            { id: 150501, img: "./img/150501.jpg", price: 3999 },


        ],
        info: "vivo X90 8GB+256GB 告白 4nm天玑9200旗舰芯片 自研芯片V2 120W双芯闪充 蔡司影像 5G 拍照  ",
        arguments: ["X90", "256GB", "4nm天玑", "5G"],
        judge: "50000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1506,
        list: [
            { id: 150601, img: "./img/150601.jpg", price: 1799 },
            { id: 150602, img: "./img/150602.jpg", price: 1799 },
            { id: 150603, img: "./img/150603.jpg", price: 1999 },
        ],
        info: "vivo iQOO Z6 8GB+256GB 墨玉 80W闪充 6400万像素光学防抖 骁龙778G Plus 5G智能",
        arguments: ["Z6", "256GB", "80W闪充", "6400万像素"],
        judge: "50000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],

        },
        double11: false,
        schedule: null
    },
    {
        id: 1507,
        list: [
            { id: 150701, img: "./img/150701.jpg", price: 3299 },
            { id: 150702, img: "./img/150702.jpg", price: 3299 },



        ],
        info: "vivo S16 Pro 12GB+256GB 颜如玉 天玑8200旗舰芯片 前置5000万追焦人像 原彩柔光环 5G 拍照 ",
        arguments: ["S16", "256GB", "玑8200旗舰芯片", "5G"],
        judge: "10000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["劵2000-50"],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1508,
        list: [
            { id: 150801, img: "./img/150801.jpg", price: 1099 },
            { id: 150802, img: "./img/150802.jpg", price: 1099 },



        ],
        info: "vivo iQOO U5x 8GB+128GB星光黑 骁龙680 5000mAh大电池 6.51英寸大屏幕 智慧三摄 全网通智能",
        arguments: ["U5x", "骁龙680", "5000mAh", "6.51英寸"],
        judge: "100000",
        shop: "iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["满1000-80"],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1509,
        list: [
            { id: 150901, img: "./img/150901.jpg", price: 6999 },




        ],
        info: "vivo X90 Pro+ 12GB+512GB 原黑 蔡司一英寸T*主摄 自研芯片V2 第二代骁龙8移动平台 5G 拍照 ",
        arguments: ["X90", "256GB", "自研芯片V2", "5G"],
        judge: "20000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1510,
        list: [
            { id: 151001, img: "./img/151001.jpg", price: 1799 },
            { id: 151002, img: "./img/151002.jpg", price: 1799 },
            { id: 151003, img: "./img/151003.jpg", price: 1999 },


        ],
        info: "vivo S16e 12GB+256GB 风信紫 5000万柔光人像 原彩柔光环 OIS超稳光学防抖 5nm旗舰芯片 5G 拍照 ",
        arguments: ["S16e", "256GB", "5000万柔光人像", "5nm旗舰芯片"],
        judge: "10000",
        shop: "vivo京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠"],


        },
        double11: false,
        schedule: null
    },
    {
        id: 1601,
        list: [{ id: 160101, img: "./img/160101.jpg", price: 4599 }],
        info: "小米13 徕卡光学镜头 第二代骁龙8处理器",
        arguments: [
            "超窄边屏幕",
            "120Hz高刷",
            "67W快充",
            "12+256GB",
            "黑色",
            "5G手机",
        ],
        judge: 100000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1602,
        list: [
            { id: 160201, img: "./img/160201.jpg", price: 4999 },
            { id: 160202, img: "./img/160202.jpg", price: 4599 },
            { id: 160203, img: "./img/160203.jpg", price: 4299 },
            { id: 160204, img: "./img/160204.jpg", price: 4599 },
            { id: 160205, img: "./img/160205.jpg", price: 4599 },
        ],
        info: "小米13 徕卡光学镜头 第二代骁龙8处理器",
        arguments: ["超窄边屏幕", "120Hz高刷", "67W快充"],
        judge: 100000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠"],
        },
        double11: false,
        schedule: {
            img: "./img/1602.gif",
            info: "抢购中",
        },
    },

    {
        id: 1603,
        list: [
            { id: 160301, img: "./img/160301.jpg", price: 1899 },
            { id: 160302, img: "./img/160302.jpg", price: 1899 },
            { id: 160303, img: "./img/160303.jpg", price: 1899 },
            { id: 160304, img: "./img/160304.jpg", price: 1899 },
            { id: 160305, img: "./img/160305.jpg", price: 1899 },
        ],
        info: "Redmi K40S 骁龙870 三星E4 AMOLED",
        arguments: ["120Hz直屏", "OIS光学防抖", " 亮黑"],
        judge: 500000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1604,
        list: [
            { id: 160401, img: "./img/160401.jpg", price: 1799 },
            { id: 160402, img: "./img/160402.jpg", price: 1799 },
            { id: 160403, img: "./img/160403.jpg", price: 1799 },
        ],
        info: "Redmi Note12Pro极速版 5G 骁龙高能芯",

        arguments: ["一亿像素", "旗舰影像", " OLED柔性直屏"],
        judge: 20000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1605,
        list: [{ id: 160501, img: "./img/160501.jpg", price: 5799 }],
        info: "小米13Pro徕卡光学镜头 120W秒充 2K曲面屏",
        arguments: [
            " 120Hz高刷",
            " 120W秒充",
            "12+256GB",
            "陶黑色",
            "5G手机",
        ],
        judge: 50000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1606,
        list: [
            { id: 160601, img: "./img/160601.jpg", price: 829 },
            { id: 160602, img: "./img/160602.jpg", price: 829 },
            { id: 160603, img: "./img/160603.jpg", price: 949 },
        ],
        info: "Redmi Note 11 4G FHD+ 90Hz高刷屏",

        arguments: ["5000万三摄", "G88芯片", " 5000mAh电池"],
        judge: 200000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1607,
        list: [{ id: 160701, img: "./img/160701.jpg", price: 3899 }],
        info: "【爆款热销】K60Pro 骁龙8 IMX相机",
        arguments: ["120W秒充", "12GB+256GB", " 墨羽"],
        judge: 20000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1608,
        list: [{ id: 160801, img: "./img/160801.jpg", price: 8999 }],
        info: "小米MIX Fold2 轻薄折叠 骁龙8+旗舰处理器 徕卡光学镜头 自研微水滴形态转轴",
        arguments: ["12GB+256GB", "玄夜黑"],
        judge: 10000,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1609,
        list: [
            { id: 160901, img: "./img/160901.jpg", price: 1059 },
            { id: 160902, img: "./img/160902.jpg", price: 1059 },
        ],
        info: "小米 红米Note9 5G手机 8G+128G 青山外 全网通",

        arguments: ["8G+128G", "云墨灰", "全网通"],
        judge: 100,
        shop: "小米甄选专卖店",
        icons: {
            icon1: [""],
            icon2: [""],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },

    {
        id: 1610,
        list: [
            { id: 161001, img: "./img/161001.jpg", price: 1199 },
            { id: 161002, img: "./img/161002.jpg", price: 1199 },
            { id: 161003, img: "./img/161003.jpg", price: 1029 },
        ],
        info: "Redmi Note 11 5G 天玑810 33W Pro快充",
        arguments: ["5000mA5000mAh大电池", "微光晴蓝", "8GB+256GB"],
        judge: 500,
        shop: "京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: [""],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 1701,
        list: [
            { id: 170101, img: "./img/170101.jpg", price: 8199 },
        ],
        info: "Apple iPhone 苹果14pro（A2892)",
        arguments: ["iPhone14pro", "A16芯片", "5G手机", "暗紫色"],
        judge: "5000+",
        shop: "京联数科旗舰店",
        icons:
        {

        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1702,
        list: [
            { id: 170201, img: "./img/170201.jpg", price: 5399 },
            { id: 170202, img: "./img/170202.jpg", price: 5399 },
            { id: 170203, img: "./img/170203.jpg", price: 5399 },
            { id: 170204, img: "./img/170204.jpg", price: 5399 },
            { id: 170205, img: "./img/170205.jpg", price: 5399 },

        ],
        info: "Apple iPhone 14 (A2884)",
        arguments: ["128GB", "星光色", "支持移动联通电信5G", "双卡双待手机"],
        judge: "200万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["券4000-300"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1703,
        list: [
            { id: 170301, img: "./img/170301.jpg", price: 8899 },
            { id: 170302, img: "./img/170302.jpg", price: 8899 },
            { id: 170303, img: "./img/170303.jpg", price: 8899 },
            { id: 170304, img: "./img/170304.jpg", price: 8899 },

        ],
        info: "Apple iPhone 14 Pro (A2892)",
        arguments: ["256GB", "暗紫色", "支持移动联通电信5G", "双卡双待手机"],
        judge: "50万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1704,
        list: [
            { id: 170401, img: "./img/170401.jpg", price: 9899 },
            { id: 170402, img: "./img/170402.jpg", price: 9899 },
            { id: 170403, img: "./img/170403.jpg", price: 9899 },
            { id: 170404, img: "./img/170404.jpg", price: 9899 },

        ],
        info: "Apple iPhone 14 Pro Max (A2896)",
        arguments: ["256GB", "暗紫色", "支持移动联通电信5G", "双卡双待手机"],
        judge: "50万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["券4000-850"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1705,
        list: [
            { id: 170501, img: "./img/170501.jpg", price: 4928 },
            { id: 170502, img: "./img/170502.jpg", price: 4799 },
            { id: 170503, img: "./img/170503.jpg", price: 4799 },
            { id: 170504, img: "./img/170504.jpg", price: 4799 },
            { id: 170505, img: "./img/170505.jpg", price: 4799 },

        ],
        info: "Apple iPhone 13 (A2634)",
        arguments: ["128GB", "星光色", "支持移动联通电信5G", "双卡双待手机"],
        judge: "100万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["券4000-150"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1706,
        list: [
            { id: 170601, img: "./img/170601.jpg", price: 6199 },
            { id: 170602, img: "./img/170602.jpg", price: 6199 },
            { id: 170603, img: "./img/170603.jpg", price: 7099 },
            { id: 170604, img: "./img/170604.jpg", price: 6199 },

        ],
        info: "Apple iPhone 14 Plus (A2888)",
        arguments: ["128GB", "蓝色", "支持移动联通电信5G", "双卡双待手机"],
        judge: "20万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["券4000-200"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1707,
        list: [
            { id: 170701, img: "./img/170701.jpg", price: 7099 },

        ],
        info: "【资源机】苹果 13pro Apple iPhone 13 pro 国行 拍拍严选 二手手机",
        arguments: ["5G", "远峰蓝色", "256G"],
        judge: "500+",
        shop: "拍拍严选官方旗舰店",
        icons:
        {

        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1708,
        list: [
            { id: 170801, img: "./img/170801.jpg", price: 7759 },
            { id: 170802, img: "./img/170802.jpg", price: 9089 },
            { id: 170803, img: "./img/170803.jpg", price: 9109 },

        ],
        info: "Apple 苹果 iphone 14 pro max",
        arguments: ["全网通5G手机", "暗夜紫", "128G"],
        judge: "2000+",
        shop: "微焱数码旗舰店",
        icons:
        {
            icon1: ["免邮"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 1709,
        list: [
            { id: 170901, img: "./img/170901.jpg", price: 4899 },
            { id: 170902, img: "./img/170902.jpg", price: 4399 },
            { id: 170903, img: "./img/170903.jpg", price: 4899 },
            { id: 170904, img: "./img/170904.jpg", price: 4899 },
            { id: 170905, img: "./img/170905.jpg", price: 4899 },
            { id: 170906, img: "./img/170906.jpg", price: 4899 },

        ],
        info: "Apple iPhone 12 (A2404)",
        arguments: ["128G", "蓝色", "支持移动联通电信5G", "双卡双待"],
        judge: "400万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["满3000-300"],
        },
        double11: false,
        schedule: {

        }
    },
    {
        id: 1710,
        list: [
            { id: 171001, img: "./img/171001.jpg", price: 3899 },
            { id: 171002, img: "./img/171002.jpg", price: 3499 },
            { id: 171003, img: "./img/171003.jpg", price: 3499 },

        ],
        info: "Apple iPhone SE(A2785)",
        arguments: ["128G", "星光色", "支持移动联通电信5G手机"],
        judge: "2万+",
        shop: "Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["满2000-200"],
        },
        double11: false,
        schedule: {

        }
    },
    //------------------------------------二-------------------------------------------
    {
        id: 2101,
        list: [
            { id: 210101, img: "./img/210101.jpg", price: 1399 },
            { id: 210102, img: "./img/210102.jpg", price: 1399 },
            { id: 210103, img: "./img/210103.jpg", price: 1399 },
        ],
        info: "OPPO K10x 极光 8GB+128GB 67W超级闪充 5000mAh长续航 120Hz高帧屏 6400万三摄 高通骁龙695 拍照 5G",
        arguments: ["A16", "256GB", "OLED直屏", "暗紫色"],
        judge: 100000,
        shop: " OPPO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠", "券"],
        },
        double11: false,
    },
    {
        id: 2102,
        list: [
            { id: 210201, img: "./img/210201.jpg", price: 4799 },
            { id: 210202, img: "./img/210202.jpg", price: 4799 },
            { id: 210203, img: "./img/210203.jpg", price: 4799 },
            { id: 210204, img: "./img/210204.jpg", price: 4799 },
            { id: 210205, img: "./img/210205.jpg", price: 4799 },
            { id: 210206, img: "./img/210206.jpg", price: 4799 },
            { id: 210207, img: "./img/210207.jpg", price: 4799 },
        ],
        info: "Apple iPhone 13 (A2634) 128GB 星光色 支持移动联通电信5G 双卡双待",
        arguments: ["A2634", "星光色", "128GB"],
        judge: 5000000,
        shop: " Apple产品京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["券4000-150"],
        },
        double11: false,
    },
    {
        id: 2103,
        list: [
            { id: 210301, img: "./img/210301.jpg", price: 9899 },
            { id: 210302, img: "./img/210302.jpg", price: 9899 },
            { id: 210303, img: "./img/210303.jpg", price: 9899 },
            { id: 210304, img: "./img/210304.jpg", price: 9899 },
            { id: 210305, img: "./img/210305.jpg", price: 9899 },
        ],
        info: "Apple iPhone 14 Pro Max (A2896) 256GB 暗紫色 支持移动联通电信5G 双卡双待",
        arguments: ["A2896", "256GB", "暗紫色", "支持移动联通电信5G 双卡双待"],
        judge: 5000000,
        shop: " Apple产品京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["券4000-150"],
        },
    },
    {
        id: 2104,
        list: [
            { id: 210401, img: "./img/210401.jpg", price: 3799.9 },
            { id: 210102, img: "./img/210102.jpg", price: 1399 },
            { id: 210103, img: "./img/210103.jpg", price: 1399 },
        ],
        info: "联想笔记本电脑 小新Air14 英特尔酷睿i5 14英寸轻薄本(i5 16G 512G 高色域 大电池)银  商务办公手提电脑",
        arguments: ["英特尔酷睿i5", "i5 16G 512G", "商务办公手提电脑"],
        judge: 200000,
        shop: " 联想京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["满十-0.9"],
        },
        double11: false,
    },
    {
        id: 2105,
        list: [{ id: 210501, img: "./img/210501.jpg", price: 4799 }],
        info: " 荣耀Magic5 荣耀鹰眼相机 第二代骁龙8旗舰芯片 5100mAh电池 5G手机 12GB+256GB 勃朗蓝 ",
        arguments: ["12GB+256GB", "勃朗蓝"],
        judge: 0,
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
    },
    {
        id: 2106,
        list: [
            { id: 210601, img: "./img/210601.jpg", price: 4599 },
            { id: 210602, img: "./img/210602.jpg", price: 4599 },
        ],
        info: "小米13 徕卡光学镜头 第二代骁龙8处理器",
        arguments: ["第二代骁龙8处理器", "256GB", "OLED直屏", "暗紫色"],
        judge: 100000,
        shop: " 小米京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
    },
    {
        id: 2107,
        list: [
            { id: 210701, img: "./img/210701.jpg", price: 2499 },
            { id: 210702, img: "./img/210702.jpg", price: 2499 },
            { id: 210703, img: "./img/210703.jpg", price: 2499 },
            { id: 210704, img: "./img/210704.jpg", price: 2499 },
            { id: 210704, img: "./img/210705.jpg", price: 2499 },
        ],
        info: "OPPO Reno9 8GB+256GB 微醺 6400万水光人像镜头 120Hz OLED超清曲面屏 4500mAh大电池 7.19mm轻薄 5G",
        arguments: ["PPO Reno9", "8GB+256GB", "OLED超清曲面屏", "明日金"],
        judge: 50000,
        shop: " OPPO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["券1000-200"],
        },
        double11: false,
    },
    {
        id: 2108,
        list: [
            { id: 210801, img: "./img/210801.jpg", price: 1599 },
            { id: 210802, img: "./img/210802.jpg", price: 1599 },
            { id: 210803, img: "./img/210803.jpg", price: 1599 },
        ],
        info: "小米11青春版 骁龙780G处理器  AMOLED柔性直屏 8GB+256GB 清凉薄荷 5G时尚手机",
        arguments: ["骁龙780G处理器", "8GB+256GB", "AMOLED柔性直屏", "清凉夏"],
        judge: 200000,
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon4: ["赠", "券"],
        },
        double11: false,
    },
    {
        id: 2109,
        list: [
            { id: 210901, img: "./img/210901.jpg", price: 3999 },
            { id: 210902, img: "./img/210902.jpg", price: 3999 },
        ],
        info: "vivo X90 8GB+256GB 告白 4nm天玑9200旗舰芯片 自研芯片V2 120W双芯闪充 蔡司影像 5G 拍照",
        arguments: ["4nm天玑9200旗舰芯片", "8GB+256GB", "120W双芯闪充", "告白"],
        judge: 50000,
        shop: " vivo京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
    },
    {
        id: 2110,
        list: [
            { id: 211001, img: "./img/211001.jpg", price: 8999 },
            { id: 211002, img: "./img/211002.jpg", price: 8999 },
        ],
        info: "华为/HUAWEI Mate Xs 2 升级支持北斗卫星消息 超轻薄超平整超可靠 8GB+256GB雅黑折叠屏",
        arguments: ["Mate Xs 2", "8GB+256GB", "雅黑折叠屏", "暗紫色"],
        judge: 20000,
        shop: " 华为京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
    },
   
    {
        id: 2205,
        list: [
            { id: 220501, img: "./img/220501.jpg", price: 1399 },
            { id: 220502, img: "./img/220502.jpg", price: 1399 },

        ],
        info: "OPPO K10x 极光 8GB+128GB 67W超级闪充 5000mAh长续航 120Hz高帧屏 6400万三摄 高通骁龙695 拍照 5G",
        arguments: ["高通骁龙695", "256GB", "LCD", "极光"],
        judge: "10万+",
        shop: " OPPO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["券1280-100"]
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    {
        id: 2206,
        list: [
            { id: 220601, img: "./img/220601.jpg", price: 1699 },
            { id: 220602, img: "./img/220602.jpg", price: 1699 },
            { id: 220603, img: "./img/220603.jpg", price: 1699 },
            { id: 220604, img: "./img/220604.jpg", price: 1699 },

        ],
        info: "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G",
        arguments: ["骁龙600", "256GB", "OLED曲屏", "琥珀星光"],
        judge: "20万+",
        shop: " 荣耀京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["券1670-100"]
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    {
        id: 2207,
        list: [
            { id: 220701, img: "./img/220701.jpg", price: 1099 },
            { id: 220702, img: "./img/220702.jpg", price: 1099 },
            { id: 220703, img: "./img/220703.jpg", price: 1319 },

        ],
        info: "华为畅享 50z 5000万高清AI三摄 5000mAh超能续航 128GB 大内存鸿蒙智能",
        arguments: ["5000mAh", "256GB", "LCD", "宝石蓝"],
        judge: "5万+",
        shop: " 华为京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    {
        id: 2208,
        list: [
            { id: 220801, img: "./img/220801.jpg", price: 2499 },

        ],
        info: "OPPO Reno9 轻薄长续航 金丝琉璃工艺",
        arguments: ["骁龙778G", "256GB", "OLED曲面屏", "微醺"],
        judge: "5万+",
        shop: " OPPO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    {
        id: 2209,
        list: [
            { id: 220901, img: "./img/220901.jpg", price: 1009 },
            { id: 220902, img: "./img/220902.jpg", price: 1099 },
            { id: 220903, img: "./img/220903.jpg", price: 1019 },

        ],
        info: "Redmi Note 11 5G 33W Pro快充 5000mAh大电池  6GB +128GB 智能",
        arguments: ["天玑810", "256GB", "LCD", "浅梦星河"],
        judge: "100万+",
        shop: " 小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    {
        id: 2210,
        list: [
            { id: 221001, img: "./img/221001.jpg", price: 5399 },
            { id: 221002, img: "./img/221002.jpg", price: 5399 },
            { id: 221003, img: "./img/221003.jpg", price: 5399 },
            { id: 221004, img: "./img/221004.jpg", price: 5399 },

        ],
        info: "Apple iPhone 14 (A2884) 128GB 星光色 支持移动联通电信5G 双卡双待",
        arguments: ["A15", "256GB", "OLED直屏", "午夜色"],
        judge: "200万+",
        shop: " Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon: ["券4000-300"]
        },
        double11: false,
        schedule: {
            img: null,
            info: null
        }
    },
    // 3
    {
        id: 2301,
        list: [
            { id: 230101, img: "./img/230101.jpg", price: 2999 },
            { id: 230102, img: "./img/230102.jpg", price: 2999 },
            { id: 230103, img: "./img/230103.jpg", price: 2999 },
        ],
        info: "vivo iQOO Neo7 12GB+256GB 天玑9000+ 独显芯片Pro+ E5柔性直屏",
        arguments: ["天玑9000", "256GB", "E5柔性直屏", "几何黑"],
        judge: "10万+",
        shop: " iQOO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满2000-300"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2302,
        list: [
            { id: 230201, img: "./img/230201.jpg", price: 3999 },
            { id: 230202, img: "./img/230202.jpg", price: 3999 },
            { id: 230203, img: "./img/230203.jpg", price: 3999 },
            { id: 230204, img: "./img/230204.jpg", price: 3999 },
        ],
        info: "vivo X90 8GB+256GB 4nm天玑9200旗舰芯片 自研芯片V2 120W双芯闪充 蔡司影像 5G 拍照 ",
        arguments: ["天玑9200", "256GB", "E5柔性直屏", "告白"],
        judge: "5万+",
        shop: " iQOO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满2000-300"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2303,
        list: [
            { id: 230301, img: "./img/230301.jpg", price: 3899 },
            { id: 230302, img: "./img/230302.jpg", price: 3299 },
            { id: 230303, img: "./img/230303.jpg", price: 3299 },
            { id: 230304, img: "./img/230304.jpg", price: 3899 },
        ],
        info: "vivo iQOO 10 12GB+256GB传奇版 第一代骁龙8+ 自研芯片V1+ E5超视网膜屏 KPL官方比赛专用 5G电竞 ",
        arguments: ["骁龙8+", "256GB", "E5超视网膜屏", "传奇版"],
        judge: "10万+",
        shop: " iQOO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满3000-600"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2304,
        list: [
            { id: 230401, img: "./img/230401.jpg", price: 6999 },
            { id: 230402, img: "./img/230402.jpg", price: 6999 },
        ],
        info: "vivo X90 Pro+ 12GB+512GB 原黑 蔡司一英寸T*主摄 自研芯片V2 第二代骁龙8移动平台 5G 拍照 ",
        arguments: ["二代骁龙8+", "512GB", "5G", "原黑"],
        judge: "2万+",
        shop: " vivo京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2305,
        list: [
            { id: 230501, img: "./img/230501.jpg", price: 12999 },
            { id: 230502, img: "./img/230502.jpg", price: 12999 },
        ],
        info: "HUAWEI Mate 50 RS 保时捷设计 北斗卫星消息 超光变XMAGE影像 超微距长焦摄像头 512GB墨蓝瓷华为鸿蒙 ",
        arguments: ["保时捷设计", "512GB", "OLED曲面屏", "墨蓝瓷"],
        judge: "5000+",
        shop: " 华为京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2306,
        list: [
            { id: 230601, img: "./img/230601.jpg", price: 3899 },
            { id: 230602, img: "./img/230602.jpg", price: 4599 },
            { id: 230603, img: "./img/230603.jpg", price: 3899 },
            { id: 230604, img: "./img/230604.jpg", price: 3899 },
        ],
        info: "Redmi K60 Pro 第二代骁龙8处理器 2K高光屏 IMX800相机 120W秒充 12GB+256GB 墨羽 小米红米5G",
        arguments: ["二代骁龙8", "256GB", "OLED直屏", "晴雪"],
        judge: "2万+",
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2307,
        list: [
            { id: 230701, img: "./img/230701.jpg", price: 1459 },
            { id: 230702, img: "./img/230702.jpg", price: 1399 },
            { id: 230703, img: "./img/230703.jpg", price: 1399 },
            { id: 230704, img: "./img/230704.jpg", price: 1999 },
        ],
        info: "Redmi Note11T Pro 5G 天玑8100 144HzLCD旗舰直屏 67W快充 6GB+128GB子夜黑 5G智能手机 小米红米",
        arguments: ["天玑8100", "128GB", "OLED直屏", "子夜黑"],
        judge: "50万+",
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2308,
        list: [
            { id: 230801, img: "./img/230801.jpg", price: 1099 },
            { id: 230802, img: "./img/230802.jpg", price: 1349 },
            { id: 230803, img: "./img/230803.jpg", price: 1349 },
        ],
        info: "华为畅享 50z 5000万高清AI三摄 5000mAh超能续航 128GB 幻夜黑 大内存鸿蒙智能",
        arguments: ["5000mAh", "128GB", "LCD", "幻夜黑"],
        judge: "5万+",
        shop: " 华为京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2309,
        list: [
            { id: 230901, img: "./img/230901.jpg", price: 899 },
            { id: 230902, img: "./img/230902.jpg", price: 899 },
        ],
        info: "OPPO A36 6GB+128GB 晴川蓝 高通骁龙680 5000mAh长续航 90Hz炫彩屏 大内存游戏拍照",
        arguments: ["高通骁龙680", "128GB", "LCD", "晴川蓝"],
        judge: "10万+",
        shop: " OPPO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满500-50"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    {
        id: 2310,
        list: [{ id: 231001, img: "./img/231001.jpg", price: 6899 }],
        info: "Apple iPhone 14 (A2884) 256GB 午夜色 支持移动联通电信5G 双卡双待",
        arguments: ["A15", "256GB", "OLED直屏", "午夜色"],
        judge: "500+",
        shop: "",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        },
    },
    
    {
        id: 2402,
        list: [{ id: 240201, img: "./img/240201.jpg", price: 2040 }],
        info: "vivo iQOO 7 骁龙888 支持120W快闪充 KPL官方赛事电竞 99新 传奇版 12GB+256GB",
        arguments: ["iQOO", "256GB", "快闪充", "黑镜"],
        judge: 1222,
        shop: " 和合优品二手商品专营店",
        icons: null,
        double11: false,
        schedule: null,
    },
    {
        id: 2403,
        list: [
            { id: 240301, img: "./img/240301.jpg", price: 2699 },
            { id: 240302, img: "./img/240302.jpg", price: 2799 },
            { id: 240303, img: "./img/240303.jpg", price: 4299 },
        ],
        info: "OPPO Find X5 12GB+256GB 雅白 骁龙888 自研影像芯片 哈苏影像 5000万双主摄 120Hz高刷屏 80W闪充 5G",
        arguments: ["X5", "256GB", "高刷屏", "雅白"],
        judge: 222,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    
    {
        id: 2405,
        list: [{ id: 240501, img: "./img/240501.jpg", price: 4037 }],
        info: "HUAWEI P50 原色双影像单元 基于鸿蒙操作系统 万象双环设计 8GB+256GB曜金黑 华为",
        arguments: ["p50", "256GB", "OLED直屏", "曜金黑"],
        judge: 1233,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: {
            info: "北京预售",
        },
    },
    {
        id: 2406,
        list: [
            { id: 240601, img: "./img/240601.jpg", price: 1349 },
            { id: 240602, img: "./img/240602.jpg", price: 1549 },
        ],
        info: "华为/HUAWEI nova 9 SE 一亿像素超清摄影 创新Vlog体验 支持66W快充 8GB+128GB幻夜黑 华为",
        arguments: ["nova9", "128GB", "OLED直屏", "幻夜黑"],
        judge: 1123,
        shop: " 智能手机喜景专区",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 2407,
        list: [{ id: 240701, img: "./img/240701.jpg", price: 4599 }],
        info: "ROG游戏手机6 12GB+256GB 暗影黑 骁龙8+Gen1 矩阵式液冷散热6.0 165Hz三星电竞屏 2x3Plus肩键 5G ROG6",
        arguments: ["ROG6", "256GB", "三星电竞屏", "暗影黑"],
        judge: 2333,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon4: ["赠", "券"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 2408,
        list: [{ id: 240801, img: "./img/240801.jpg", price: 2499 }],
        info: "vivo S16e 12GB+256GB 风信紫 5000万柔光人像 原彩柔光环 OIS超稳光学防抖 5nm旗舰芯片 5G 拍照 ",
        arguments: ["S16e", "256GB", "OLED直屏", "风信紫"],
        judge: 10321,
        shop: " vivo京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 2409,
        list: [
            { id: 240901, img: "./img/240901.jpg", price: 2699 },
            { id: 240902, img: "./img/240902.jpg", price: 2799 },
            { id: 240903, img: "./img/240903.jpg", price: 2899 },
        ],
        info: "realme真我10 Pro+ 2160Hz旗舰曲面屏* 天玑1080旗舰芯 一亿像素街拍相机 8GB+128GB 星曜之光 5G",
        arguments: ["A16", "128GB", "曲面屏", "星曜之光"],
        judge: 220,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: {
            info: "北京预售",
        },
    },
    {
        id: 2410,
        list: [{ id: 241001, img: "./img/241001.jpg", price: 5799 }],
        info: "小米13 Pro 徕卡光学镜头 第二代骁龙8处理器 2K曲面屏 120Hz高刷 120W秒充 12+256GB 陶黑色 5G",
        arguments: ["小米13", "256GB", "2K曲面屏", "陶黑色"],
        judge: 52000,
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    // 5
    {
        id: 2501,
        list: [{ id: 250101, img: "./img/250101.jpg", price: 1009 }],
        list: [{ id: 250102, img: "./img/250102.jpg", price: 1089 }],
        list: [{ id: 250103, img: "./img/250103.jpg", price: 1019 }],
        info: "Redmi Note 11 5G 天玑810 33W Pro快充 5000mAh大电池  6GB +128GB 浅梦星河 智能手机 小米 红米",
        arguments: [],
        judge: "100万+",
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2502,
        list: [{ id: 250201, img: "./img/250201.jpg", price: 2599 }],
        info: "小米 Redmi 红米K60E 新品5G手机 天玑8200处理器 2K旗舰直屏 OIS光学防抖相机 晴雪 12GB+256GB",
        arguments: [],
        judge: "93",
        shop: "小米优选专卖店",
        icons: {
            // icon1: ['自营'],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2503,
        list: [{ id: 250301, img: "./img/250301.jpg", price: 1499 }],
        list: [{ id: 250302, img: "./img/250302.jpg", price: 1495 }],
        list: [{ id: 250303, img: "./img/250303.jpg", price: 1329 }],
        info: "华为/HUAWEI nova 9 SE 一亿像素超清摄影 创新Vlog体验 支持66W快充 8GB+256GB冰晶蓝 华为手机",
        arguments: [],
        judge: "1万+",
        shop: "京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2504,
        list: [{ id: 250401, img: "./img/250401.jpg", price: 1799 }],
        list: [{ id: 250402, img: "./img/250402.jpg", price: 1799 }],
        list: [{ id: 250403, img: "./img/250403.jpg", price: 1799 }],
        info: "Redmi Note12Pro极速版 5G 骁龙高能芯一亿像素 旗舰影像 OLED柔性直屏 8GB+256GB微光绿 智能手机 小米红米",
        arguments: [],
        judge: "2万+",
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2505,
        list: [{ id: 250501, img: "./img/250501.jpg", price: 6999 }],
        list: [{ id: 250502, img: "./img/250502.jpg", price: 6499 }],
        info: "vivo X90 Pro+ 12GB+512GB 原黑 蔡司一英寸T*主摄 自研芯片V2 第二代骁龙8移动平台 5G 拍照",
        arguments: [],
        judge: "2万+",
        shop: "vivo京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2506,
        list: [{ id: 250601, img: "./img/250601.jpg", price: 6399 }],
        info: "OPPO Find N2 Flip 12GB+256GB 流金 任意窗 5000万超清自拍 120Hz镜面屏 4300mAh大电量 5G 小折叠屏手机",
        arguments: [],
        judge: "1万+",
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2507,
        list: [{ id: 250701, img: "./img/250701.jpg", price: 1099 }],
        list: [{ id: 250702, img: "./img/250702.jpg", price: 1099 }],
        info: "OPPO A55s 8GB+128GB 律动黑 双模5G 超大存储 5000mAh超大电池 长续航 后置AI三摄 超清画质 拍照手机",
        arguments: [],
        judge: "20万+",
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["券780-50"],
            icon4: ["满500-50"],
        },
        double11: "false",
        schedule: null,
    },
    {
        id: 2508,
        list: [{ id: 250801, img: "./img/250801.jpg", price: 4399 }],
        list: [{ id: 250802, img: "./img/250802.jpg", price: 4399 }],
        info: "OPPO 一加 11 16GB+256GB 一瞬青 第二代骁龙 8 哈苏影像拍照 2K + 120Hz 高刷屏 游戏电竞5G旗舰",
        arguments: [],
        judge: "5万+",
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["券780-100"],
            icon5: ["赠"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 2509,
        list: [{ id: 250901, img: "./img/250901.jpg", price: 4399 }],
        info: "vivo iQOO 11 12GB+256GB 传奇版 第二代骁龙8 2K 144Hz E6全感屏 120W闪充 自研芯片V2 5G电竞",
        arguments: [],
        judge: "5万+",
        shop: "vivo京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 2510,
        list: [{ id: 251001, img: "./img/251001.jpg", price: 1699 }],
        list: [{ id: 251002, img: "./img/251002.jpg", price: 1499 }],
        list: [{ id: 251003, img: "./img/251003.jpg", price: 1699 }],
        info: "OPPO A58 8GB+256GB 静海蓝 轻薄机身 33W超级闪充 5000mAh大电池 90Hz高刷炫彩屏 双模5G芯片 长续航 5G",
        arguments: [],
        judge: "2000+",
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["券780-50"],
        },
        double11: "false",
        schedule: null,
    },
    //----------------------------------三---------------------------------------------
    {
        "id": 3101,
        "list": [
            {
                "id": "310101",
                "img": "/img/310101.jpg",
                "price": 1399
            },
            {
                "id": "310102",
                "img": "/img/310102.jpg",
                "price": 1499
            }
        ],
        "info": "OPPO K10x 极光 8GB+128GB 67W超级闪充 5000mAh长续航 120Hz高帧屏 6400万三摄 高通骁龙695 拍照 5G手机",
        "arguments": [
            "【120Hz高帧屏丨67W超级闪充丨5000mAh长续航】【K9x爆款优惠】"
        ],
        "judge": "10万+",
        "shop": "OPPO京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "券1280-100"
            ],
            "icon4": [
                "满800-100"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3102,
        "list": [
            {
                "id": "310201",
                "img": "/img/310201.jpg",
                "price": 1099
            },
            {
                "id": "310202",
                "img": "/img/310202.jpg",
                "price": 1199
            },
            {
                "id": "310203",
                "img": "/img/310203.jpg",
                "price": 1299
            }
        ],
        "info": "华为畅享 50z 5000万高清AI三摄 5000mAh超能续航 128GB 宝石蓝 大内存鸿蒙智能手机",
        "arguments": [
            "【华为畅享50z新品上市】5000万高清AI三摄",
            "5000mAh超能续航",
            "6.75英寸影音大屏",
            "大内存鸿蒙智能手机！更多手机热销爆款，限量抢购，猛戳》》》"
        ],
        "judge": "5万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3103,
        "list": [
            {
                "id": "310301",
                "img": "/img/310301.jpg",
                "price": 1699
            },
            {
                "id": "310302",
                "img": "/img/310302.jpg",
                "price": 1799
            },
            {
                "id": "310303",
                "img": "/img/310303.jpg",
                "price": 1899
            },
            {
                "id": "310304",
                "img": "/img/310304.jpg",
                "price": 1999
            }
        ],
        "info": "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G手机 8GB+128GB 彩云追月",
        "arguments": [
            "【限时优惠100元，到手价1599元，限时6期免息】120HzOLED硬核曲屏",
            "5100mAh快充大电池",
            "7.9mm轻薄机身",
            "奢华设计！X30极致性价比"
        ],
        "judge": "20万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "满1670-100"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3104,
        "list": [
            {
                "id": "310401",
                "img": "/img/310401.jpg",
                "price": 2699
            },
            {
                "id": "310402",
                "img": "/img/310402.jpg",
                "price": 2799
            },
            {
                "id": "310403",
                "img": "/img/310403.jpg",
                "price": 2899
            },
            {
                "id": "310404",
                "img": "/img/310404.jpg",
                "price": 2999
            }
        ],
        "info": "Redmi K60 骁龙8+处理器 2K高光屏 6400万超清相机 5500mAh长续航 12GB+256GB 墨羽 小米红米5G",
        "arguments": [],
        "judge": "10万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "新品"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3105,
        "list": [
            {
                "id": "310501",
                "img": "/img/310501.jpg",
                "price": 4799
            },
            {
                "id": "310502",
                "img": "/img/310502.jpg",
                "price": 4899
            },
            {
                "id": "310503",
                "img": "/img/310503.jpg",
                "price": 4999
            },
            {
                "id": "310504",
                "img": "/img/310504.jpg",
                "price": 5099
            },
            {
                "id": "310505",
                "img": "/img/310505.jpg",
                "price": 5199
            },
            {
                "id": "310506",
                "img": "/img/310506.jpg",
                "price": 5299
            }
        ],
        "info": "Apple iPhone 13 (A2634) 128GB 星光色 支持移动联通电信5G 双卡双待手机",
        "arguments": [],
        "judge": "500万+",
        "shop": "Apple产品京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "券4000-150"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3106,
        "list": [
            {
                "id": "310601",
                "img": "/img/310601.jpg",
                "price": 1268
            },
            {
                "id": "310602",
                "img": "/img/310602.jpg",
                "price": 1368
            }
        ],
        "info": "华为智选 Hi nova 9z 5G全网通手机 6.67英寸120Hz原彩屏hinova 6400万像素超清摄影 66W快充8GB+128GB亮黑色",
        "arguments": [],
        "judge": "50万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "券780-100"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3107,
        "list": [
            {
                "id": "310701",
                "img": "/img/310701.jpg",
                "price": 1399
            },
            {
                "id": "310702",
                "img": "/img/310702.jpg",
                "price": 1499
            },
            {
                "id": "310703",
                "img": "/img/310703.jpg",
                "price": 1599
            }
        ],
        "info": "荣耀Play6T Pro 天玑810 40W超级快充 6nm疾速芯 4800万超清双摄 全网通 5G手机 8GB+256GB 钛空银",
        "arguments": [
            "【限时优惠250元，到手价1349元，限时绑赠AM115耳机，赠完即止】7.45mm超薄设计，40w超级快充，超高性价比！查看Play6T版~"
        ],
        "judge": "10万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "满1370-50"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3108,
        "list": [
            {
                "id": "310801",
                "img": "/img/310801.jpg",
                "price": 1919
            },
            {
                "id": "310802",
                "img": "/img/310802.jpg",
                "price": 2019
            },
            {
                "id": "310803",
                "img": "/img/310803.jpg",
                "price": 2119
            }
        ],
        "info": "HUAWEI nova 10 SE 一亿像素质感人像 4500mAh长续航 轻薄机身128GB 10号色 华为手机",
        "arguments": [
            "【华为nova10se新品上市】一亿像素质感人像",
            "4500mAh长续航",
            "66W疾速快充！更多手机热销爆款，限量抢购，猛戳》》》"
        ],
        "judge": "5万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "秒杀"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3109,
        "list": [
            {
                "id": "310901",
                "img": "/img/310901.jpg",
                "price": 1799
            },
            {
                "id": "310902",
                "img": "/img/310902.jpg",
                "price": 1899
            },
            {
                "id": "310903",
                "img": "/img/310903.jpg",
                "price": 1999
            }
        ],
        "info": "vivo iQOO Z6 8GB+256GB 墨玉 80W闪充 6400万像素光学防抖 骁龙778G Plus 5G智能手机iqooz6",
        "arguments": [
            "【学生购教育优惠版赠XE160耳机&半年延保】iQOONeo7SE至高降300元！"
        ],
        "judge": "5万+",
        "shop": "iQOO京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3110,
        "list": [
            {
                "id": "311001",
                "img": "/img/311001.jpg",
                "price": 1699
            },
            {
                "id": "311002",
                "img": "/img/311002.jpg",
                "price": 1799
            },
            {
                "id": "311003",
                "img": "/img/311003.jpg",
                "price": 1899
            }
        ],
        "info": "realme真我Q5 Pro 80W超速闪充 骁龙870处理器 120Hz AMOLED E4 旗舰屏 8GB+256GB 雪地漂移",
        "arguments": [
            "【16+512GB新版本7日20:00开抢#Neo5潮玩电竞旗舰#到手价3199元】150W光速秒充！白条6期免息！老客/学生专享散热背夹！速戳(此商品不参加上述活动)"
        ],
        "judge": "5万+",
        "shop": "真我realme京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3201,
        "list": [
            {
                "id": "320101",
                "img": "/img/320101.jpg",
                "price": 1099
            },
            {
                "id": "320102",
                "img": "/img/320102.jpg",
                "price": 1199
            }
        ],
        "info": "vivo iQOO U5x 8GB+128GB星光黑 骁龙680 5000mAh大电池 6.51英寸大屏幕 智慧三摄 全网通智能手机",
        "arguments": [
            "【学生购教育优惠版赠XE160耳机&半年延保】iQOOZ6XPLUS会员赠耳机！"
        ],
        "judge": "10万+",
        "shop": "iQOO京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "满1000-80"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3202,
        "list": [
            {
                "id": "320201",
                "img": "/img/320201.jpg",
                "price": 1799
            },
            {
                "id": "320202",
                "img": "/img/320202.jpg",
                "price": 1899
            },
            {
                "id": "320203",
                "img": "/img/320203.jpg",
                "price": 1999
            },
            {
                "id": "320204",
                "img": "/img/320204.jpg",
                "price": 2099
            }
        ],
        "info": "Redmi Note12Pro 5G IMX766 旗舰影像 OIS光学防抖 OLED柔性直屏 8GB+256GB浅梦星河 智能手机 小米红米",
        "arguments": [
            "Redmi Note12Pro 5G IMX766 旗舰影像 OIS光学防抖 OLED柔性直屏 8GB+256GB浅梦星河 智能手机 小米红米"
        ],
        "judge": "10万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3203,
        "list": [
            {
                "id": "320301",
                "img": "/img/320301.jpg",
                "price": 920
            },
            {
                "id": "320302",
                "img": "/img/320302.jpg",
                "price": 1020
            },
            {
                "id": "320303",
                "img": "/img/320303.jpg",
                "price": 1120
            },
            {
                "id": "320304",
                "img": "/img/320304.jpg",
                "price": 1220
            }
        ],
        "info": "荣耀畅玩20 5000mAh超大电池续航 6.5英寸大屏 莱茵护眼 8GB+128GB 钛空银 双卡双待 全网通",
        "arguments": [
            "【限时优惠200元，到手价1099元】大内存，长续航~X30极致性价比"
        ],
        "judge": "100万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3204,
        "list": [
            {
                "id": "320401",
                "img": "/img/320401.jpg",
                "price": 799
            },
            {
                "id": "320402",
                "img": "/img/320402.jpg",
                "price": 899
            },
            {
                "id": "320403",
                "img": "/img/320403.jpg",
                "price": 999
            },
            {
                "id": "320404",
                "img": "/img/320404.jpg",
                "price": 1099
            },
            {
                "id": "320405",
                "img": "/img/320405.jpg",
                "price": 1199
            }
        ],
        "info": "爱心东东 新款关爱心X13 Pro真八核智能手机可用5G移动联通电信卡4g全网通游戏超薄百元便宜学生大屏老人机 尊贵黑【8+256GB】",
        "arguments": [
            "【12纳米八核芯】6.5英寸大屏，256G大内存，可用5G卡华为HMS服务，AG玻璃4000毫安Type-C接口，人脸解锁，应用多开"
        ],
        "judge": "1万+",
        "shop": "弗瑞尔手机通讯专营店",
        "icons": {
            "icon1": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3205,
        "list": [
            {
                "id": "320501",
                "img": "/img/320501.jpg",
                "price": 9899
            },
            {
                "id": "320502",
                "img": "/img/320502.jpg",
                "price": 9999
            },
            {
                "id": "320503",
                "img": "/img/320503.jpg",
                "price": 10099
            },
            {
                "id": "320504",
                "img": "/img/320504.jpg",
                "price": 10199
            }
        ],
        "info": "Apple iPhone 14 Pro Max (A2896) 256GB 暗紫色 支持移动联通电信5G 双卡双待手机",
        "arguments": [
            "【喜爱此刻入手】iPhone14Pro系列领券立减850元，加129元立得20W原装快充头！！！点击"
        ],
        "judge": "去看二手",
        "shop": "Apple产品京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "券4000-850"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3206,
        "list": [
            {
                "id": "320601",
                "img": "/img/320601.jpg",
                "price": 2449
            },
            {
                "id": "320602",
                "img": "/img/320602.jpg",
                "price": 2549
            },
            {
                "id": "320603",
                "img": "/img/320603.jpg",
                "price": 2649
            },
            {
                "id": "320604",
                "img": "/img/320604.jpg",
                "price": 2749
            }
        ],
        "info": "HUAWEI nova 10 【内置66W华为超级快充】 前置6000万超广角镜头 6.88mm轻薄机身 128GB 10号色 华为手机",
        "arguments": [
            "手机华为手机京东店铺官方nova10鸿蒙超广角镜头超级快充"
        ],
        "judge": "20万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3207,
        "list": [
            {
                "id": "320701",
                "img": "/img/320701.jpg",
                "price": 1299
            },
            {
                "id": "320702",
                "img": "/img/320702.jpg",
                "price": 1399
            },
            {
                "id": "320703",
                "img": "/img/320703.jpg",
                "price": 1499
            }
        ],
        "info": "Redmi Note12 5G 120Hz OLED屏幕 骁龙4移动平台 5000mAh长续航 8GB+256GB镜瓷白 智能手机 小米红米",
        "arguments": [
            "Redmi Note12 5G 120Hz OLED屏幕 骁龙4移动平台 5000mAh长续航 8GB+256GB镜瓷白 智能手机 小米红米"
        ],
        "judge": "5000+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3208,
        "list": [
            {
                "id": "320801",
                "img": "/img/320801.jpg",
                "price": 5759
            },
            {
                "id": "320802",
                "img": "/img/320802.jpg",
                "price": 5859
            },
            {
                "id": "320803",
                "img": "/img/320803.jpg",
                "price": 5959
            },
            {
                "id": "320804",
                "img": "/img/320804.jpg",
                "price": 6059
            }
        ],
        "info": "Apple iPhone 14 (A2884) 256GB 星光色 支持移动联通电信5G 双卡双待手机Apple",
        "arguments": [
            "Apple iPhone 14 (A2884) 256GB 星光色 支持移动联通电信5G 双卡双待手机Apple"
        ],
        "judge": "1万+",
        "shop": "京东线下店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3209,
        "list": [
            {
                "id": "320901",
                "img": "/img/320901.jpg",
                "price": 8999
            },
            {
                "id": "320902",
                "img": "/img/320902.jpg",
                "price": 9099
            }
        ],
        "info": "华为/HUAWEI Mate Xs 2 升级支持北斗卫星消息 超轻薄超平整超可靠 8GB+256GB雅黑折叠屏手机",
        "arguments": [
            "华为/HUAWEI Mate Xs 2 升级支持北斗卫星消息 超轻薄超平整超可靠 8GB+256GB雅黑折叠屏手机"
        ],
        "judge": "2万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3210,
        "list": [
            {
                "id": "321001",
                "img": "/img/321001.jpg",
                "price": 1449
            },
            {
                "id": "321002",
                "img": "/img/321002.jpg",
                "price": 1549
            },
            {
                "id": "321003",
                "img": "/img/321003.jpg",
                "price": 1649
            }
        ],
        "info": "HUAWEI nova 10z 【内置40W华为超级快充】6400万超清三摄 6.6英寸无界全视屏 128GB幻夜黑 华为手机鸿蒙",
        "arguments": [
            "HUAWEI nova 10z 【内置40W华为超级快充】6400万超清三摄 6.6英寸无界全视屏 128GB幻夜黑 华为手机鸿蒙"
        ],
        "judge": "5万+",
        "shop": "华为京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3301,
        "list": [
            {
                "id": "330101",
                "img": "/img/330101.jpg",
                "price": 1799
            },
            {
                "id": "330102",
                "img": "/img/330102.jpg",
                "price": 1899
            },
            {
                "id": "330103",
                "img": "/img/330103.jpg",
                "price": 1999
            }
        ],
        "info": "Redmi Note12Pro极速版 5G 骁龙高能芯一亿像素 旗舰影像 OLED柔性直屏 8GB+256GB子夜黑 智能手机 小米红米",
        "arguments": [
            "Redmi Note12Pro极速版 5G 骁龙高能芯一亿像素 旗舰影像 OLED柔性直屏 8GB+256GB子夜黑 智能手机 小米红米"
        ],
        "judge": "2万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3302,
        "list": [
            {
                "id": "330201",
                "img": "/img/330201.jpg",
                "price": 899
            },
            {
                "id": "330202",
                "img": "/img/330202.jpg",
                "price": 999
            }
        ],
        "info": "华为智选 U-Magic/优畅享30e 5G手机华为智选 22.5W安全快充 5000mAh大容量电池 4GB+128GB月光银 全网通手机",
        "arguments": [
            "华为智选 U-Magic/优畅享30e 5G手机华为智选 22.5W安全快充 5000mAh大容量电池 4GB+128GB月光银 全网通手机"
        ],
        "judge": "5000+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3303,
        "list": [
            {
                "id": "330301",
                "img": "/img/330301.jpg",
                "price": 1299
            },
            {
                "id": "330302",
                "img": "/img/330302.jpg",
                "price": 1399
            },
            {
                "id": "330303",
                "img": "/img/330303.jpg",
                "price": 1499
            }
        ],
        "info": "realme真我Q5 6nm骁龙5G芯 60W智慧闪充 5000mAh超大电池 8GB+256GB 冰河斩浪 5G智能手机",
        "arguments": [
            "【16+512GB新版本7日20:00开抢#Neo5潮玩电竞旗舰#到手价3199元】150W光速秒充！白条6期免息！老客/学生专享散热背夹！速戳(此商品不参加上述活动)"
        ],
        "judge": "5万+",
        "shop": "真我realme京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "券59-10"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3304,
        "list": [
            {
                "id": "330401",
                "img": "/img/330401.jpg",
                "price": 1249
            },
            {
                "id": "330402",
                "img": "/img/330402.jpg",
                "price": 1349
            },
            {
                "id": "330403",
                "img": "/img/330403.jpg",
                "price": 1449
            }
        ],
        "info": "Redmi Note12 5G 120Hz OLED屏幕 骁龙4移动平台 5000mAh长续航 6GB+128GB时光蓝 智能手机 小米红米",
        "arguments": [
            "Redmi Note12 5G 120Hz OLED屏幕  骁龙4移动平台 5000mAh长续航 6GB+128GB时光蓝 智能手机 小米红米"
        ],
        "judge": "10万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3305,
        "list": [
            {
                "id": "330501",
                "img": "/img/330501.jpg",
                "price": 7999
            },
            {
                "id": "330502",
                "img": "/img/330502.jpg",
                "price": 8099
            },
            {
                "id": "330503",
                "img": "/img/330503.jpg",
                "price": 8199
            },
            {
                "id": "330504",
                "img": "/img/330504.jpg",
                "price": 8299
            }
        ],
        "info": "Apple iPhone 14 Pro (A2892) 256GB 暗紫色 支持移动联通电信5G 双卡双待手机",
        "arguments": [
            "【喜爱此刻入手】iPhone14Pro系列领券立减850元，加129元立得20W原装快充头！！！点击"
        ],
        "judge": "去看二手",
        "shop": "Apple产品京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "券4000-850"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3306,
        "list": [
            {
                "id": "330601",
                "img": "/img/330601.jpg",
                "price": 1069
            },
            {
                "id": "330602",
                "img": "/img/330602.jpg",
                "price": 1169
            }
        ],
        "info": "realme真我10s 天玑810 5G疾速芯 5000mAh大电池+33W智慧闪充 1080P超清护眼屏 8GB+128GB 石晶黑 5G手机",
        "arguments": [
            "【16+512GB新版本7日20:00开抢#Neo5潮玩电竞旗舰#到手价3199元】150W光速秒充！白条6期免息！老客/学生专享散热背夹！速戳(此商品不参加上述活动)"
        ],
        "judge": "1万+",
        "shop": "真我realme京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3307,
        "list": [
            {
                "id": "330701",
                "img": "/img/330701.jpg",
                "price": 1859
            },
            {
                "id": "330702",
                "img": "/img/330702.jpg",
                "price": 1959
            },
            {
                "id": "330703",
                "img": "/img/330703.jpg",
                "price": 2059
            }
        ],
        "info": "Redmi K40S 骁龙870 三星E4 AMOLED 120Hz直屏 OIS光学防抖 67W快充 幻镜 12GB+256GB 5G智能手机 小米红米",
        "arguments": [
            "Redmi K40S 骁龙870 三星E4 AMOLED 120Hz直屏 OIS光学防抖 67W快充 幻镜 12GB+256GB 5G智能手机 小米红米"
        ],
        "judge": "1万+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "秒杀"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3308,
        "list": [
            {
                "id": "330801",
                "img": "/img/330801.jpg",
                "price": 1599
            },
            {
                "id": "330802",
                "img": "/img/330802.jpg",
                "price": 1699
            },
            {
                "id": "330803",
                "img": "/img/330803.jpg",
                "price": 1799
            }
        ],
        "info": "realme真我10 Pro 120Hz超窄天际屏 一亿像素街拍相机 8GB+256GB 星曜之光 5G手机",
        "arguments": [
            "【新品越级登场】120Hz超窄天际屏，一亿像素街拍相机！"
        ],
        "judge": "2000+",
        "shop": "真我realme京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "新品"
            ],
            "icon4": [
                "券59-10"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3309,
        "list": [
            {
                "id": "330901",
                "img": "/img/330901.jpg",
                "price": 2799
            },
            {
                "id": "330902",
                "img": "/img/330902.jpg",
                "price": 2899
            },
            {
                "id": "330903",
                "img": "/img/330903.jpg",
                "price": 2999
            },
            {
                "id": "330904",
                "img": "/img/330904.jpg",
                "price": 3099
            }
        ],
        "info": "三星 SAMSUNG Galaxy A53 5G手机 120Hz超顺滑全视屏 IP67级防尘防水 6400万超清四摄 8GB+128GB 糯糯白",
        "arguments": [
            "【三星焕新好时节】6400万超清四摄；120Hz超顺滑全视屏；IP67级防尘防水。详情点击"
        ],
        "judge": "1万+",
        "shop": "三星京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3310,
        "list": [
            {
                "id": "331001",
                "img": "/img/331001.jpg",
                "price": 7499
            },
            {
                "id": "331002",
                "img": "/img/331002.jpg",
                "price": 7599
            },
            {
                "id": "331003",
                "img": "/img/331003.jpg",
                "price": 7699
            },
            {
                "id": "331004",
                "img": "/img/331004.jpg",
                "price": 7799
            }
        ],
        "info": "三星 SAMSUNG Galaxy Z Flip4 掌心折叠设计 立式自由拍摄系统 8GB+256GB 5G折叠屏手机 幽紫秘境",
        "arguments": [
            "【三星焕新好时节】下单赠Buds2Pro耳机！掌心折叠设计；立式自由拍摄系统。详情点击"
        ],
        "judge": "2万+",
        "shop": "三星京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3401,
        "list": [
            {
                "id": "340101",
                "img": "/img/340101.jpg",
                "price": 399
            },
            {
                "id": "340102",
                "img": "/img/340102.jpg",
                "price": 499
            },
            {
                "id": "340103",
                "img": "/img/340103.jpg",
                "price": 599
            }
        ],
        "info": "小辣椒NB18 智能手机自营 全网通大屏超薄学生老人机 可用5G移动联通电信卡 32GB 江南白",
        "arguments": [
            "智能简易双系统，语音王播报，钢化玻璃机身，人脸快速解锁，可用5G移动联通电信卡"
        ],
        "judge": "2000+",
        "shop": "手机京喜官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3402,
        "list": [
            {
                "id": "340201",
                "img": "/img/340201.jpg",
                "price": 5699
            },
            {
                "id": "340202",
                "img": "/img/340202.jpg",
                "price": 5799
            },
            {
                "id": "340203",
                "img": "/img/340203.jpg",
                "price": 5899
            },
            {
                "id": "340204",
                "img": "/img/340204.jpg",
                "price": 5999
            }
        ],
        "info": "三星 SAMSUNG Galaxy S23 超视觉夜拍 可持续性设计 超亮全视护眼屏 8GB+256GB 悠远黑 5G手机",
        "arguments": [
            "【三星焕新好时节】GalaxyS23系列震撼上市！超视觉夜拍！6大购机礼遇，惊喜享不停！更多新品信息"
        ],
        "judge": "2000+",
        "shop": "三星京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "新品"
            ],
            "icon4": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3403,
        "list": [
            {
                "id": "340301",
                "img": "/img/340301.jpg",
                "price": 315
            },
            {
                "id": "340302",
                "img": "/img/340302.jpg",
                "price": 415
            }
        ],
        "info": "拍拍 vivo Y93 水滴屏 全面屏 全网通4G游戏手机 双卡双待 黑色 4G+64G全网通 9成新",
        "arguments": [
            "2月开学季巨惠，国行正品，学生机，备用机，高性价比强烈推荐，oppoR9s低至288，小米11青春版低至1043点击直达"
        ],
        "judge": "2000+",
        "shop": "唯尔优品二手商品专营店",
        "icons": {
            "icon1": [
                "免邮"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3404,
        "list": [
            {
                "id": "340401",
                "img": "/img/340401.jpg",
                "price": 2899
            },
            {
                "id": "340402",
                "img": "/img/340402.jpg",
                "price": 2999
            }
        ],
        "info": "摩托罗拉moto S30 Pro 黄金曲面超感屏 5000万像素超大底 68W闪充 骁龙888+ 5G手机 12GB+512GB 月夜黑",
        "arguments": [
            "摩托罗拉moto S30 Pro 黄金曲面超感屏  5000万像素超大底 68W闪充 骁龙888+ 5G手机 12GB+512GB 月夜黑"
        ],
        "judge": "1万+",
        "shop": "摩托罗拉手机京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "满2898-200"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3405,
        "list": [
            {
                "id": "340501",
                "img": "/img/340501.jpg",
                "price": 2399
            },
            {
                "id": "340502",
                "img": "/img/340502.jpg",
                "price": 2499
            },
            {
                "id": "340503",
                "img": "/img/340503.jpg",
                "price": 2599
            },
            {
                "id": "340504",
                "img": "/img/340504.jpg",
                "price": 2699
            }
        ],
        "info": "三星 SAMSUNG Galaxy A53 5G手机 120Hz超顺滑全视屏 IP67级防尘防水 6400万超清四摄 8GB+128GB 糯糯白",
        "arguments": [
            "【三星焕新好时节】6400万超清四摄；120Hz超顺滑全视屏；IP67级防尘防水。详情点击"
        ],
        "judge": "1万+",
        "shop": "三星京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3406,
        "list": [
            {
                "id": "340601",
                "img": "/img/340601.jpg",
                "price": 1399
            },
            {
                "id": "340602",
                "img": "/img/340602.jpg",
                "price": 1499
            },
            {
                "id": "340603",
                "img": "/img/340603.jpg",
                "price": 1599
            },
            {
                "id": "340604",
                "img": "/img/340604.jpg",
                "price": 1699
            }
        ],
        "info": "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G手机 6GB+128GB 幻夜黑",
        "arguments": [
            "【荣耀X40】【购机赠碎屏保6个月】实力霸屏轻薄长续航，5000万超清影像",
            "高频调光护眼好屏!荣耀手机热销爆款",
            "限量抢购，猛戳》》》查看"
        ],
        "judge": "5000+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "每满1389-20"
            ],
            "icon4": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3407,
        "list": [
            {
                "id": "340701",
                "img": "/img/340701.jpg",
                "price": 2499
            },
            {
                "id": "340702",
                "img": "/img/340702.jpg",
                "price": 2599
            },
            {
                "id": "340703",
                "img": "/img/340703.jpg",
                "price": 2699
            }
        ],
        "info": "vivo S16e 12GB+256GB 风信紫 5000万柔光人像 原彩柔光环 OIS超稳光学防抖 5nm旗舰芯片 5G 拍照 手机",
        "arguments": [
            "【新品上市，白条12期免息+限量赠XE710耳机+晒单赠50元E卡+直播间限量赠44W闪充套装】S16春日悠蓝上市》"
        ],
        "judge": "1万+",
        "shop": "vivo京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "新品"
            ],
            "icon4": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3408,
        "list": [
            {
                "id": "340801",
                "img": "/img/340801.jpg",
                "price": 3999
            },
            {
                "id": "340802",
                "img": "/img/340802.jpg",
                "price": 4099
            }
        ],
        "info": "摩托罗拉moto X40 第二代骁龙8 165Hz四曲臻彩屏 IP68防水 125W闪充 护眼黑科技 5G手机 12GB+256GB 墨晶黑",
        "arguments": [
            "摩托罗拉moto X40 第二代骁龙8 165Hz四曲臻彩屏  IP68防水 125W闪充 护眼黑科技 5G手机 12GB+256GB 墨晶黑"
        ],
        "judge": "2000+",
        "shop": "摩托罗拉手机京东自营官方旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3409,
        "list": [
            {
                "id": "340901",
                "img": "/img/340901.jpg",
                "price": 799
            },
            {
                "id": "340902",
                "img": "/img/340902.jpg",
                "price": 899
            }
        ],
        "info": "百事乐自营L14pro八核灵动岛智能手机大屏长续航游戏机学生性价比旗舰百元备用机4g移动联通电信卡远峰蓝256g",
        "arguments": [
            "灵动岛年度新品/搭载华为HMS服务/联发科八核处理器/简易双系统自由切换/4500毫安长续航大电池/2100万像素/联系客服赠豪礼"
        ],
        "judge": "2000+",
        "shop": "百事乐（LEBEST）手机京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3410,
        "list": [
            {
                "id": "341001",
                "img": "/img/341001.jpg",
                "price": 163
            },
            {
                "id": "341002",
                "img": "/img/341002.jpg",
                "price": 263
            }
        ],
        "info": "拍拍 OPPO A37 二手手机 备用机 全网通安卓智能手机 玫瑰金 2G+16G全网通 9成新",
        "arguments": [
            "2月开学季巨惠，国行正品，学生机，备用机，高性价比强烈推荐，oppoR9s低至288，小米11青春版低至1043点击直达"
        ],
        "judge": "2万+",
        "shop": "唯尔优品二手商品专营店",
        "icons": {
            "icon1": [
                "免邮"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3501,
        "list": [
            {
                "id": "350101",
                "img": "/img/350101.jpg",
                "price": 1199
            },
            {
                "id": "350102",
                "img": "/img/350102.jpg",
                "price": 1299
            },
            {
                "id": "350103",
                "img": "/img/350103.jpg",
                "price": 1399
            },
            {
                "id": "350104",
                "img": "/img/350104.jpg",
                "price": 1499
            }
        ],
        "info": "OPPO K9x 5G手机天玑810游戏芯 5000mAh超长续航 6400万超清三摄游戏拍照手机 黑曜武士 8GB+128GB",
        "arguments": [
            "【FindN2系列新品上市】至高12期免息，晒单赢至高50元红包；轻巧好用值得重用，限时抢购，立即，"
        ],
        "judge": "2万+",
        "shop": "OPPO官方旗舰店",
        "icons": {
            "icon1": [
                "京东物流"
            ],
            "icon2": [
                "满399-100"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3502,
        "list": [
            {
                "id": "350201",
                "img": "/img/350201.jpg",
                "price": 1459
            },
            {
                "id": "350202",
                "img": "/img/350202.jpg",
                "price": 1559
            },
            {
                "id": "350203",
                "img": "/img/350203.jpg",
                "price": 1659
            },
            {
                "id": "350204",
                "img": "/img/350204.jpg",
                "price": 1759
            }
        ],
        "info": "Redmi Note11T Pro 5G 天玑8100 144HzLCD旗舰直屏 67W快充 6GB+128GB子夜黑 5G智能手机 小米红米",
        "arguments": [
            "Redmi Note11T Pro 5G 天玑8100 144HzLCD旗舰直屏 67W快充 6GB+128GB子夜黑 5G智能手机 小米红米"
        ],
        "judge": "50万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3503,
        "list": [
            {
                "id": "350301",
                "img": "/img/350301.jpg",
                "price": 679
            },
            {
                "id": "350302",
                "img": "/img/350302.jpg",
                "price": 779
            },
            {
                "id": "350303",
                "img": "/img/350303.jpg",
                "price": 879
            }
        ],
        "info": "小黄蜂10搭载Flyme系统八核智能手机超薄大屏游戏学生老人备用可用4G全网通移动联通电信卡长续航瓷白128GB",
        "arguments": [
            "魅族Flyme系统界面简洁无干扰，极速运行畅玩游戏，全新升级，卓越体验！智能+老人双模式，6.5英寸高清大屏，充电宝级大电池持久续航，八核处理器，人脸解锁"
        ],
        "judge": "500+",
        "shop": "小黄蜂手机京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3504,
        "list": [
            {
                "id": "350401",
                "img": "/img/350401.jpg",
                "price": 1399
            },
            {
                "id": "350402",
                "img": "/img/350402.jpg",
                "price": 1499
            },
            {
                "id": "350403",
                "img": "/img/350403.jpg",
                "price": 1599
            },
            {
                "id": "350404",
                "img": "/img/350404.jpg",
                "price": 1699
            }
        ],
        "info": "小米11青春活力版 骁龙778G处理器 AMOLED柔性直屏 清凉薄荷 8GB+128GB 5G时尚手机",
        "arguments": [
            "小米11青春活力版 骁龙778G处理器 AMOLED柔性直屏 清凉薄荷 8GB+128GB 5G时尚手机"
        ],
        "judge": "5万+",
        "shop": "小米京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "满1000-30"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3505,
        "list": [
            {
                "id": "350501",
                "img": "/img/350501.jpg",
                "price": 3253
            },
            {
                "id": "350502",
                "img": "/img/350502.jpg",
                "price": 3353
            },
            {
                "id": "350503",
                "img": "/img/350503.jpg",
                "price": 3453
            }
        ],
        "info": "nubia 努比亚Z50 12GB+256GB 黑礁 第二代骁龙8 144HZ高刷 新35mm定制光学系统5000mAh电池80W快充拍照5G手机",
        "arguments": [
            "【努比亚Z50Ultra3月7日14:00发布会】【点击商详】1元提前解锁6大权益查看"
        ],
        "judge": "1000+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "放心购"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3506,
        "list": [
            {
                "id": "350601",
                "img": "/img/350601.jpg",
                "price": 1199
            },
            {
                "id": "350602",
                "img": "/img/350602.jpg",
                "price": 1299
            },
            {
                "id": "350603",
                "img": "/img/350603.jpg",
                "price": 1399
            }
        ],
        "info": "华为智选 优畅享50 Plus 5G全网通120Hz高刷 66W超级快充 6400万AI影像 8GB+128GB 雅致黑 手机",
        "arguments": [
            "【优畅享50plus】120Hz高刷",
            "66W超级快充",
            "6400万AI影像",
            "立即抢购;晒单联系客服领取充电器套装华为手机热销爆款(此商品不参加上述活动)"
        ],
        "judge": "2000+",
        "shop": "智能手机京喜专区",
        "icons": {
            "icon1": [
                "自营"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3507,
        "list": [
            {
                "id": "350701",
                "img": "/img/350701.jpg",
                "price": 4099
            },
            {
                "id": "350702",
                "img": "/img/350702.jpg",
                "price": 4199
            },
            {
                "id": "350703",
                "img": "/img/350703.jpg",
                "price": 4299
            },
            {
                "id": "350704",
                "img": "/img/350704.jpg",
                "price": 4399
            }
        ],
        "info": "荣耀80 Pro 1.6亿像素超清主摄 骁龙8+旗舰芯片 AI Vlog视频大师 5G手机 12GB+512GB 墨玉青",
        "arguments": [
            "荣耀80 Pro 1.6亿像素超清主摄 骁龙8+旗舰芯片 AI Vlog视频大师 5G手机 12GB+512GB 墨玉青"
        ],
        "judge": "2万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ],
            "icon3": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3508,
        "list": [
            {
                "id": "350801",
                "img": "/img/350801.jpg",
                "price": 3299
            },
            {
                "id": "350802",
                "img": "/img/350802.jpg",
                "price": 3399
            },
            {
                "id": "350803",
                "img": "/img/350803.jpg",
                "price": 3499
            }
        ],
        "info": "荣耀80 GT 骁龙8+旗舰芯 超帧独显芯片 120Hz原画超帧屏 IMX800主摄 5G手机 12GB+256GB 星际黑",
        "arguments": [
            "荣耀80 GT 骁龙8+旗舰芯 超帧独显芯片 120Hz原画超帧屏 IMX800主摄 5G手机 12GB+256GB 星际黑"
        ],
        "judge": "2万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "新品"
            ],
            "icon3": [
                "满3000-300"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3509,
        "list": [
            {
                "id": "350901",
                "img": "/img/350901.jpg",
                "price": 3499
            },
            {
                "id": "350902",
                "img": "/img/350902.jpg",
                "price": 3599
            },
            {
                "id": "350903",
                "img": "/img/350903.jpg",
                "price": 3699
            },
            {
                "id": "350904",
                "img": "/img/350904.jpg",
                "price": 3799
            }
        ],
        "info": "荣耀Magic4 全新一代骁龙8 双曲屏设计 LTPO屏幕 潜望式长焦摄像头 7P广角主摄 5G 全网通版 8GB+128GB 瓷青",
        "arguments": [
            "【12期免息，以旧换新至高补贴480元】120Hz-LTPO高刷屏~~选品质，购荣耀~"
        ],
        "judge": "5万+",
        "shop": "荣耀京东自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "赠"
            ]
        },
        "double11": false,
        "schedule": null
    },
    {
        "id": 3510,
        "list": [
            {
                "id": "351001",
                "img": "/img/351001.jpg",
                "price": 2799
            },
            {
                "id": "351002",
                "img": "/img/351002.jpg",
                "price": 2899
            },
            {
                "id": "351003",
                "img": "/img/351003.jpg",
                "price": 2999
            }
        ],
        "info": "vivo iQOO Neo6 SE 12GB+512GB 星际 高通骁龙870 双电芯80W闪充 OIS光学防抖 双模5G全网通手机iqooneo6se",
        "arguments": [
            "vivo iQOO Neo6 SE 12GB+512GB 星际 高通骁龙870 双电芯80W闪充 OIS光学防抖 双模5G全网通手机iqooneo6se"
        ],
        "judge": "5000+",
        "shop": "京东手机优选自营旗舰店",
        "icons": {
            "icon1": [
                "自营"
            ],
            "icon2": [
                "满2000-300"
            ]
        },
        "double11": false,
        "schedule": null
    },
    //------------------------------------四------------------------------------
    {
        id: 4101,
        list: [{
            id: 410101,
            img: "./img/410101.jpg",
            price: 3198
        },
        {
            id: 410102,
            img: "./img/410102.jpg",
            price: 3198
        },
        {
            id: 410103,
            img: "./img/410103.jpg",
            price: 3187
        },

        ],
        info: "OPPO Reno9 Pro 16GB+256GB 微醺 7.19mm轻薄机身 双芯人像摄影系统 120Hz OLED超清屏 超速大内存 5G手机",
        judge: "200+",
        shop: " 京东手机优选自营旗舰店",
       
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["券3000-90"],
            icon4: ["赠"]
        }
    },

    {
        id: 4102,
        list: [{
            id: 410201,
            img: "./img/410201.jpg",
            price: 2799
        },
        {
            id: 410202,
            img: "./img/410202.jpg",
            price: 2799
        },

        ],
        info: "联想拯救者Y70性能手机 12GB+256GB 骁龙8+Gen1霜刃M散热7.99mm金属中框OIS大底主摄144Hz OLED护眼电竞屏 白",
        arguments: ["A16", "256GB", "OLED直屏", "暗紫色"],
        judge: "5000+",
        shop: " 联想京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],

        },

    },


    {
        id: 4103,
        list: [{
            id: 410301,
            img: "./img/410301.jpg",
            price: 18999
        },
        {
            id: 410302,
            img: "./img/410302.jpg",
            price: 18999
        },
        {
            id: 410303,
            img: "./img/410303.jpg",
            price: 18999
        },

        ],
        info: "华为 HUAWEI Mate X2 5G全网通12GB+512GB釉白色典藏版 麒麟芯片 超感知徕卡四摄华为手机折叠屏（标配无充）",
        judge: "5万+",
        shop: " 华为京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],

        },
    },


    {
        id: 4104,
        list: [{
            id: 410401,
            img: "./img/410401.jpg",
            price: 2399
        },
        {
            id: 410402,
            img: "./img/410402.jpg",
            price: 2399
        },
        {
            id: 410403,
            img: "./img/410403.jpg",
            price: 2399
        },

        ],
        info: "realme真我GT Neo3 天玑8100 80W光速秒充 勒芒 (80W) 12GB+256GB 5G手机",
        judge: "1000+",
        shop: " 中国移动手机京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["厂商配送"],
            icon3: ["满2399-300"],

        },
    },

    {
        id: 4105,
        list: [{
            id: 410501,
            img: "./img/410501.jpg",
            price: 1499
        },
        {
            id: 410502,
            img: "./img/410502.jpg",
            price: 1699
        },
        {
            id: 410503,
            img: "./img/410503.jpg",
            price: 1499
        },

        ],
        info: "荣耀x40 新品5G手机 手机荣耀 幻夜黑 6+128GB全网通",
        arguments: ["现货速发", "快"],
        judge: "5000+",
        shop: " 炜东电商旗舰店",
        icons: {
            icon1: ["京东物流"],
            icon2: ["秒杀"],
            icon3: ["券1000-50"]

        },
    },

    {
        id: 4106,
        list: [{
            id: 410601,
            img: "./img/410601.jpg",
            price: 999
        },
        {
            id: 410602,
            img: "./img/410602.jpg",
            price: 919
        },
        {
            id: 410603,
            img: "./img/410603.jpg",
            price: 999
        },

        ],
        info: "Redmi Note 11 4G FHD+ 90Hz高刷屏 5000万三摄 G88芯片 5000mAh电池 6GB+128GB 梦幻晴空 手机 小米 红米",
        judge: "200+",
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],

        },
    },

    {
        id: 4107,
        list: [{
            id: 410701,
            img: "./img/410701.jpg",
            price: 1899
        },
        {
            id: 410702,
            img: "./img/410702.jpg",
            price: 1899
        },


        ],
        info: "海信(Hisense) A9 墨水屏阅读手机 高刷新6.1英寸300PPi 电子书阅读器 电纸书 Hi-Fi 6GB+128GB全网通 黛青",
        judge: "1万+",
        shop: " 海信手机自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],

        },
    },

    {
        id: 4108,
        list: [{
            id: 410801,
            img: "./img/410801.jpg",
            price: 1088
        },
        {
            id: 410802,
            img: "./img/410802.jpg",
            price: 1018
        },
        {
            id: 410803,
            img: "./img/410803.jpg",
            price: 1068
        },
        {
            id: 410804,
            img: "./img/410804.jpg",
            price: 1058
        },


        ],
        info: "苹果8Plus手机 Apple iPhone 8Plus 苹果8P 二手手机 二手9成新 红色 64G全网通【100%电池】9成新",
        judge: "1万+",
        shop: " 京致二手手机专营店",
        icons: {
            icon1: ["8件9.9折"],


        },
    },
    {
        id: 4109,
        list: [{
            id: 410901,
            img: "./img/410901.jpg",
            price: 99
        },
        {
            id: 410902,
            img: "./img/410902.jpg",
            price: 99
        },
        {
            id: 410903,
            img: "./img/410903.jpg",
            price: 99
        },

        ],
        info: "纽曼（Newman） N220 4G全网通老人手机移动联通电信 三网双卡双待 大字大声大按键学生老年手机黑色",
        judge: "2万+",
        shop: " 纽曼京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],

        },
    },

    {
        id: 4110,
        list: [{
            id: 411001,
            img: "./img/411001.jpg",
            price: 230
        },
        {
            id: 411002,
            img: "./img/411002.jpg",
            price: 240
        },
        {
            id: 411003,
            img: "./img/411003.jpg",
            price: 230
        },

        ],
        info: "小米（MI）红米5 二手手机金属机身 快速充电 智能美颜拍照学习安卓全网通老人学生备用 金色 2+16GB 9成新",
        judge: "15",
        shop: " 一多二手手机专营店",
        icons: {
            icon1: ["放心购"],
            icon2: ["券200-1"],
            icon3: ["每满200-2"]

        },
    },
    {
        id: 4201,
        list: [{
            id: 420101,
            img: "./img/420101.jpg",
            price: 2399
        },
        {
            id: 420102,
            img: "./img/420102.jpg",
            price: 2099
        },
        {
            id: 420103,
            img: "./img/420103.jpg",
            price: 2099
        },

        ],
        info: "荣耀X40 GT 骁龙888旗舰芯 144Hz高刷电竞屏 66W超级快充 5G手机 8GB+256GB 竞速黑",
        judge: "10万+",
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["满2370-130"],

        }
    },


    {
        id: 4202,
        list: [{
            id: 420201,
            img: "./img/420201.jpg",
            price: 799
        },
        {
            id: 420202,
            img: "./img/420202.jpg",
            price: 799
        },
        {
            id: 420203,
            img: "./img/420203.jpg",
            price: 699
        },
        ],
        info: "新款星盖世X13Pro真八核超薄智能手机可用5G移动联通电信卡4g全网通百元便宜学生智能备用老人机 P50尊贵黑【8+128GB】",
        judge: "10万+",
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["京东物流"],
            icon2: ["新品"],
            icon3: ["券799-100"],
        }
    },

    {
        id: 4203,
        list: [{
            id: 420301,
            img: "./img/420301.jpg",
            price: 2699
        },
        {
            id: 420302,
            img: "./img/420302.jpg",
            price: 2699
        },
        {
            id: 420303,
            img: "./img/420303.jpg",
            price: 2499
        },
        ],
        info: "vivo S16 8GB+128GB 烟花 高通骁龙870 前置5000万追焦人像 原彩柔光环 66W闪充 5G 拍照 手机",
        judge: "2万+",
        shop: " vivo京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满2000-50"],
            icon5: ["赠"],
        }
    },

    {
        id: 4204,
        list: [{
            id: 420401,
            img: "./img/420401.jpg",
            price: 4899
        },
        {
            id: 420402,
            img: "./img/420402.jpg",
            price: 4899
        },
        {
            id: 420403,
            img: "./img/420403.jpg",
            price: 4899
        },
        {
            id: 420404,
            img: "./img/420404.jpg",
            price: 4899
        },
        {
            id: 420405,
            img: "./img/420405.jpg",
            price: 4899
        },
        {
            id: 420406,
            img: "./img/420406.jpg",
            price: 4899
        },
        ],
        info: "Apple iPhone 12 (A2404) 128GB 黑色 支持移动联通电信5G 双卡双待手机",
        arguments: ["【喜爱此刻入手】精心设计，耐看又耐用，指定iPhone产品限时至高优惠1500元！限量抢购！点击！"],
        judge: "400万+",
        shop: " Apple产品京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["满2000-200"],

        }
    },

    {
        id: 4205,
        list: [{
            id: 420501,
            img: "./img/420501.jpg",
            price: 5996
        },],
        info: "OPPO Find N 全新折叠旗舰 8GB+256GB 云端 多角度自由悬停 120Hz镜面折叠屏 黄金折叠比例 骁龙888 5G手机",
        judge: 58,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        }
    },

    {
        id: 4206,
        list: [{
            id: 420601,
            img: "./img/420601.jpg",
            price: 1099
        },
        {
            id: 420602,
            img: "./img/420602.jpg",
            price: 1099
        },
        {
            id: 420603,
            img: "./img/420603.jpg",
            price: 1399
        },
        ],
        info: "华为畅享50z 新品手机华为 鸿蒙系统 幻夜黑 256G 全网通",
        arguments: ["【华为畅享50z新品上市】5000万高清AI三摄,5000mAh超能续航,大内存鸿蒙智能手机！"],
        judge: " 500+",
        shop: " 京铠玄手机旗舰店",
        icons: {
            icon1: ["京东物流"],
            icon2: ["新品"],
            icon3: ["免邮"],
            icon4: ["赠"],
        }
    },

    {
        id: 4207,
        list: [{
            id: 420701,
            img: "./img/420701.jpg",
            price: 9999
        },
        {
            id: 420702,
            img: "./img/420702.jpg",
            price: 9999
        },
        {
            id: 420703,
            img: "./img/420702.jpg",
            price: 9999
        },
        {
            id: 420704,
            img: "./img/420704.jpg",
            price: 9999
        },
        {
            id: 420705,
            img: "./img/420705.jpg",
            price: 9999
        },
        ],
        info: "荣耀Magic5 荣耀鹰眼相机 第二代骁龙8旗舰芯片 5100mAh电池 5G手机 12GB+256GB ",
        judge: "500+",
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        }
    },


    {
        id: 4208,
        list: [{
            id: 420801,
            img: "./img/420801.jpg",
            price: 2799
        },],
        info: "OPPO 一加 Ace 2 满血版骁龙8+旗舰平台 1.5K灵犀屏 超帧超画引擎 5G游戏电竞手机 冰河蓝 12GB+256GB",
        judge: " 1000+",
        shop: " A+官方旗舰店",
        icons: {
            icon1: ["新品"],
        },
        double11: false,
        schedule: {
            info: "北京预售无货"
        }
    },

    {
        id: 4209,
        list: [{
            id: 420901,
            img: "./img/420901.jpg",
            price: 3588
        },
        {
            id: 420902,
            img: "./img/420902.jpg",
            price: 3438
        },
        {
            id: 420903,
            img: "./img/420903.jpg",
            price: 3158
        },
        {
            id: 420904,
            img: "./img/420904.jpg",
            price: 3158
        },
        ],
        info: "华为* HUAWEI P50E 全网通手机 8GB+128GB 可可茶金 ",
        arguments: ["【中国电信国企品质保障】页面价已直降，晒单再返15元红包，详情请咨询客服！电信用户优惠专区~"],
        judge: "200+",
        shop: " 中国电信京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["赠"],
        }
    },

    {
        id: 4210,
        list: [{
            id: 421001,
            img: "./img/421001.jpg",
            price: 2799
        },
        {
            id: 421002,
            img: "./img/421002.jpg",
            price: 2799
        },
        ],
        info: "联想拯救者Y70性能手机 12GB+256GB 骁龙8+Gen1霜刃M散热7.99mm金属中框OIS大底主摄144Hz OLED护眼电竞屏 白",
        judge: "5000+",
        shop: " 联想京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        }
    },

    {
        id: 4301,
        list: [{
            id: 430101,
            img: "./img/430101.jpg",
            price: 15679
        },],
        info: "三星 SAMSUNG 心系天下 三星 W23 非凡陶瓷 尊奢铰链 瑰丽边框 16GB+512GB 5G折叠",
        arguments: [""],
        judge: 5000,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["秒杀"],
            icon4: ["赠"]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4302,
        list: [{
            id: 430201,
            img: "./img/430201.jpg",
            price: 4899
        },
        {
            id: 430202,
            img: "./img/430202.jpg",
            price: 4899
        },
        {
            id: 430203,
            img: "./img/430203.jpg",
            price: 4899
        },
        {
            id: 430204,
            img: "./img/430204.jpg",
            price: 4899
        },

        ],
        info: "三星 SAMSUNG Galaxy Z Flip3 5G 折叠屏 双模5G",
        arguments: [""],
        judge: 2,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠", "券"]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {
        id: 4303,
        list: [{
            id: 430301,
            img: "./img/430301.jpg",
            price: 12999
        },
        {
            id: 430302,
            img: "./img/430302.jpg",
            price: 12999
        },
        {
            id: 430303,
            img: "./img/430303.jpg",
            price: 12999
        },
        ],
        info: "三星 SAMSUNG Galaxy Z Fold4 沉浸大屏体验 PC般强大生产力 12GB+256GB 5G折叠",
        arguments: [""],
        judge: 2000,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4304,
        list: [{
            id: 430401,
            img: "./img/430401.jpg",
            price: 3599
        },],
        info: "三星 SAMSUNG Galaxy S22 超视觉夜拍系统超清夜景 超电影影像系统 超耐用精工设计 8GB+128GB 浮光粉 5G",
        arguments: [""],
        judge: 100,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4305,
        list: [{
            id: 430501,
            img: "./img/430501.jpg",
            price: 3699
        },
        {
            id: 430502,
            img: "./img/430502.jpg",
            price: 4399
        },
        {
            id: 430503,
            img: "./img/430503.jpg",
            price: 4399
        },
        {
            id: 430504,
            img: "./img/430504.jpg",
            price: 4399
        },
        ],
        info: "三星 SAMSUNG Galaxy S21系列 5G 6400万影像旗舰120Hz S21(8G+256G)丝雾白 官方标配",
        arguments: [""],
        judge: 200,
        shop: " 京联数科旗舰店",
        icons: {
            icon1: [""],
            icon2: [""],
            icon3: [""],
            icon4: ["赠", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {
        id: 4306,
        list: [{
            id: 430601,
            img: "./img/430601.jpg",
            price: 6999
        },],
        info: "三星 SAMSUNG Galaxy S23+",
        arguments: [""],
        judge: 200,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4307,
        list: [{
            id: 430701,
            img: "./img/430701.jpg",
            price: 4099
        },
        {
            id: 430702,
            img: "./img/430702.jpg",
            price: 4099
        },
        {
            id: 430703,
            img: "./img/430703.jpg",
            price: 4099
        },
        {
            id: 430704,
            img: "./img/430704.jpg",
            price: 4099
        },
        {
            id: 430705,
            img: "./img/430705.jpg",
            price: 4099
        },
        ],
        info: "三星 SAMSUNG Galaxy S22 超视觉夜拍系统超清夜景 超电影影像系统 超耐用精工设计 8GB+128GB 雾松绿 5G",
        arguments: [""],
        judge: 5,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4308,
        list: [{
            id: 430801,
            img: "./img/430801.jpg",
            price: 5699
        },],
        info: "三星 SAMSUNG Galaxy S23 超视觉夜拍 可持续性设计 超亮全视护眼屏 8GB+256GB 悠野绿 5G",
        arguments: [""],
        judge: 2000,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {

        id: 4309,
        list: [{
            id: 430901,
            img: "./img/430901.jpg",
            price: 7499
        },
        {
            id: 430902,
            img: "./img/430902.jpg",
            price: 7499
        },
        {
            id: 430903,
            img: "./img/430903.jpg",
            price: 7499
        },
        {
            id: 430904,
            img: "./img/430904.jpg",
            price: 7499
        },
        ],
        info: "三星 SAMSUNG Galaxy Z Flip4 掌心折叠设计 立式自由拍摄系统 8GB+256GB 5G折叠屏",
        arguments: [""],
        judge: 2,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["赠", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: "北京预定",
        }
    },
    {

        id: 4310,
        list: [{
            id: 431001,
            img: "./img/431001.jpg",
            price: 8999
        },],
        info: "三星 SAMSUNG Galaxy S23 Ultra",
        arguments: [""],
        judge: 5000,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: [""],
            icon4: ["", ""]
        },
        double11: false,
        schedule: {
            img: null,
            info: null,
        }
    },
    {
        id: 4401,
        list: [{
            id: 440101,
            img: "./img/440101.jpg",
            price: 1699
        },
        {
            id: 440102,
            img: "./img/440102.jpg",
            price: 1699
        },
        {
            id: 440103,
            img: "./img/440103.jpg",
            price: 1699
        },
        {
            id: 440104,
            img: "./img/440104.jpg",
            price: 1699
        },

        ],
        info: "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G手机",
        arguments: [],
        judge: 200000,
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon4: ["满1690-100"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4402,
        list: [{
            id: 440201,
            img: "./img/440201.jpg",
            price: 1099
        },
        {
            id: 440202,
            img: "./img/440202.jpg",
            price: 1099
        },
        {
            id: 440203,
            img: "./img/440203.jpg",
            price: 1199
        },

        ],
        info: "Redmi Note 11 5G 天玑810 33W Pro快充 5000mAh大电池 6GB +128GB 神秘黑境",
        arguments: [],
        judge: 1000000,
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4403,
        list: [{
            id: 440301,
            img: "./img/440301.jpg",
            price: 2399
        },
        {
            id: 440302,
            img: "./img/440302.jpg",
            price: 2099
        },
        {
            id: 440303,
            img: "./img/440303.jpg",
            price: 2099
        },

        ],
        info: "荣耀X40 GT 骁龙888旗舰芯 144Hz高刷电竞屏 66W超级快充 5G手机 12GB+256GB",
        arguments: [],
        judge: 100000,
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon4: ["满2370-130"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4404,
        list: [{
            id: 440401,
            img: "./img/440401.jpg",
            price: 3698
        },
        {
            id: 440402,
            img: "./img/440402.jpg",
            price: 3698
        },
        {
            id: 440403,
            img: "./img/440403.jpg",
            price: 3998
        },

        ],
        info: "OPPO Reno9 Pro+ 16GB+256GB 碧海青 骁龙8+旗舰芯片 自研影像芯片 80W超级",
        arguments: [],
        judge: 1000000,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon4: ["秒杀", "券3000-90", "赠"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4405,
        list: [{
            id: 440501,
            img: "./img/440501.jpg",
            price: 5499
        },
        {
            id: 440502,
            img: "./img/440502.jpg",
            price: 5999
        },

        ],
        info: "vivo iQOO 10 Pro 12GB+256GB赛道版 200W闪充 第一代骁龙8+ 自研芯片V1+",
        arguments: [],
        judge: 20000,
        shop: " iQOO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon4: ["满3000-1000"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4406,
        list: [{
            id: 440601,
            img: "./img/440601.jpg",
            price: 3599
        },
        {
            id: 440602,
            img: "./img/440602.jpg",
            price: 3599
        },
        {
            id: 440603,
            img: "./img/440603.jpg",
            price: 3599
        },

        ],
        info: "荣耀80 Pro 直屏版 1.6亿像素超清主摄 骁龙8+旗舰芯片 AI Vlog视频大师 5G手机",
        arguments: [],
        judge: 1000,
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4407,
        list: [{
            id: 440701,
            img: "./img/440701.jpg",
            price: 5399
        },
        {
            id: 440702,
            img: "./img/440702.jpg",
            price: 5999
        },
        {
            id: 440703,
            img: "./img/440703.jpg",
            price: 4399
        },
        {
            id: 440704,
            img: "./img/440704.jpg",
            price: 4299
        },
        {
            id: 440705,
            img: "./img/440705.jpg",
            price: 5999
        },

        ],
        info: "ROG游戏手机6 12GB+128GB 幻影白 骁龙8+Gen1 矩阵式液冷散热6.0 165Hz三星电竞屏 2x3Plus肩键 5G ROG6",
        arguments: [],
        judge: 20000,
        shop: " 玩家国度ROG京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4408,
        list: [{
            id: 440801,
            img: "./img/440801.jpg",
            price: 6099
        },
        {
            id: 440802,
            img: "./img/440802.jpg",
            price: 6399
        },
        {
            id: 440803,
            img: "./img/440803.jpg",
            price: 6399
        },
        {
            id: 440804,
            img: "./img/440804.jpg",
            price: 6399
        },
        {
            id: 440805,
            img: "./img/440805.jpg",
            price: 7069
        },
        {
            id: 440806,
            img: "./img/440806.jpg",
            price: 6699
        },
        {
            id: 440807,
            img: "./img/440807.jpg",
            price: 6069
        },
        {
            id: 440808,
            img: "./img/440808.jpg",
            price: 6699
        },

        ],
        info: "HUAWEI Mate 50 Pro 曲面旗舰 超可靠昆仑玻璃 超光变XMAGE影像 北斗卫星消息 256GB 曜金黑 华为鸿蒙手机",
        arguments: [],
        judge: 500,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon4: ["赠"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4409,
        list: [{
            id: 440901,
            img: "./img/440901.jpg",
            price: 1998
        },

        ],
        info: "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G手机AGM H5Pro三防智能手机 超大音量 7000mAh电池4800万主摄防水防摔全网通4G手机8 G + 128 G ",
        arguments: [],
        judge: 500,
        shop: "AGM京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            // icon2: ["放心购"],
            // // icon3:["新品"],
            // icon4:["满1690-100"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    }, {
        id: 4410,
        list: [{
            id: 441001,
            img: "./img/441001.jpg",
            price: 4999
        },
        {
            id: 441002,
            img: "./img/441002.jpg",
            price: 4999
        },
        {
            id: 441003,
            img: "./img/441003.jpg",
            price: 4999
        },


        ],
        info: " vivo X Note 12GB+256GB 璨夜黑 7英寸2K+ E5超感宽幕 3D大面积指纹 旗舰骁龙8 Gen1 5G 大屏 手机 xnote nex",
        arguments: [],
        judge: 10000,
        shop: " vivo京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            // icon4:["满1690-100"]
        },
        double11: false,
        schedule: null
        // {
        // img: "./img/1.gif",
        // info: "北京预售"
        // }
    },
    {
        id: 4601,
        list: [{
            id: 460101,
            img: "./img/460101.jpg",
            price: 8899.00
        },
        {
            id: 460102,
            img: "./img/460102.jpg",
            price: 8899.00
        },
        {
            id: 460103,
            img: "./img/460103.jpg",
            price: 8899.00
        },
        {
            id: 460104,
            img: "./img/460104.jpg",
            price: 8899.00
        },
        ],
        info: "Apple iPhone 14 Pro (A2892)",
        arguments: ["256GB 暗紫色 支持移动联通电信5G 双卡双待手机"],
        judge: 500000,
        shop: "Apple产品京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["劵4000-850"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4602,
        list: [{
            id: 460201,
            img: "./img/460201.jpg",
            price: 9899.00
        },
        {
            id: 460202,
            img: "./img/460202.jpg",
            price: 9899.00
        },
        {
            id: 460203,
            img: "./img/460203.jpg",
            price: 9899.00
        },
        {
            id: 460204,
            img: "./img/460204.jpg",
            price: 9899.00
        },
        ],
        info: "Apple iPhone 14 Pro Max (A2896) ",
        arguments: ["256GB 暗紫色 支持移动联通电信5G 双卡双待手机"],
        judge: 500000,
        shop: "Apple产品京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["劵4000-850"],
        },
        double11: false,
        schedule: null

    },
    {
        id: 4603,
        list: [{
            id: 460301,
            img: "./img/460301.jpg",
            price: 4799.00
        },
        {
            id: 460302,
            img: "./img/460302.jpg",
            price: 4799.00
        },
        {
            id: 460303,
            img: "./img/460303.jpg",
            price: 5699.00
        },
        {
            id: 460304,
            img: "./img/460304.jpg",
            price: 4799.00
        },
        {
            id: 460305,
            img: "./img/460305.jpg",
            price: 4799.00
        },
        {
            id: 460306,
            img: "./img/460306.jpg",
            price: 4799.00
        },

        ],
        info: "Apple iPhone 13 (A2634) 128GB ",
        arguments: ["星光色 支持移动联通电信5G 双卡双待手机 "],
        judge: 5000000,
        shop: "Apple产品京东自营旗舰店 ",
        icons: {
            icon1: ["自营"],
            icon2: ["劵4000-850"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4604,
        list: [{
            id: 460401,
            img: "./img/460401.jpg",
            price: 2799.00
        },
        {
            id: 460402,
            img: "./img/460402.jpg",
            price: 2799.00
        },

        ],
        info: "OPPO 一加 Ace 2 ",
        arguments: ["12GB+256GB 浩瀚黑 满血版骁龙®8+旗舰平台 1.5K灵犀触控"],
        judge: 50000,
        shop: "一加手机京东官方自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["新品"],
            icon3: ["赠"],
        },
        double11: false,
        schedule: {
            info: "北京预定"
        }
    },
    {
        id: 4605,
        list: [{
            id: 460501,
            img: "./img/460501.jpg",
            price: 9899.00
        }],
        info: "Apple iPhone 14 Pro Max (A2896)",
        arguments: ["256GB 暗紫色 支持移动联通电信5G 双卡双待手机"],
        judge: 5000,
        shop: " ",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4606,
        list: [{
            id: 460601,
            img: "./img/460601.jpg",
            price: 1199.00
        },
        {
            id: 460602,
            img: "./img/460602.jpg",
            price: 1199.00
        },

        ],
        info: "OPPO K9x",
        arguments: ["8GB+128GB 银紫超梦 天玑810 5000mAh长续航 33W快充 90Hz电竞屏 6400万三摄 拍照5G"],
        judge: 500000,
        shop: "OPPO京东自营官方旗舰店 ",

        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["劵780-50"],
            icon4: ["满500-50"]
        },
        double11: false,
        schedule: null
    },
    {
        id: 4607,
        list: [{
            id: 460701,
            img: "./img/460701.jpg",
            price: 15999.00
        },

        ],
        info: "三星 SAMSUNG 心系天下 三星 W23 非凡陶瓷 尊奢铰链 瑰丽边框 16GB+512GB",
        arguments: ["16GB+512GB"],
        judge: 5000,
        shop: "三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["赠"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4608,
        list: [{
            id: 460801,
            img: "./img/460801.jpg",
            price: 4258.00
        },
        {
            id: 460802,
            img: "./img/460802.jpg",
            price: 4228.00
        },
        {
            id: 460803,
            img: "./img/460803.jpg",
            price: 4258.00
        },

        ],
        info: "HUAWEI P50",
        arguments: ["原色双影像单元 基于鸿蒙操作系统 万象双环设计 支持66W超级快"],
        judge: 200000,
        shop: "华为京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4609,
        list: [{
            id: 460901,
            img: "./img/460901.jpg",
            price: 2999.00
        },
        {
            id: 460902,
            img: "./img/460902.jpg",
            price: 2999.00
        },
        {
            id: 460903,
            img: "./img/460903.jpg",
            price: 2999.00
        },

        ],
        info: "realme真我GT Neo5",
        arguments: ["150W光速秒充 觉醒光环系统 144Hz 1.5K直屏 骁龙8+ 5G芯"],
        judge: 20000,
        shop: "真我realme京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["新品"],
        },
        double11: false,
        schedule: null
    },
    {
        id: 4610,
        list: [{
            id: 461001,
            img: "./img/461001.jpg",
            price: 5799.00
        },
        {
            id: 461002,
            img: "./img/461002.jpg",
            price: 5999.00
        },
        ],
        info: "努比亚 nubia 红魔8Pro+全面屏下游戏手机",
        arguments: ["16GB+512GB暗夜骑士 第二代骁龙8 基于鸿蒙操作系统 万象双环设计 支持66W超级快"],
        judge: 10000,
        shop: "努比亚京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["新品"],
        },
        double11: false,
        schedule: null
    },
    //-----------------------------------五-----------------------------------------------------------
    {
        id: 5201,
        list: [
            { id: 520101, img: "./img/520101.jpg", price: 1499 },
            { id: 520102, img: "./img/520102.jpg", price: 1499 },
            { id: 520103, img: "./img/520103.jpg", price: 1799 },
            { id: 520104, img: "./img/520104.jpg", price: 1649 },
            { id: 520105, img: "./img/520105.jpg", price: 1769 },
            { id: 520106, img: "./img/520106.jpg", price: 1799 }
        ],
        info: "华为智选 NZone s7pro 5G手机 【S7 Pro】优雅黑 8+128G",
        argument: ["8GB运存", "6.6英寸"],
        judge: 500,
        shop: "京铠玄手机旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: ["免邮", "赠"],
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5202,
        list: [
            { id: 520201, img: "./img/520201.jpg", price: 6169 },
            { id: 520202, img: "./img/520202.jpg", price: 5899 },
            { id: 520203, img: "./img/520203.jpg", price: 5199 },
            { id: 520204, img: "./img/520204.jpg", price: 5399 },
            { id: 520205, img: "./img/520205.jpg", price: 5899 },
            { id: 520206, img: "./img/520206.jpg", price: 4269 }
        ],
        info: "华为mate40e 5G手机【4G可选】支持鸿蒙HarmonyOs 釉白色 8+128G  (5G) 全网通【碎屏险无线充套装]",
        argument: ["8GB运存", "5G", "6.5英寸"],
        judge: 5000,
        shop: "疆界互联旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: ["赠"],
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5203,
        list: [
            { id: 520301, img: "./img/520301.jpg", price: 6169 },
            { id: 520302, img: "./img/520302.jpg", price: 5899 },
            { id: 520303, img: "./img/520303.jpg", price: 5199 },
            { id: 520304, img: "./img/520304.jpg", price: 5399 },
            { id: 520305, img: "./img/520305.jpg", price: 5899 },
            { id: 520306, img: "./img/520306.jpg", price: 4269 }
        ],
        info: "小米 Redmi 红米 Note11 Pro 游戏智能5G手机 6G+128G 神秘黑境 官方标配【碎屏险+晒单有礼】",
        argument: ["6GB运存", "6.67英寸"],
        judge: 2000,
        shop: "疆界互联旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: ["秒杀", "赠"],
            icon4: ["新品"],
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5204,
        list: [
            { id: 520401, img: "./img/520401.jpg", price: 1499 },
            { id: 520402, img: "./img/520402.jpg", price: 1499 },
            { id: 520403, img: "./img/520403.jpg", price: 1499 }
        ],
        info: "Redmi Note10 Pro 5G智能手机 天玑1100液冷游戏芯120Hz变速金刚红米小米 月魄 6GB+128GB",
        argument: ["6GB运存", "液冷散热", "6.6英寸"],
        judge: 10000,
        shop: "小米手机官方旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: null,
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5205,
        list: [
            { id: 520501, img: "./img/520501.jpg", price: 1199 }
        ],
        info: "vivo iQOO U3x 高通骁龙八核强芯 5000mAh大电池 90Hz竞速屏 双模5G全网通 6GB+64GB 幻蓝 iqoou3x手机",
        judge: 100000,
        shop: "IQOO京东官方自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: null,
            icon3: null,
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5206,
        list: [
            { id: 520601, img: "./img/520601.jpg", price: 1469 },
            { id: 520602, img: "./img/520602.jpg", price: 1459 },
            { id: 520603, img: "./img/520603.jpg", price: 1379 },
            { id: 520604, img: "./img/520604.jpg", price: 1599 }
        ],
        info: "荣耀X10 5G手机【麒麟820芯片】 光速银 8+128G【全网通】",
        argument: ["8GB运存"],
        judge: 10000,
        shop: "疆界互联旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: ["赠"],
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5207,
        list: [
            { id: 520701, img: "./img/520701.jpg", price: 2669 },
            { id: 520702, img: "./img/520702.jpg", price: 2699 },
            { id: 520703, img: "./img/520703.jpg", price: 2699 },
            { id: 520704, img: "./img/520704.jpg", price: 2699 }
        ],
        info: "华为nova9 新品手机 9号色 8+128G全网通",
        argument: ["8GB运存", "6.57英寸"],
        judge: 10000,
        shop: "炜东电商旗舰店",
        icons: {
            icon1: null,
            icon2: null,
            icon3: ["赠", "京东物流", "免邮", "秒杀"],
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5208,
        list: [
            { id: 520801, img: "./img/520801.jpg", price: 180 },
            { id: 520802, img: "./img/520802.jpg", price: 180 }
        ],
        info: "纽曼 Newman L6 爵士黑 4G全网通 移动联通电信老人手机 超长待机 大字大声大按键老年机 学生儿童备用功能机",
        argument: ["2.4英寸"],
        judge: 20000,
        shop: "纽曼京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["赠"],
            icon4: null,
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5209,
        list: [
            { id: 520901, img: "./img/520901.jpg", price: 399 }
        ],
        info: "天语（K-Touch）K30 智能老人手机 4G全网通 超长待机 双卡双待 移动联通电信 学生备用老年人手机 黑色",
        argument: ["人脸识别", "5.5英寸"],
        judge: 5000,
        shop: "天语手机京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["赠"],
            icon4: ["新品"],
        },
        double11: true,
        schedule: null,
    },
    {
        id: 5210,
        list: [
            { id: 521001, img: "./img/521001.jpg", price: 5999 }
        ],
        info: "Apple iPhone 13 (A2634) 128GB 午夜色",
        argument: ["6.1英寸"],
        judge: 102500,
        shop: "Apple产品京东自营旗舰",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: null,
            icon4: null,
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5501,
        list: [
            { id: 550101, img: "./img/550101.jpg", price: 5799 },
            { id: 550102, img: "./img/550102.jpg", price: 6299 },
            { id: 550103, img: "./img/550103.jpg", price: 5799 },
            { id: 550104, img: "./img/550104.jpg", price: 6299 },
        ],
        info: "小米13Pro 徕卡光学镜头 第二代骁龙8处理器",
        arguments: ["120hz高刷", "12+256GB", "2K曲面屏", "陶瓷黑"],
        judge: 50537,
        shop: " 小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["赠"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5502,
        list: [
            { id: 550201, img: "./img/550201.jpg", price: 1099 },
            { id: 550202, img: "./img/550202.jpg", price: 1099 },
            { id: 550203, img: "./img/550203.jpg", price: 1299 },
        ],
        info: "荣耀Play6C 5000mAh大电池 高通骁龙5G芯 22.5W超级快充 侧边指纹解锁",
        arguments: ["22.5W超级快充", "侧边指纹解锁", "钛空银"],
        judge: 200123,
        shop: " 荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon3: ["券满1070-100"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            info: "北京预售",
        },
    },
    {
        id: 5503,
        list: [
            { id: 550301, img: "./img/550301.jpg", price: 5759 },
            { id: 550302, img: "./img/550302.jpg", price: 5399 },
            { id: 550303, img: "./img/550303.jpg", price: 6299 },
        ],
        info: "Apple iPhone 14 支持移动联通电信5G",
        arguments: ["A2884", "256GB", "双卡双待手机", "星光色"],
        judge: 1078,
        shop: " Apple京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            // icon2: ["放心购"],
            // icon3:["新品"],
            // icon4:["赠","券"]
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5504,
        list: [
            { id: 550401, img: "./img/550401.jpg", price: 1789 },
            { id: 550402, img: "./img/550402.jpg", price: 1949 },
        ],
        info: "OPPO 一加  竞速版享OPPO官方售后MAX120Hz5G游戏手",
        arguments: ["Ace", "天玑8100", "变速电屏", "黑色"],
        judge: 1320,
        shop: "一加官方旗舰店",
        icons: {
            //icon1: ["自营"],
            icon1: ["放心购"],
            //icon3:["新品"],
            icon2: ["满1000-40"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            info: "北京预售",
        },
    },
    {
        id: 5505,
        list: [
            { id: 550501, img: "./img/550501.jpg", price: 3499 },
            { id: 550502, img: "./img/550502.jpg", price: 3499 },
            { id: 550503, img: "./img/550503.jpg", price: 3499 },
        ],
        info: "OPPO Find X5  骁龙888 自研影像芯片 哈苏影像",
        arguments: ["8GB+128GB", "5000万双主摄", "雅白"],
        judge: 21820,
        shop: " OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon3: ["赠", "券1000-500"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5506,
        list: [
            { id: 550601, img: "./img/550601.jpg", price: 4499 },
            { id: 550602, img: "./img/550602.jpg", price: 3499 },
        ],
        info: "摩托罗拉moto X30 Pro 全新骁龙8+",
        arguments: [
            "2亿像素",
            "125W闪充+50W无线充",
            "144Hz曲面边屏",
            "墨韵黑",
        ],
        judge: 10563,
        shop: "摩托罗拉手机京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            // icon2: ["放心购"],
            // icon3:["新品"],
            icon2: ["满4199-700"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5507,
        list: [
            { id: 550701, img: "./img/550701.jpg", price: 2799 },
            { id: 550702, img: "./img/550702.jpg", price: 2799 },
            { id: 550703, img: "./img/550703.jpg", price: 2299 },
        ],
        info: "vivo iQOO Neo6 SE 12GB+512GB 高通骁龙870",
        arguments: ["双电芯80W闪充", "OIS光学防抖", "双模5G全网通", "星际色"],
        judge: 5480,
        shop: " 京东手机优选自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            // icon4:["赠","券"]
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5508,
        list: [
            { id: 550801, img: "./img/550801.jpg", price: 6999 },
            { id: 550802, img: "./img/550802.jpg", price: 6999 },
            { id: 550803, img: "./img/550803.jpg", price: 7999 },
        ],
        info: "三星 SAMSUNG Galaxy S23+",
        arguments: ["超视觉夜拍", "可持续性设计", "超亮全视护眼屏", "悠雾紫"],
        judge: 2537,
        shop: "三星京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon3: ["赠"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5509,
        list: [
            { id: 550901, img: "./img/550901.jpg", price: 6499 },
            { id: 550902, img: "./img/550902.jpg", price: 5999 },
            { id: 550903, img: "./img/550903.jpg", price: 5199 },
        ],
        info: "努比亚 nubia 红魔7S Pro屏下摄像旗舰 新骁龙8+ 5G电竞游戏手机",
        arguments: [
            "18GB+512GB透明闪速版",
            "稳帧引擎",
            "66W快充",
            "透明闪速版",
        ],
        judge: 10450,
        shop: "努比亚京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon3: ["满3000-500"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5510,
        list: [
            { id: 551001, img: "./img/551001.jpg", price: 3199 },
            { id: 551002, img: "./img/551002.jpg", price: 3499 },
            { id: 551003, img: "./img/551003.jpg", price: 3499 },
        ],
        info: "GT2大师探索版 骁龙8+旗舰芯",
        arguments: ["新一代X7独显芯片", "5000mAh+100W光速秒充", "硬箱·旷野"],
        judge: 23226,
        shop: "真我realme京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            // icon3:["新品"],
            icon3: ["券59-10"],
        },
        double11: false,
        schedule: {
            // img: "./img/1.gif",
            // info: "北京预售"
        },
    },
    {
        id: 5101,
        list: [
            {
                id: 510101,
                img: "./img/510101.jpg",
                price: 1699,
                id: 510102,
                img: "./img/510102.jpg",
                price: 1699,
                id: 510103,
                img: "./img/510103.jpg",
                price: 1699,
                
            },
        ],
        info: "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G",
        arguments: ["快充大电池", "7.9mm轻薄设计", "OLED硬核曲屏", "120Hz"],
        judge: 200001,
        shop: "荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5102,
        list: [
            {
                id: 510201,
                img: "./img/510201.jpg",
                price: 1399,
                id: 510202,
                img: "./img/510202.jpg",
                price: 1399,
            },
        ],
        info: "OPPO K10x 极光 8GB+128GB 67W超级闪充 5000mAh长续航 120Hz高帧屏 6400万三摄 高通骁龙695 拍照 5G",
        arguments: [
            "高通骁龙695",
            "120Hz高帧屏",
            "5000mAh长续航",
            "67W超级闪充",
        ],
        judge: 100001,
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5103,
        list: [
            {
                id: 510301,
                img: "./img/510301.jpg",
                price: 4599,
            },
        ],
        info: "小米13 徕卡光学镜头 第二代骁龙8处理器",
        arguments: ["徕卡光学镜头", "智能手机", "小米"],
        judge: 100001,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5104,
        list: [
            {
                id: 510401,
                img: "./img/510401.jpg",
                price: 1009,
                id: 510402,
                img: "./img/510402.jpg",
                price: 1009,
                id: 510403,
                img: "./img/510403.jpg",
                price: 1009,
                id: 510404,
                img: "./img/510404.jpg",
                price: 1009,
            },
        ],
        info: "OPPO Reno9 8GB+256GB 微醺 6400万水光人像镜头 120Hz OLED超清曲面屏 4500mAh大电池 7.19mm轻薄 5G",
        arguments: ["7.19mm轻薄", "4500mAh大电池", "OLED超清曲面屏", "120Hz"],
        judge: 50001,
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5105,
        list: [
            {
                id: 510501,
                img: "./img/510501.jpg",
                price: 2499,
                id: 510502,
                img: "./img/510502.jpg",
                price: 2499,
                id: 510503,
                img: "./img/510503.jpg",
                price: 2499,
                id: 510504,
                img: "./img/510504.jpg",
                price: 2499,
            },
        ],
        info: "荣耀80 1.6亿像素超清主摄 AI Vlog视频大师 全新Magic OS 7.0系统 5G手机 8GB+256GB 墨玉青",
        arguments: ["全新Magic", "5G手机", "256GB", "墨玉青"],
        judge: 100001,
        shop: "荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5106,
        list: [
            {
                id: 510601,
                img: "./img/510601.jpg",
                price: 1009,
                id: 510601,
                img: "./img/510602.jpg",
                price: 1019,
                id: 510601,
                img: "./img/510603.jpg",
                price: 1199,
            },
        ],
        info: "Redmi Note 11 5G 天玑810 33W Pro快充 5000mAh大电池  6GB +128GB 神秘黑境 智能手机 小米 红米",
        arguments: ["神秘黑境", "智能手机", "小米", "红米"],
        judge: 1000001,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5107,
        list: [
            {
                id: 510701,
                img: "./img/510701.jpg",
                price: 9699,
            },
        ],
        info: "三星 SAMSUNG Galaxy S23 Ultra 超视觉夜拍 稳劲性能 大屏S Pen书写 12GB+256GB 悠柔白 5G",
        arguments: ["悠柔白", "稳劲性能", "超视觉夜拍", "5G"],
        judge: 5001,
        shop: "三星京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5108,
        list: [
            {
                id: 510801,
                img: "./img/510801.jpg",
                price: 3299,
                id: 510802,
                img: "./img/510802.jpg",
                price: 3299,
                id: 510803,
                img: "./img/510803.jpg",
                price: 3299,
            },
        ],
        info: "荣耀80 GT 骁龙8+旗舰芯 超帧独显芯片 120Hz原画超帧屏 IMX800主摄 5G手机 12GB+256GB 光雨流星",
        arguments: ["IMX800主摄", "光雨流星", "120Hz原画超帧屏", "5G手机"],
        judge: 200001,
        shop: "荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5109,
        list: [
            {
                id: 510901,
                img: "./img/510901.jpg",
                price: 1499,
                id: 510902,
                img: "./img/510902.jpg",
                price: 1499,
                id: 510903,
                img: "./img/510903.jpg",
                price: 1199,
            },
        ],
        info: "vivo iQOO Z6x 8GB+256GB 黑镜 6000mAh巨量电池 44W闪充 6nm强劲芯 5G智能",
        arguments: ["黑境", "6000mAh巨量电池", "44W闪充", "6nm强劲芯"],
        judge: 100001,
        shop: "IQOO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5110,
        list: [
            {
                id: 511001,
                img: "./img/511001.jpg",
                price: 1199,
                id: 511002,
                img: "./img/511002.jpg",
                price: 1199,
            },
        ],
        info: "OPPO K9x 8GB+128GB 银紫超梦 天玑810 5000mAh长续航 33W快充 90Hz电竞屏 6400万三摄 拍照5G",
        arguments: [
            "5000mAh超大电池",
            "6400万超清摄像",
            "90Hz电竞屏",
            "K10x爆款优惠",
        ],
        judge: 500001,
        shop: "OPPO京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满1666-200"],
        },
        double11: false,
        schedule: {
            img: null,
            info: "",
        },
    },
    {
        id: 5401,
        list: [
            { id: 540101, img: "./img/540101.jpg", price: 2799 },
            { id: 540102, img: "./img/540102.jpg", price: 2399 },
            { id: 540103, img: "./img/540103.jpg", price: 2399 },
            { id: 540104, img: "./img/540104.jpg", price: 2799 },
        ],
        info: "三星 SAMSUNG Galaxy A53 5G手机 120Hz超顺滑全视屏 IP67级防尘防水",
        arguments: ["A53", "256GB", "OLED直屏", "黑色"],
        judge: 10859,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["赠"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5402,
        list: [
            { id: 540201, img: "./img/540201.jpg", price: 7499 },
            { id: 540202, img: "./img/540202.jpg", price: 7499 },
            { id: 540203, img: "./img/540203.jpg", price: 7499 },
            { id: 540204, img: "./img/540204.jpg", price: 7499 },
        ],
        info: "三星 SAMSUNG 心系天下 三星 W23 非凡陶瓷 尊奢铰链 瑰丽边框 16GB+512GB",
        arguments: ["骁龙8+", "256GB", "OLED折叠屏", "粉色"],
        judge: 5842,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["赠"],
            icon3: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5403,
        list: [
            { id: 540301, img: "./img/540301.jpg", price: 5199 },
            { id: 540302, img: "./img/540302.jpg", price: 5699 },
            { id: 540303, img: "./img/540303.jpg", price: 5699 },
            { id: 540304, img: "./img/540304.jpg", price: 5699 },
        ],
        info: "三星 SAMSUNG Galaxy S23 超视觉夜拍 可持续性设计 超亮全视护眼屏",
        arguments: ["骁龙8 Gen2", "128GB", "OLED直屏", "白色"],
        judge: 8415,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["赠"],
            icon3: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5404,
        list: [
            { id: 540401, img: "./img/540401.jpg", price: 3699 },
            { id: 540402, img: "./img/540402.jpg", price: 4099 },
            { id: 540403, img: "./img/540403.jpg", price: 4099 },
            { id: 540404, img: "./img/540404.jpg", price: 4099 },
        ],
        info: "三星 SAMSUNG Galaxy S21 FE 5G 第二代动态AMOLED 120Hz 骁龙888 IP68防尘防",
        arguments: ["骁龙8882", "128GB", "OLED直屏", "白色"],
        judge: 21413,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5405,
        list: [
            { id: 540501, img: "./img/540501.jpg", price: 4599 },
            { id: 540502, img: "./img/540502.jpg", price: 4799 },
            { id: 540503, img: "./img/540503.jpg", price: 4799 },
            { id: 540504, img: "./img/540504.jpg", price: 4799 },
        ],
        info: "SAMSUNG Galaxy Z Flip3 5G 折叠屏 双模5G手机 立式交互体验 IPX8防水",
        arguments: ["骁龙888", "256GB", "OLED折叠屏", "黑色"],
        judge: 124123,
        shop: " 三星京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["1000-30"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5406,
        list: [
            { id: 540601, img: "./img/540601.jpg", price: 1399 },
            { id: 540602, img: "./img/540602.jpg", price: 1399 },
        ],
        info: "OPPO K10x 极光 8GB+128GB 67W超级闪充 5000mAh长续航 120Hz高帧屏 6400",
        arguments: ["骁龙600系列", "128GB", "直屏", "极夜黑色"],
        judge: 143124,
        shop: "OPPO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["1280-100"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5407,
        list: [
            { id: 540701, img: "./img/540701.jpg", price: 1699 },
            { id: 540702, img: "./img/540702.jpg", price: 1699 },
            { id: 540703, img: "./img/540703.jpg", price: 1699 },
        ],
        info: "荣耀X40 120Hz OLED硬核曲屏 5100mAh 快充大电池 7.9mm轻薄设计 5G手机",
        arguments: ["骁龙600系列", "128GB", "OLED曲面屏", "琥珀星光"],
        judge: 235987,
        shop: "荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["1670-100"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5408,
        list: [
            { id: 540801, img: "./img/540801.jpg", price: 2499 },
            { id: 540802, img: "./img/540802.jpg", price: 2499 },
            { id: 540803, img: "./img/540803.jpg", price: 2499 },
            { id: 540803, img: "./img/540804.jpg", price: 2499 },
        ],
        info: "荣耀80 1.6亿像素超清主摄 AI Vlog视频大师 全新Magic OS 7.0系统 5G手机",
        arguments: ["骁龙782G", "256GB", "OLED曲面屏", "亮黑色"],
        judge: 120392,
        shop: "荣耀京东自营旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon3: ["2450-100"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5409,
        list: [
            { id: 540901, img: "./img/540901.jpg", price: 1499 },
            { id: 540902, img: "./img/540902.jpg", price: 1499 },
            { id: 540903, img: "./img/540903.jpg", price: 1499 },
        ],
        info: "vivo iQOO Z6x 8GB+256GB 黑镜 6000mAh巨量电池 44W闪充 6nm强劲芯片",
        arguments: ["天玑800系列", "128GB", "直屏", "蓝冰色"],
        judge: 106587,
        shop: "iQOO京东自营官方旗舰店",
        icons: {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5410,
        list: [
            { id: 541001, img: "./img/541001.jpg", price: 1899 },
            { id: 541002, img: "./img/541002.jpg", price: 1899 },
            { id: 541003, img: "./img/541003.jpg", price: 1899 },
            { id: 541003, img: "./img/541004.jpg", price: 1899 },
        ],
        info: "Redmi K40S 骁龙870 三星E4 AMOLED 120Hz直屏 OIS光学防抖 亮黑",
        arguments: ["骁龙870", "256GB", "OLED直屏", "晴雪"],
        judge: 562158,
        shop: "小米京东自营旗舰店",
        icons: {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null,
    },
    {
        id: 5301,
        list: [{
            id: 530101,
            img: "./img/530101.jpg",
            price: 5699
        },
        ],
        info: "vivo iQOO Neo7竞速版 12GB+256GB 几何黑 骁龙8+旗舰芯片 独显芯片Pro+",
        arguments: ["骁龙8+", "128GB+256GB", "几何黑"],
        judge: 2,
        shop: " iQOO京东自营官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon1: ["放心购"]
        },
        double11: false,
        schedule: null
    }, {
        id: 5302,
        list: [{
            id: 530201,
            img: "./img/530201.jpg",
            price: 5399
        },
        {
            id: 530202,
            img: "./img/530202.jpg",
            price: 5399
        },
        {
            id: 530203,
            img: "./img/530203.jpg",
            price: 5399
        },
        { id: 530204, img: "./img/530204.jpg", price: 5399 },
        { id: 530205, img: "./img/530205.jpg", price: 5399 }
        ],
        info: "Apple iPhone 14 (A2884) 128GB 星光色 支持移动联通电信5G 双卡双待手机",
        arguments: ["A16", "128GB", "OLED直屏", "星光色"],
        judge: 200,
        shop: " Apple产品京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["券4000-300"]
        },
        double11: false,
        schedule: null
    }, {
        id: 5303,
        list: [
            { id: 530301, img: "./img/530301.jpg", price: 1899 },
            { id: 530302, img: "./img/530302.jpg", price: 1899 },
            { id: 530303, img: "./img/530303.jpg", price: 1899 },
            { id: 530304, img: "./img/530304.jpg", price: 1899 },
            { id: 530305, img: "./img/530305.jpg", price: 1899 },
        ],
        info: "Redmi K40S 骁龙870 三星E4 AMOLED 120Hz直屏 OIS光学防抖 亮黑",
        arguments: ["骁龙870", "256GB", "120Hz直屏", "亮黑"],
        judge: 50,
        shop: " 小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null
    }, {
        id: 5304,
        list: [
            { id: 530401, img: "./img/530401.jpg", price: 1799 },
            { id: 530402, img: "./img/530402.jpg", price: 1799 },
            { id: 530403, img: "./img/530403.jpg", price: 1799 },
            { id: 530404, img: "./img/530404.jpg", price: 1799 }
        ],
        info: "Redmi Note12Pro 5G IMX766 旗舰影像 OIS光学防抖 OLED柔性直屏 8GB+256GB",
        arguments: ["OIS光学防抖", "8GB+256GB", "OLED柔性直屏", "子夜黑"],
        judge: 10,
        shop: " 小米京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null
    }, {
        id: 5305,
        list: [
            { id: 530501, img: "./img/530501.jpg", price: 2499 },
            { id: 530502, img: "./img/530502.jpg", price: 2499 },
            { id: 530503, img: "./img/530503.jpg", price: 2499 },
            { id: 530504, img: "./img/530504.jpg", price: 2499 }
        ],
        info: "SAMSUNG Galaxy A53 5G手机 120Hz超顺滑全视屏 IP67级防尘防水 6400万超清四摄",
        arguments: ["5G手机", "256GB", "OLED直屏", "宇宙黑"],
        judge: 10,
        shop: " 京东手机优选自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
        },
        double11: false,
        schedule: null
    }, {
        id: 5306,
        list: [
            { id: 530601, img: "./img/530601.jpg", price: 2699 },
            { id: 530602, img: "./img/530602.jpg", price: 2699 },
            { id: 530603, img: "./img/530603.jpg", price: 2699 },
        ],
        info: "诺基亚（NOKIA）105 新 移动2G 老人老年手机 直板按键手机 学生备用功能机",
        arguments: ["NOKIA", "移动2G", "直板按键手机", "学生备用功能机"],
        judge: 50,
        shop: " 诺基亚手机京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"]
        },
        double11: false,
        schedule: null
    }, {
        id: 5307,
        list: [
            { id: 530701, img: "./img/530701.jpg", price: 2499 },
            { id: 530702, img: "./img/530702.jpg", price: 2499 },
            { id: 530703, img: "./img/530703.jpg", price: 2499 },
            { id: 530704, img: "./img/530704.jpg", price: 2499 }
        ],
        info: "荣耀80 1.6亿像素超清主摄 AI Vlog视频大师 全新Magic OS 7.0系统 5G手机",
        arguments: ["Magic OS 7.0", "256GB", "OLED直屏", "亮黑色"],
        judge: 10,
        shop: " 荣耀京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon2: ["放心购"],
            icon3: ["新品"],
            icon4: ["满2450-100"],
        },
        double11: false,
        schedule: null
    }, {
        id: 5308,
        list: [
            { id: 530801, img: "./img/530801.jpg", price: 799 },
            { id: 530802, img: "./img/530802.jpg", price: 799 },
            { id: 530803, img: "./img/530803.jpg", price: 899 }
        ],
        info: "MEIZU魅族 魅蓝note2 移动联通4G手机 老人学生备用手机 魅蓝10S大湾蓝",
        arguments: ["MEIZU", "学生备用手机", "移动联通4G手机", "幻夜黑"],
        judge: 100,
        shop: " 魅族官方旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon1: ["放心购"]
        },
        double11: false,
        schedule: null
    }, {
        id: 5309,
        list: [
            { id: 530901, img: "./img/530901.jpg", price: 2699 },
            { id: 530902, img: "./img/530902.jpg", price: 2699 },
            { id: 530903, img: "./img/530903.jpg", price: 2699 }
        ],
        info: "realme真我10 Pro+ 2160Hz旗舰曲面屏* 天玑1080旗舰芯 一亿像素街拍相机",
        arguments: ["天玑1080", "256GB", "2160Hz旗舰曲面屏", "星耀之光"],
        judge: 2,
        shop: " 真我realme京东自营旗舰店",
        icons:
        {
            icon1: ["自营"],
            icon1: ["新品"]
        },
        double11: false,
        schedule: null
    }, {
        id: 5310,
        list: [
            { id: 531001, img: "./img/531001.jpg", price: 599 },
            { id: 531002, img: "./img/531002.jpg", price: 599 },
            { id: 531003, img: "./img/531003.jpg", price: 799 }
        ],
        info: "金立 K13Pro 八核智能手机自营 128GB 全网通游戏大屏学生老人机 长续航 可用5G",
        arguments: ["K13Pro", "128GB", "老人机", "长续航"],
        judge: 1000,
        shop: " 手机惊喜官方旗舰店",
        icons:
        {
            icon1: ["自营"],
        },
        double11: false,
        schedule: null
    }];