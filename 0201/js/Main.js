import data from "./data.js";
import Goods from "./Goods.js";
import Shopping from "./Shopping.js";
export default class Main{
    shopping;
    constructor(){
        data.slice(0,10).forEach(item=>{
            var goods=new Goods();
            goods.addEventListener(Goods.GOODS_CHANGE,e=>this.goodsChangeHandler(e));
            goods.setData(item);
            goods.appendTo("body");
        });
        this.shopping=new Shopping();
        this.shopping.appendTo("body");
    }
    goodsChangeHandler(e){
        var goods=e.data.list.find(t=>t.id==e.ids);
        var item={
            id:e.ids,
            checked:false,
            icon:goods.img,
            info:e.data.info,
            arguments:e.data.arguments.join(),
            price:goods.price,
            num:1,
            total:goods.price,
            deleted:false
        }
        this.shopping.addGoods(item);
    }
}

new Main();