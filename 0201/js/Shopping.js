import Component from "./Component.js";
import StepNumber from "./StepNumber.js";
export default class Shopping extends Component{
    tbody;
    shoppingList=[];
    all;
    constructor(){
        super("table","shopping");
        this.elem.innerHTML=`
            <thead>
                <tr>
                    <th colspan='2'><input type='checkbox' id='all'>全选</th>
                    <th>商品</th>
                    <th></th>
                    <th>单价</th>
                    <th>数量</th>
                    <th>小计</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody></tbody>
        `
        this.tbody=this.elem.querySelector("tbody");
        this.all=this.elem.querySelector("#all");
        this.all.addEventListener("click",e=>this.checkedHandler(e));
    }
    addGoods(item){
        var goods=this.shoppingList.find(t=>t.id===item.id);
        if(goods){
            goods.num++;
            goods.total=goods.price*goods.num;
        }else{
            this.shoppingList.push(item);
        }
        this.renderTable();
    }
    renderTable(){
        this.tbody.innerHTML="";
        this.all.checked=this.shoppingList.every(item=>item.checked);
        for(var i=0;i<this.shoppingList.length;i++){
            var tr=this.createDom("tr");
            var item=this.shoppingList[i];
            for(var key in item){
                if(key==="id") continue;
                var td=this.createDom("td",undefined,tr);
                this.renderTd(td,item,key);
            }
            this.tbody.appendChild(tr);
        }
    }
    renderTd(td,item,key){
        switch(key){
            case "checked":
                var ck=this.createDom("input",undefined,td);
                ck.type="checkbox";
                ck.checked=item.checked;
                ck.id=item.id;
                ck.addEventListener("click",e=>this.checkedHandler(e));
                break;
            case "icon":
                var img=this.createDom("img",undefined,td);
                img.src=item.icon;
                img.style.width="80px"
            break;
            case "num":
                var step=new StepNumber();
                step.setStep(item.num);
                step.appendTo(td);
                step.id=item.id;
                step.addEventListener(StepNumber.STEP_CHANGE_EVENT,e=>this.stepChangehandler(e));
                break;
            case "deleted":
                var a=this.createDom("a",undefined,td);
                a.href="javascript:void(0)";
                a.textContent="删除";
                a.id=item.id;
                a.addEventListener("click",e=>this.deleteClickHandler(e),{once:true});
                break;
            case "price":
            case "total":
                td.textContent=Number(item[key]).toFixed(2);
                break;
            default:
                td.textContent=item[key];
        }
    }
    stepChangehandler(e){
       var item=this.shoppingList.find(item=>item.id===e.target.id);
       item.num=e.step;
       item.total=item.num*item.price;
        this.renderTable();
    }
    deleteClickHandler(e){
        // console.log(e.target.id);//字符串
        this.shoppingList=this.shoppingList.filter(item=>item.id!=e.target.id);
        this.renderTable();
    }
    checkedHandler(e){
        if(e.target.id==="all"){
            this.shoppingList.forEach(item=>{
                item.checked=e.target.checked;
            })
        }else{
            var item=this.shoppingList.find(item=>item.id==e.target.id);
            item.checked=e.target.checked;
        }

        this.renderTable();
    }
}