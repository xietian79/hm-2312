前端模块化
ES5模块化
AMD
  所有路径都不能带扩展名
 导出
  默认导出1个
  define(function(){
    定义模块
    return 模块
  })

 默认导出多个
  define(function(){
    定义模块1
    定义模块2
    定义模块3
    return {模块1,模块2,模块3}
  })

  导出的模块有依赖
  define([依赖模块路径1,依赖模块路径2...],function(依赖模块1,依赖模块2...){
    定义模块
    return 模块
  })

 导入
  require([路径1,路径2...],function(路径1的模块,路径2的模块...){

  })

  require([路径1,路径2...],function(路径1的模块,{模块a,模块b..}...){

  })
ES6
 导出
  export default 模块
  export 模块

  导入
    import 模块名 from 路径
    import {模块1,模块2} from 路径




nodejs模块化
Commonjs
  导出
    module.exports=
    exports.模块名=
  导入
    var 模块=require(路径)
    var {模块1,模块2..}=require(路径)
    var {模块1=别名,模块2..}=require(路径)

ES6
 导出
  export default 模块
  export 模块

  导入
    import 模块名 from 路径
    import {模块1,模块2} from 路径
    import {模块1 as 别名,模块2} from 路径