// var obj:object={a:1,b:2}
// obj.a

// fn变量无法设置类型
// var fn=(a:number,b:number):void=>{
//     console.log(a+b);
// }

// 对象定义接口类型

// 这样写obj变量类型时，只能修改属性值，不能删除或者添加新属性
// var obj:{a:number,b:number}={a:1,b:2};
// obj.a++;
// console.log(obj.a);
// delete obj.a;//删除会报错
// obj.c=3;//不能添加新属性

// ?: 表示这个属性是可选属性，这个属性可有可无
// var obj:{a:number,b?:number}={a:1,b:2};
// obj.a++;
// delete obj.b;

// 所以我们可以添加已经设定可有可无的可选属性
// var obj:{a:number,b?:number,c?:number}={a:1};
// obj.b=20;
// obj.c=30;


// 首先先做断言,在分开给入属性，但是这些属性仍然不能删除
// var obj:{a:number,b:number,c:number}={a:1} as {a:number,b:number,c:number}
// obj.b=10;
// obj.c=20;
// delete obj.b;

// [key:string]:number  key属性名必须是字符串类型 属性值必须number数值型
// 可以随意给对象中增加或者删除键名，但是键名必须是字符型，值必须是数值型
// var obj:{[key:string]:number}={a:1,b:2};

// obj.c=10;
// obj.age=20;
// delete obj.c;


// 匿名函数或者箭头函数定义接口类型

// var fn= function(a:number,b:number):void
// {
//     console.log(a,b);
// }

// (a:number,b:number) 是函数的参数类型
// =>void 是函数的返回类型
// var fn:(a:number,b:number)=>void=function(a,b){

// }

// var fn:(a:number,b:number)=>number=(a,b)=>a+b;

// 如果写匿名函数或箭头函数时，参数是初始值参数时，必须设置为可选参数，但是函数内如给入初值
// var fn:(a:number,b?:number,c?:number,...arg:Array<number>)=>void=(a,b=3,c,...arg)=>{
//     console.log(a,b,c,arg)
// }
// fn(1);
// fn(1,2);
// fn(1,2,3);
// fn(1,2,3,4,5)