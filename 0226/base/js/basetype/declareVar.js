"use strict";
// for (var i = 0; i < 10; i++) {
//   setTimeout(function (n:number) {
//     console.log(n);
//   }, 100 * i,i);
// }
// function fn(){
//     console.log(a);//undefined
//     if(a===undefined){
//         var a=10;
//     }
//     console.log(a);//10
// }
// fn();
// var a:number=1;
// var b:number=2;
// [b,a]=[a,b];
// console.log(a,b);
// 在ts中函数也有命名函数、匿名函数、箭头函数
// a:number 必填参数，数值类型
// b:number|string=1 默认值参数，数值类型或者字符串类型
// c?:number=1 可选参数，数值类型
// ...arg:Array<number> 剩余参数都是数值型数组
// function fn(a:number,b:number|string=1,c?:number,...arg:Array<number>){
//     console.log(a,b,c,arg)
// }
// fn(1);
// fn(1,2);
// fn(1,2,3);
// fn(1,2,3,4);
// fn(1,2,3,4,5);
// ...arg:[string,number,string] 剩余参数都是元组类型
// function fn(a:number,b:number|string=1,...arg:[string,number,string]){
//     console.log(a,b,arg);
// }
// fn(1,2,"a",3,"b");
// fn(1,2);
// 不返回值
// function fn():void
// {
// }
// function fn():number{
//     return 1;
// }
// function fn():number|string{
//     return 1;
// }
// 不能再返回值类型中使用?
// 匿名函数
// var fn=function(a:number,b:number):void
// {
// }
// fn(1,2)
// 箭头函数 在ts中使用箭头函数，不能省略() ,{}
// var fn=(a:number,b:number):void=>{
//     console.log(a+b);
// }
// var fn=(a:number):number=>{
//     return a+1;
// }
//# sourceMappingURL=declareVar.js.map