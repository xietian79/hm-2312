"use strict";
// var str1:string="a";
// var str2:String="a";
// str1=1; //报错
// str1=new String("b");
// str2=new String("b");
// console.log(str2=="b");//true
// console.log(str2==="b");//false
// 字面量类型存储在栈中，构造函数创建的字符类型存储在堆中
//1、 string (String)
// var str1:string="a";
// var str2:string='a';
// var str3:string=`a`;
//2、 number (Number)
// var num1:number=1;
// var num2:Number=1;
// var num2:number=-4;
// var num3:number=3.5
// var num4:number=3.5e+3;
// var num5:number=3.5e-3;
// var num6:number=0o4;//8进制
// var num7:number=0xFF;//16进制
//3、 boolean (Boolean)
// var bool1:boolean=true;
// var bool2:boolean=false;
//4、 undefined
// var a:undefined;
// var b:undefined=undefined;
// |  这个变量可以是前面的类型或者后面的类型
// var a:number|undefined;
// a=5;
// a=undefined;
//5、 null
// var o:null=null;
//6、 object(Object)  声明它是一个对象，但是这个对象的属性不可用
// var o:object={a:1,b:2};
// var o1:Object={a:2,b:2};
// o=new Object({a:2,b:2});
// o1=new Object({a:1,b:2})
// var o:object={a:1,b:2};
// console.log(o.a);
// delete o.a;
// o.a=10;
// o.c=20;
// var o:object|null={a:1,b:2};
// o=null;
//7、 symbol(Symbol)
// var s:Symbol=Symbol("a");
// var s1:symbol=Symbol("b");
//8、 ES2020 bigint
// var a:bigint=34567898765789876n;
//9、 正则类型
// var reg:RegExp=/a/g;
//10、 日期类型
// var date:Date=new Date();
//11、 数组
// Array<元素的类型>
// var arr:Array<number>=[1,2,3,4]
// var arr:Array<number|string>=[1,2,"a","b"];
// 元素类型[]
// var arr:number[]=[1,2,3,4,5];
// (元素类型|元素类型)[]
// var arr:(number|string)[]=[1,2,3,4,"a"]
// 二维数组
// var arr:Array<Array<number>>=[[1,2,3],[4,5,6]]
// var arr:number[][]=[[1,2,3],[4,5,6]];
//12、 元组 表明数组中某个元素是什么类型
// var list:[string,number,boolean,string]=["a",1,true,"b"];
// 用法与数组相同,尽量不要对元组做增删改
// list.pop();
// list.push("d");
// console.log(list)
// var a:number=list[1];
// var b:string=list[4];//报错 因为给元组中添加了一个新的元素，但是在元组声明时没有这个元素类型，所以调用时会报错
// 13、枚举 枚举是指常量属性，不能修改，配置默认的常量属性，部分常量会做归类
// const enum COLOR{RED,GREEN,BLUE};
// console.log(COLOR.RED);//0
// console.log(COLOR.GREEN);//1
// console.log(COLOR.BLUE);//2
// const enum ACTION{RUN,WALK,PLAY,JUMP};
// var state:ACTION=ACTION.JUMP;
// switch(state){
//     case ACTION.RUN:
//         break;
//     case ACTION.WALK:
//         break;
//     case ACTION.PLAY:
//         break;
//     case ACTION.JUMP:
//         break;
// }
// 枚举类型只能赋值为字符串或者数值型
// const enum ACTION{RUN="run",WALK="walk",PLAY="play",JUMP="jump"};
// var state:ACTION=ACTION.PLAY;
// console.log(state==="play")
// const enum COLOR{RED=0,GREEN=1,BLUE=2};
// const enum COLOR{RED=3,GREEN,BLUE};//3 4 5
// console.log(COLOR.GREEN)
// const enum COLOR{RED,GREEN=5,BLUE};//0  5  6
// console.log(COLOR.RED)
// console.log(COLOR.BLUE)
// const enum COLOR{RED="red",GREEN=1,BLUE};//red 1  2
// console.log(COLOR.BLUE)
// const enum COLOR{RED=1,GREEN="green",BLUE};//报错 如果前一个枚举值不是数值，后面枚举值不能省略值
// 枚举可以使用反射，反射值必须是字符串，必须和属性名相同
// const enum COLOR{Red="Red",GREEN=1,BLUE};
// var state:COLOR=COLOR['Red'];
// 14、Set
// var s:Set<number>=new Set([1,2,3,4,5]);
// 15、Map Map<键的类型,值的类型>
// var m:Map<string,number>=new Map();
// m.set("a",1);
// m.set("b",2);
// m.set("c","d")
// 16、any  禁止使用any
// var a:any=1;
// a="a";
// 17、void 不返回值 相当于undefined类型
// var a:void;
// a=undefined;
// function 函数名(参数:参数类型...):返回值类型 { }
// function fn(a:number,b:number):number
// {
//     var s:number=a+b;
//     return s;
// }
// void表示函数不返回任何值
// function fn():void
// {
//     console.log("aa")
// }
// 18、Never 永不
// 当函数中抛出异常不会继续时就有never
// function fn():never
// {
//     throw new Error("a");
//     console.log("aaa")
// }
// function fn(a:number):number|never
// {
//     if(!Number.isInteger(a)) throw new RangeError("必须是整数");
//     a++;
//     return a;
// }
// function fn(a:number):number|RangeError
// {
//     if(!Number.isInteger(a)) throw new RangeError("必须是整数");
//     a++;
//     return a;
// }
// 断言
// 1、在需要断言的内容后面写上 as 断言的类型
// 2、在需要断言的内容前面写上<断言的类型>
// function fn(){
//     var div:HTMLDivElement|null=document.querySelector("div");
//     if(div){
//         div.style.width="50px";
//         div.style.height="50px";
//         div.style.backgroundColor="red";
//     }
// }
// function fn(){
//     // 获取到的内容断言为HTMLDivElement
//     // var div:HTMLDivElement=document.querySelector("div") as HTMLDivElement;
//     var div:HTMLDivElement=<HTMLDivElement>document.querySelector("div");
//     div.style.width="50px";
//     div.style.height="50px";
//     div.style.backgroundColor="red";
// }
// fn();
// var imgCon:HTMLDivElement,
//     dot:HTMLUListElement,
//     left:HTMLImageElement,
//     right:HTMLImageElement,
//     prev:HTMLLIElement|undefined;
// var pos:number=0;
// init();
// function init():void
// {
//     imgCon=document.querySelector(".img-con") as HTMLDivElement;
//     dot=document.querySelector(".dot") as HTMLUListElement;
//     left=document.querySelector(".left") as HTMLImageElement;
//     right=document.querySelector(".right") as HTMLImageElement;
//     dot.addEventListener("click",dotClickHandler);
//     left.addEventListener("click",bnClickHandler);
//     right.addEventListener("click",bnClickHandler);
//     changePrev();
// }
// function dotClickHandler(e:MouseEvent):void
// {
//     var elem:HTMLElement=e.target as HTMLElement;
//     if(elem.nodeName!=="LI") return;
//     // pos=Array.from(dot.children).indexOf(e.target);
//     pos=Number(elem.getAttribute("index"));
//     imgConMove();
// }
// function bnClickHandler(e:MouseEvent){
//     if(e.target===left){
//         pos--
//         if(pos<0) pos=4;
//     }else{
//         pos++
//         if(pos>4) pos=0;
//     }
//     imgConMove();
// }
// function imgConMove():void
// {
//     imgCon.style.left=-pos*imgCon.offsetWidth/5+"px"
//     changePrev();
// }
// function changePrev():void
// {
//     if(prev){
//         prev.style.backgroundColor="rgba(255,0,0,0)";
//     }
//     // 第几张图对应li的下标
//     prev=dot.children[pos] as HTMLLIElement;
//     prev.style.backgroundColor="red";
// }
//# sourceMappingURL=type1.js.map