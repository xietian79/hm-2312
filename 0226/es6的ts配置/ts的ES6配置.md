第一次操作 ，以后不需要
1、npm i typescript -g  全局下载ts
2、查看版本 tsc -v



1、tsc --init  初始化ts配置文件 产生tsconfig.json
{
  "compilerOptions": {
     "target": "ESNext",    ts转换为js的目标版本
      "module": "ES6",       ts转换为js的模块化方式
      "rootDir": "./ts",    将哪个路径下的ts文件转换为js，源目录
      "outDir": "./js",      将ts转换为js放在哪个文件夹下，输出目录
      "sourceMap": true,    自动产生一个源映射文件ts文件，可以自动将转换后的js错误指向ts的对应位置
  }
}


2、tsc -w 监听rootDir路径下的ts，如果编辑修改就会自动转换到outDir路径下的js
3、ts中所有的路径必须写扩展名，并且路径对应的扩展名必须是js，不是ts