define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Box = /** @class */ (function () {
        function Box(name) {
            this.name = name;
        }
        Box.prototype.run = function () {
            console.log("run");
            // console.log(this.age+1);
        };
        return Box;
    }());
    exports.default = Box;
});
//# sourceMappingURL=Box.js.map