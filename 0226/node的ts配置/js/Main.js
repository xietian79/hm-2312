"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userRouter_1 = __importDefault(require("./router/userRouter"));
var app = (0, express_1.default)();
app.use("/user", userRouter_1.default);
app.listen(4000);
//# sourceMappingURL=Main.js.map